package net.one97.paytm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * Created by Rama on 4/25/2016.
 */
public class EmptyViewHolder extends RecyclerView.ViewHolder implements UpdateViewHolder {

    Context mContext;

    public EmptyViewHolder(Context mContext, View convertView) {
        super(convertView);
        this.mContext=mContext;
    }

    @Override
    public void updateViewHolder(Context context, final CJRHomePageLayoutV2 pageLayout, final String
            mGAKey) {

    }
}
