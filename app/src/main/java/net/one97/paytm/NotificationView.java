
package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NotificationView extends RelativeLayout {

    private RelativeLayout mMainLayout;
    private ImageView mCartIcon;
    public TextView mTxtCartItemCount;
    public TextView mTxtCartItemCountDecoy;
    private OnNotificationViewClickListener mOnNotificationViewClickListener;
    private boolean cartIconBlack;

    public interface OnNotificationViewClickListener {

        void setNotificationViewClick(View view);
    }

    public NotificationView(Context context) {
        super(context);
        init(context);
    }

    public NotificationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public NotificationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {

        LayoutInflater.from(context).inflate(R.layout.cart_view, this, true);
        mCartIcon = (ImageView) findViewById(R.id.icon_cart);
        mCartIcon.setBackgroundResource(R.drawable.ic_cart_dark);
        cartIconBlack = true;
        mTxtCartItemCount = (TextView) findViewById(R.id.txt_number_of_items_in_cart);
        mTxtCartItemCountDecoy = (TextView) findViewById(R.id.txt_number_of_items_in_cart_decoy);
        mMainLayout = (RelativeLayout) findViewById(R.id.container_cart_icon);
        mMainLayout.setClickable(true);
        mMainLayout.setBackgroundColor(Color.TRANSPARENT);
        mMainLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                mOnNotificationViewClickListener.setNotificationViewClick(view);
            }
        });
    }

    public void setMainLayoutParams(int gridUnit, int hight) {
        LayoutParams textParams = (LayoutParams) mTxtCartItemCount.getLayoutParams();
        textParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        LayoutParams textParamsDecoy = (LayoutParams) mTxtCartItemCountDecoy.getLayoutParams();
        textParamsDecoy.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);


        LayoutParams imageParams = (LayoutParams) mCartIcon.getLayoutParams();
        imageParams.addRule(RelativeLayout.CENTER_VERTICAL);
    }

    public void setNotificationTextMargin(int left, int top, int right, int bottom) {
        LayoutParams textParams = (LayoutParams) mTxtCartItemCount.getLayoutParams();
        textParams.leftMargin = left;
        textParams.topMargin = top;
        textParams.rightMargin = right;
        textParams.bottomMargin = bottom;

        LayoutParams textParamsDecoy = (LayoutParams) mTxtCartItemCountDecoy.getLayoutParams();
        textParamsDecoy.leftMargin = left;
        textParamsDecoy.topMargin = top;
        textParamsDecoy.rightMargin = right;
        textParamsDecoy.bottomMargin = bottom;
    }

    public void setMainLayoutBackground(int resid) {
        mMainLayout.setBackgroundResource(resid);
    }

    public void setMainLayoutTag(String tag) {
        mMainLayout.setTag(tag);
    }

    public String getMainLayoutTag() {
        return (String) mMainLayout.getTag();
    }

    public void setImageBackground(int resid) {
        mCartIcon.setBackgroundResource(resid);
    }

    public void setNotificationTextBackground(int resid) {
        mTxtCartItemCount.setBackgroundResource(resid);
    }

    public void setNotificationTextColor(int color) {
        mTxtCartItemCount.setTextColor(color);
    }

    public void setNotificationText(String text, int gridUnit) {
        if (text != null && text.trim().length() > 0) {
            setNotificationTextVisibility(VISIBLE);
            mTxtCartItemCount.setText(text);
            mTxtCartItemCountDecoy.setText(text);
            /*if (text.trim().length() > 1) {
				setNotificationTextMargin(0, gridUnit / 4, gridUnit / 7, 0);
				mTxtCartItemCount.setBackgroundResource(R.drawable.notification_oval);
			} else {*/
            setNotificationTextMargin(0, gridUnit / 4, gridUnit / 4, 0);
//				mTxtCartItemCount.setBackgroundResource(R.drawable.cart_count);
            //}
        } else {
            setNotificationTextVisibility(GONE);
        }
    }

    public void setNotificationTextVisibility(int visibility) {
        mTxtCartItemCount.setVisibility(visibility);
        mTxtCartItemCountDecoy.setVisibility(visibility);
        if (mCartIcon != null) {
            if (visibility == VISIBLE) {
                if (cartIconBlack) {
                    mCartIcon.setBackgroundResource(R.drawable.ic_cart_dark_smiley);
                } else {
                    mCartIcon.setBackgroundResource(R.drawable.ic_cart_white_smiley);
                }
            } else {
                if (cartIconBlack) {
                    mCartIcon.setBackgroundResource(R.drawable.ic_cart_dark);
                } else {
                    mCartIcon.setBackgroundResource(R.drawable.mybag_icon);
                }
            }
        }
    }

    public void setOnNotificationViewClickListener(OnNotificationViewClickListener listener) {
        mOnNotificationViewClickListener = listener;
    }

    public void animateTextLayout(Activity activity) {
        Animation traslateBottomHide = AnimationUtils.loadAnimation(
                activity, R.anim.anim_zoom);
        mTxtCartItemCount.startAnimation(traslateBottomHide);
        mTxtCartItemCountDecoy.startAnimation(traslateBottomHide);
    }

    public void setWhiteThemeforCartIcon() {
        mCartIcon.setBackgroundResource(R.drawable.ic_cart_white_smiley);
    }

    public void setWhiteBagIcon() {
        cartIconBlack = false;
        if (mTxtCartItemCount != null && mTxtCartItemCount.getVisibility() == VISIBLE)
            mCartIcon.setBackgroundResource(R.drawable.ic_cart_white_smiley);
        else
            mCartIcon.setBackgroundResource(R.drawable.mybag_icon);
    }

    public void setDarkThemeforCartIcon() {
        cartIconBlack = true;
        if (mTxtCartItemCount != null && mTxtCartItemCount.getVisibility() == VISIBLE)
            mCartIcon.setBackgroundResource(R.drawable.ic_cart_dark_smiley);
        else
            mCartIcon.setBackgroundResource(R.drawable.ic_cart_dark);
    }
}
