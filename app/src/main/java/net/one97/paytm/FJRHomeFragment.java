package net.one97.paytm;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.HashMap;


public class FJRHomeFragment extends FJRMainBaseFragment implements SwipeRefreshLayout
        .OnRefreshListener {


    private static final String TAG = "FJRSecondaryHomeFragment";
    private static final String HOME_PAGE = "homepage";
    private Resources mResources;
    private String mCurrentPageName = "Recharge";
    private CJRHomePageV2 mHomePageItemList = null;
    private ViewPager mPager;
    private View mFragmentView;
    private CJRCustomRecyclerView mMarketPlaceList;
    private boolean isDataLoaded = false;
    private boolean isDataLoading = false;
    private String mListType;
    private String mGAKey;
    private LinearLayout mLytNoNetwork;
    private CJRItem mItem;
    private HashMap<String, Object> mSearchMap = new HashMap<>();
    private CJRHomeFragmentListAdapter mPageListAdapter;
    private FrameLayout wallet_sticky_header;
    protected ProgressBar mProgessBarItemLoading;
    private HomePresenter mHomePresenter;
    FragmentToActivityListener mainActivity;

    public FJRHomeFragment() {

    }

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            getSearchItem();
        }
    }


    @Override
    public void onResume() {
        if (mProgessBarItemLoading != null)
            mProgessBarItemLoading.setVisibility(View.GONE);
        updateHeading();
        super.onResume();
    }

    public void updateHeading() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            if (wallet_sticky_header != null) {
                TextView walletheader = (TextView) wallet_sticky_header.findViewById(R.id.txt_horizontal_links_title);

                walletheader.setText(getResources().getString(R.string.wallet_top_bar_heading));
            }
        }
    }

    private void getSearchItem() {
        mItem = (CJRItem) getArguments()
                .getSerializable(CJRConstants.EXTRA_INTENT_ITEM);
        if (mItem != null) {
            mSearchMap.put(CJRConstants.SEARCH_TYPE, mItem.getSearchType());
            mSearchMap.put(CJRConstants.SEARCH_CATEGORY, mItem.getSearchCategory());
            mSearchMap.put(CJRConstants.SEARCH_TERM, mItem.getSearchTerm());
            mSearchMap.put(CJRConstants.SEARCH_RESULT_TYPE, mItem.getSearchResultType());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_home_layout, null);
        mMarketPlaceList = (CJRCustomRecyclerView) mFragmentView.findViewById(R.id.home_list);

        mHomePresenter = new HomePresenter(this);
        mLytNoNetwork = (LinearLayout) mFragmentView.findViewById(R.id.no_network);
        mLytNoNetwork.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mProgessBarItemLoading = (ProgressBar) mFragmentView.findViewById(R.id
                .secondary_home_page_item_loading);

        homepage_loading = (RelativeLayout) mFragmentView.findViewById(R.id.rel_homepage_loading);
        mResources = getResources();

        wallet_sticky_header = (FrameLayout) mFragmentView.findViewById(R.id.wallet_sticky_header);

            wallet_sticky_header.setVisibility(View.GONE);



        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()
                .getBaseContext());
        mMarketPlaceList.setLayoutManager(linearLayoutManager);
        (mFragmentView.findViewById(R.id.network_retry_btn)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefresh();
            }
        });
        //to show upgrade button or not

        return mFragmentView;
    }



    public void isDataLoading() {
        if (getActivity() == null || isDetached()) {
            return;
        }
        if (isDataLoaded || isDataLoading) return;
        isDataLoading = true;
    }


    public void setCurrentPageName(String name) {
        mCurrentPageName = name;
    }


    public void hideProgress() {
        if (homepage_loading != null)
            homepage_loading.setVisibility(View.GONE);
    }

    public void hideWalletView() {
        mLytNoNetwork.setVisibility(View.GONE);
        if (wallet_sticky_header != null && wallet_sticky_header.getVisibility() == View.VISIBLE)
            wallet_sticky_header.setVisibility(View.GONE);
    }

    public void noIntenetConnection() {
        if (mHomePageItemList == null) {
            hideProgress();
            isDataLoading = false;
            isDataLoaded = false;
            mLytNoNetwork.setVisibility(View.VISIBLE);
        }
    }

    public void updateHomePageData(IJRDataModel dataModel) {
        mHomePageItemList = (CJRHomePageV2) dataModel;


        //Updating the category_id and category_url in application_class. Need for deeplinks in
        // future.
        if (getActivity() instanceof CJRActionBarBaseActivity) {
           // ((CJRActionBarBaseActivity) getActivity()).setCategoryIdUrl(mHomePageItemList);
        }

        //adding event static item to homepage list
        //addSmartListForTesting();

        hideWalletView();

        if (getActivity() == null) return;
        if (dataModel == null || getActivity() == null || isDetached()) {
            return;
        }

        if (dataModel instanceof CJRHomePageV2) {

            CJRHomePageV2 homePage = new CJRHomePageV2();
            if (homePage.getErrorMsg() != null) {
               /* CJRAppUtility.showAlert(getActivity(), mResources
                                .getString(R.string.network_error_heading),
                        mResources.getString(R.string
                                .network_error_message));*/
            }

            prepareHomepageList();
        }
    }

    private void addSmartListForTesting() {
        try {
            int rowPosition = 0;
            CJRHomePageDetailV2 selectedPageItem = mHomePageItemList.getmPage().get(rowPosition);
            selectedPageItem.getHomePageLayoutList().get(0).getHomePageItemList().add(0, getGoldItem());

            selectedPageItem = mHomePageItemList.getmPage().get(rowPosition);
            selectedPageItem.getHomePageLayoutList().get(0).getHomePageItemList().add(1, getMutualFundItem());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //generating event item
    public CJRHomePageItem getGoldItem() {
        CJRHomePageItem eventItem = new CJRHomePageItem();
        eventItem.setItemID("51323");
        eventItem.setParentItem("1");
        eventItem.setName("Gold");
        eventItem.setTitle(null);
        eventItem.setUrlType(CJRConstants.URL_TYPE_GOLD);
        eventItem.setUrl("https://paytm.com/digitalgold");
        return eventItem;
    }


    private CJRHomePageItem getMutualFundItem() {
        CJRHomePageItem eventItem = new CJRHomePageItem();
        eventItem.setItemID("51323");
        eventItem.setParentItem("1");
        eventItem.setName("Mutual Funds");
        eventItem.setTitle(null);
        eventItem.setImageUrl("");
        eventItem.setUrlType(CJRConstants.URL_TYPE_MUTUAL_FUND);
        eventItem.setUrl("https://paytm.com/mutualfunds");
        return eventItem;
    }

    private void prepareHomepageList() {

        trimHomePageData(mHomePageItemList);
        mGAKey = mHomePageItemList.getGAKey();
        mListType = mGAKey;

        if (mHomePageItemList == null) {
            return;
        }

        if (mHomePageItemList.getmPage().size() > 0) {
            mHomePageItemList.setGATitle(mCurrentPageName);
            isDataLoaded = true;
        }

        if (mPageListAdapter != null && mHomePageItemList != null &&
                mHomePageItemList.getmPage().size() > 0) {
            addHeaderBar();
            mHomePageItemList.getmPage().add(addFooterHomeLayout());
            mPageListAdapter.updateFromNetwork(mHomePageItemList);
        } else
            updateUI();
    }


    private void updateUI() {
        if (getActivity() == null || isDetached()) {
            return;
        }
        if (mHomePageItemList == null) {
            return;
        }

        addHeaderBar();
        mHomePageItemList.getmPage().add(addFooterHomeLayout());
        mPageListAdapter = new CJRHomeFragmentListAdapter(getActivity(),mHomePageItemList,mGAKey);
            mMarketPlaceList.setAdapter(mPageListAdapter);

        hideProgress();
    }

    private void addHeaderBar() {
        mHomePageItemList.getmPage().add(0, addViewPagerHomeLayout());
            mHomePageItemList.getmPage().add(1, mHomePresenter.getWalletHomeLayout());

    }


    public void showNetworkErrorMessage(String title, String message) {
        if (mHomePageItemList == null) {
            mLytNoNetwork.setVisibility(View.VISIBLE);
            ((TextView) mLytNoNetwork.findViewById(R.id.no_network_title)).setText(title);
            ((TextView) mLytNoNetwork.findViewById(R.id.no_network_message)).setText(message);
        }
    }




    @Override
    public void onResponse(IJRDataModel dataModel) {
        if (getActivity()==null || dataModel == null || (getActivity() != null && getActivity().isFinishing()))
            return;

    }



    private void trimHomePageData(CJRHomePageV2 homePage) {
        if (homePage != null) {
            ArrayList<CJRHomePageDetailV2> pageLayout = homePage.getmPage();
            pageLayout.trimToSize();
            for (CJRHomePageDetailV2 layout : pageLayout) {
                ArrayList<CJRHomePageLayoutV2> cjrHomePageLayoutV2s = layout.getHomePageLayoutList();
                cjrHomePageLayoutV2s.trimToSize();
                for (CJRHomePageLayoutV2 c : cjrHomePageLayoutV2s) {
                    c.getHomePageItemList().trimToSize();
                }
            }
        }
    }

    private CJRHomePageDetailV2 addViewPagerHomeLayout() {
        CJRHomePageLayoutV2 customLayout = new CJRHomePageLayoutV2();
        customLayout.setLayout(LayoutType.LAYOUT_VIEW_PAGER.getName());
        CJRHomePageDetailV2 cjrHomePageDetailV2 = new CJRHomePageDetailV2();
        cjrHomePageDetailV2.setmHomePageLayoutList(customLayout);

        return cjrHomePageDetailV2;
    }


    private CJRHomePageDetailV2 addFooterHomeLayout() {
        CJRHomePageDetailV2 cjrHomePageDetailV2 = new CJRHomePageDetailV2();
        CJRHomePageLayoutV2 customLayout = new CJRHomePageLayoutV2();

        customLayout.setLayout(LayoutType.LAYOUT_FOOTER_SELL_PARTNER_CONTACT_ICONS.getName());
        customLayout.setFooterImageURL(mHomePageItemList.getFooterImage());
        cjrHomePageDetailV2.setmHomePageLayoutList(customLayout);
        return cjrHomePageDetailV2;
    }




    @Override
    public void onFileReadComplete(IJRDataModel dataModel, String fileHeader) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onEditViewClick(View view) {

    }

    @Override
    public void onRefresh() {
        isDataLoaded = false;
        isDataLoading = false;
        if (homepage_loading != null)
            //homepage_loading.setVisibility(View.VISIBLE);
        mHomePresenter.loadData();
    }



    @Override
    public void updateData(CJRItem item) {
        //TODO(bheemesh) since mainFragment removed need to check later
//        if (mainFragment != null) {
//            mainFragment.updateData(item);
//        }
    }

    private void updateArgument(CJRItem item) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            bundle.putSerializable(CJRConstants.BUNDLE_EXTRA_CATEGORY_ITEM, item);
            bundle.putBoolean("EXTRA_IS_CATALOG_UPDATED", true);
        }
    }

    @Override
    public void onServerDataLoaded() {
        // TODO Auto-generated method stub

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onRefresh();
    }


    /**
     * Handling all the click event of the items here. related functions are below written
     */






    /**
     * update header string as per user login condition and
     * if user is not login this string click envent should move to login activity
     * if user is login click event should move to passbook Screen.
     *
     * @param
     */





    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        updateHeading();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (FragmentToActivityListener) context;

        } catch (Exception ex) {


        }


    }
}
