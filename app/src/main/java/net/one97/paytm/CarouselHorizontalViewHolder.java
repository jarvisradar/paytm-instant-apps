package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bheemesh
 */
public class CarouselHorizontalViewHolder extends RecyclerView.ViewHolder implements
        UpdateViewHolder, AdapterView.OnItemClickListener {

    private CJRHorizontalListView listView;
    private int mGridUnit;
    private Context mContext;
    private CJRHomePageLayoutV2 cjrHomePageLayoutV2;
    private String containerInstanceID="";
    private TextView mtitle;
    private View bottombar;
    private String mchildSiteId;

    public CarouselHorizontalViewHolder(Context mContext, View convertView) {
        super(convertView);
        this.mContext = mContext;
        listView = (CJRHorizontalListView) convertView.findViewById(R
                .id.carousel_horizontal_list);
        mtitle = (TextView)convertView.findViewById(R.id.title);
        bottombar = convertView.findViewById(R.id.bottom_bar);
    }

    @Override
    public void updateViewHolder(Context context, final CJRHomePageLayoutV2 pageLayout, String
            mGAKey) {

        final String itemName = pageLayout.getName();
        this.cjrHomePageLayoutV2 = pageLayout;

    /*    if(pageLayout.getLayout().equalsIgnoreCase("carousel-row")) {
            mtitle.setVisibility(View.VISIBLE);
            mtitle.setText(cjrHomePageLayoutV2.getName());
            bottombar.getLayoutParams().height= (int) mContext.getResources().getDimension(R.dimen
                    .bottom_view_height);
        }*/

        String layoutType = (null != mchildSiteId) ? StringUtils.concat(pageLayout.getmLayoutType(),"_",pageLayout.getmListTitleName()) :
                pageLayout.getLayout();

        if(null != listView && listView.getAdapter() == null) {

            CarouselHorizontalListAdapter adapter = new CarouselHorizontalListAdapter((Activity) mContext,
                    pageLayout.getHomePageItemList(), layoutType, itemName, mGAKey + "-", containerInstanceID,mchildSiteId);
            adapter.setSendViewEvent(true);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(this);
            Object containerID = getContainerID();
            if(containerID != null){
                adapter.setContainerId(containerID.toString());
            }
        } else {
            ((CarouselHorizontalListAdapter)listView.getAdapter()).setItemList(pageLayout.getHomePageItemList(), layoutType);
        }

//        if(mchildSiteId!=null){
//            CarouselHorizontalListAdapter adapter = new CarouselHorizontalListAdapter((Activity) mContext,
//                    pageLayout.getHomePageItemList(), pageLayout.getmLayoutType()+"_"+pageLayout.getmListTitleName(), itemName, mGAKey + "-", containerInstanceID,mchildSiteId);
//            listView.setAdapter(adapter);
//
//        }
//        else {
//
//            Log.e("GAv", "updateViewHolder ..... ");
//
//            CarouselHorizontalListAdapter adapter = new CarouselHorizontalListAdapter((Activity) mContext,
//                    pageLayout.getHomePageItemList(), pageLayout.getLayout(), itemName, mGAKey + "-", containerInstanceID);
//            adapter.setSendViewEvent(true);
//            listView.setAdapter(adapter);
//
//        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        CJRHorizontalListView listView = (CJRHorizontalListView) parent;
        CJRHomePageItem item = (CJRHomePageItem) listView
                .getItemAtPosition(position);

        if(cjrHomePageLayoutV2 != null){

            Map<String, Object> sourceInfo = new HashMap<String, Object>();
            sourceInfo.put(CJRConstants.SOURCE_POSITION_KEY, position + 1);
            sourceInfo.put(CJRConstants.SOURCE_ID_KEY, item.getItemID());
            sourceInfo.put(CJRConstants.SOURCE_CONTAINER_INSTANCE_ID_KEY, containerInstanceID);
            sourceInfo.put(CJRConstants.SOURCE_CONTAINER_ID_KEY, getContainerID());

            item.setSourceInfo(sourceInfo);


        }

    }

    private Object getContainerID() {
        String containerId = null;
        if (cjrHomePageLayoutV2 != null && cjrHomePageLayoutV2.getDatasources() != null && cjrHomePageLayoutV2.getDatasources().size() > 0) {
            containerId = cjrHomePageLayoutV2.getDatasources().get(0).getmContainerID();
        }
        return containerId;
    }

    public void updateSiteID(String mchildSiteId){
        this.mchildSiteId = mchildSiteId;
    }

}
