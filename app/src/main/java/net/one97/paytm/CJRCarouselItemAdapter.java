
package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CJRCarouselItemAdapter extends CJRBaseAdapter {

    private ArrayList<CJRHomePageItem> mItemList;
    private LayoutInflater mInflater;
    private String mLayoutName;
    private Activity mActivity;
    private String mParent;
    private String mScreenName;
    private String mcontainerInstanceId;

    public CJRCarouselItemAdapter(Activity activity, ArrayList<CJRHomePageItem> itemList, String
            layout, String parent, String screenName, String containerInstanceID) {
        super();
        mItemList = itemList;
        mInflater = LayoutInflater.from(activity);
        mLayoutName = layout;
        mActivity = activity;
        mParent = parent;
        mScreenName = screenName;
        mcontainerInstanceId = containerInstanceID;
    }


    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mItemList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int resId;
        int imageWidth = 0;
        int imageHeight = 0;
        resId = R.layout.small_carousel_item;

        CJRHomePageItem item = mItemList.get(position);
        item.setParentItem(mParent);
        if (convertView == null) {
            convertView = mInflater.inflate(resId, null);
            ViewHolder holder = new ViewHolder();
            holder.contentThumbnail = (ImageView) convertView.findViewById(R.id.content_thumbnail);
            holder.progressbar = (ProgressBar) convertView.findViewById(R.id.image_progress_bar);

            convertView.setTag(holder);
        }

        final ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        imageHeight = (int) mActivity.getResources().getDimension(R.dimen
                .small_carousel_height);
        imageWidth = (int) mActivity.getResources().getDimension(R.dimen.small_carousel_width);


        String imageUrl = getImageUrl(mActivity, item.getImageUrl(),  CJRConstants.IMAGE_TYPE_HOMEPAGE,
                imageWidth, imageHeight);
        if (imageUrl != null) {
            //viewHolder.progressbar.setVisibility(View.VISIBLE);

            Picasso.with(mActivity.getApplicationContext())
                    .load(imageUrl)
                    .into(viewHolder.contentThumbnail);

        } else {
            //viewHolder.progressbar.setVisibility(View.GONE);
        }


        if (!item.isItemViewed()) {
            item.setItemViewed();
            if(mcontainerInstanceId!=null) item.setmContainerInstanceID(mcontainerInstanceId);
            else item.setmContainerInstanceID("");

        }
        return convertView;
    }



    public static final String IMAGE_FORMAT_WEBP = ".webp";

    private static String createImageUrl(String imageUrl, String resolution) {
        if (imageUrl == null)
            return null;
        StringBuilder image = new StringBuilder(imageUrl);
        image.insert(image.lastIndexOf("/") + 1, resolution);

        if (WebPUtils.isWebPSupported()) {
            image.append(IMAGE_FORMAT_WEBP);
        }
        return image.toString();
    }


    public String getImageUrl(Context context, String defaultImageUrl,
                              int imageType, int width, int height) {
        width = Math.abs(width);
        height = Math.abs(height);

        StringBuilder imageDimension = new StringBuilder();
        imageDimension.append(width).append("x").append(height).append("/");

//		if(CJRConstants.IMAGE_TYPE_HOMEPAGE==imageType){
//			return createImageUrlJPG(defaultImageUrl, imageDimension.toString());
//		}

        return createImageUrl(defaultImageUrl, imageDimension.toString());

		/*
         * switch (imageType) { case CJRConstants.IMAGE_TYPE_CAROUSEL_1: return
		 * createImageUrl(defaultImageUrl, getCarousel1ImageParam(activity));
		 *
		 *
		 * case CJRConstants.IMAGE_TYPE_CAROUSEL_2: return
		 * createImageUrl(defaultImageUrl, getCarousel2ImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_PRODUCT_ROW: return
		 * createImageUrl(defaultImageUrl, getProductRowImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_HEADER: return
		 * createImageUrl(defaultImageUrl, getHeaderImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_GRID: return
		 * createImageUrl(defaultImageUrl, getGridImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_PRODUCT_DETAIL: return
		 * createImageUrl(defaultImageUrl,
		 * getProductDetailImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_FOOTER: return
		 * createImageUrl(defaultImageUrl, getFooterImage(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_MERCHANT_LOGO: return
		 * createImageUrl(defaultImageUrl, getMerchantLogo(activity));
		 *
		 * default: return defaultImageUrl; }
		 */
    }


    public ArrayList<CJRHomePageItem> getData() {
        return mItemList;
    }

    public void clearData() {
        if (mItemList != null) {
            mItemList.clear();
            notifyDataSetChanged();
        }
    }


    public static class ViewHolder {
        ImageView contentThumbnail;
        ProgressBar progressbar;
    }

}
