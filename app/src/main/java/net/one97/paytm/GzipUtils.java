package net.one97.paytm;

import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.zip.GZIPInputStream;

/**
 * Created by Rama on 6/20/2016.
 */
public class GzipUtils {


    public static Reader convertReader(byte[] jsonBytes) {
        Reader reader=null;
        try {
            GZIPInputStream gStream = new GZIPInputStream(new ByteArrayInputStream(jsonBytes));
            reader = new InputStreamReader(gStream);
        }catch (Exception e){
            e.printStackTrace();
        }
        return reader;
    }


    public static String convertString(Reader reader) {
        StringBuilder jsonString = new StringBuilder();
        try {
            final BufferedReader in = new BufferedReader(reader);
            String read;
            while ((read = in.readLine()) != null) {
                jsonString.append( read ).append( "\n" );
            }
            reader.close();
            in.close();
        } catch (IOException e) {
            Log.e("VolleyUtils", e.getMessage());
        }
        return jsonString.toString();
    }

}
