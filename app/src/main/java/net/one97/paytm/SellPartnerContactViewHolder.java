package net.one97.paytm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Rama on 4/25/2016.
 */
public class SellPartnerContactViewHolder extends RecyclerView.ViewHolder implements UpdateViewHolder {

    Context mContext;
    private TextView mSellOn;
    private TextView mPartnerWith;
    private TextView mContactUs;
    private TextView mOurBlog;

    public SellPartnerContactViewHolder(Context mContext, View convertView) {
        super(convertView);
        this.mContext=mContext;
        mSellOn = (TextView) convertView.findViewById(R.id.tv_sell_on);
        mPartnerWith = (TextView) convertView.findViewById(R.id.tv_partner);
        mContactUs = (TextView) convertView.findViewById(R.id.tv_contact_us);
        mOurBlog = (TextView) convertView.findViewById(R.id.tv_our_blog);
    }

    @Override
    public void updateViewHolder(Context context, final CJRHomePageLayoutV2 pageLayout, final String
            mGAKey) {

        mSellOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        mPartnerWith.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        mContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        mOurBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }
}
