package net.one97.paytm;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by krishna on 13/7/16.
 */
public class CJRSelectCityModel implements IJRDataModel {

    private static final long serialVersionUID = 1L;
     @SerializedName("label")
    private String mLabel;
    @SerializedName("value")
    private String mValue;
    @SerializedName("isTopCity")
    private boolean mIsTopCity;
    @SerializedName("image")
    private String mCityImageUrl;
    @SerializedName("searchKeys")
    private List<String> mSeachKeys;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("longitude")
    private String longitude;

    private String mViewType;

    public String getLabel() {
        return mLabel;
    }

    public void setlabel(String mlabel) {
        this.mLabel = mlabel;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String mValue) {
        this.mValue = mValue;
    }

    public boolean isTopCity() {
        return mIsTopCity;
    }

    public void setIsTopCity(boolean mIsTopCity) {
        this.mIsTopCity = mIsTopCity;
    }

    public String getmCityImageUrl() {
        return mCityImageUrl;
    }

    public void setmCityImageUrl(String mCityImageUrl) {
        this.mCityImageUrl = mCityImageUrl;
    }

    public List<String> getmSeachKeys() {
        return mSeachKeys;
    }

    public void setmSeachKeys(List<String> mSeachKeys) {
        this.mSeachKeys = mSeachKeys;
    }

    public String getmViewType() {
        return mViewType;
    }

    public void setmViewType(String mViewType) {
        this.mViewType = mViewType;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
