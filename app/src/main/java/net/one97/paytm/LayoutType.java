package net.one97.paytm;

/**
 * Created by Rama on 4/25/2016.
 */
public enum LayoutType {

    LAYOUT_PRODUCT_ROW(100, "row"),         //min index
    LAYOUT_CAROUSEL_1(101, "carousel_1"),
    LAYOUT_CAROUSEL2(102, "carousel-2"),
    LAYOUT_CAROUSEL_2(103, "carousel_2"),
    LAYOUT_TEXT_LINKS(109, "textlinks"),
    LAYOUT_HORIZONTAL_TEXT_LIST(110, "horizontal_text_list"),
    LAYOUT_VIEW_PAGER(111, "view_pager"),
    LAYOUT_WALLET_OPTIONS(112, "wallet_option"),
    LAYOUT_FOOTER_SOCIAL_ICONS(113, "footer_view"),
    LAYOUT_SMART_LIST(114, "smart-icon-list"),
    LAYOUT_GAMES_HOME_PAGE_INFO(115, "games_apps_info_text"),
    LAYOUT_SUCCESS_CARD(116,"success_card"),
    LAYOUT_PENDING_CARD(117,"pending_card"),
    LAYOUT_FAILURE_CARD(118,"failure_card"),
    LAYOUT_AUTO_CARD(119,"auto_card"),
    LAYOUT_FOOTER_CARD(120,"footer_card"),
    LAYOUT_PAYMENT_CARD(121,"payment_card"),
    LAYOUT_IGNORE_TYPE(-1,"ignore_type"),
    LAYOUT_CAROUSEL_4(200, "carousel_4"),
    LAYOUT_CAROUSEL4(206, "carousel-4"),
    LAYOUT_CAROUSEL_5(201, "carousel_5"),
    LAYOUT_CAROUSEL5(216, "carousel-5"),
    LAYOUT_SNIPPET_1(317, "snippets-grid"),
    LAYOUT_SNIPPET_2(318,"photo-carousel-featured"),
    LAYOUT_PRODUCT_GRID(319,"product-list-grid"),
    LAYOUT_VIDEO_COROUSEL(320,"video-carousel-grid"),
    LAYOUT_PHOTO_COROUSEL(321,"photo-carousel-grid"),
    LAYOUT_CATEGORY_COROUSEL(322,"category-carousel"),
    LAYOUT_CAROUSEL_ROW(323,"carousel-row"),
    LAYOUT_BRAND_PRODUCT_ROW(324,"product-list-row"),
    LAYOUT_CAROUSEL_FULL_WIDTH(325,"carousel-full-width"),
    LAYOUT_HEADER_CARD(217,"summary_header_card"),
    LAYOUT_TRAIN_DESC_CARD(218,"train_summary_desc_card"),
    LAYOUT_FOOTER_SELL_PARTNER_CONTACT_ICONS(219, "sell_partner_contact_icons"),

    //new widgets

    //Banner Widgets

    LAYOUT_H1_BANNER(500, "h1-banner"),
    LAYOUT_CAROUSEL1(501, "carousel-1"),

    LAYOUT_SQUARE_BANNER(503, "c1-square-banner"),
    LAYOUT_BANNER_2XN(504, "banner-2xn"),
    LAYOUT_BANNER_3XN(505, "banner-3xn"),
    LAYOUT_COLLAGE_5X(506, "collage-5x"),
    LAYOUT_COLLAGE_3X(507, "collage-3x"),
    LAYOUT_THIN_BANNER(508, "thin-banner"),


    //Product Widgets

    LAYOUT_PRODUCT_ROW_1XN(509, "row-1xn"),
    LAYOUT_PRODUCT_ROW_2XN(510, "row-2xn"),
    LAYOUT_PRODUCT_ROW_3XN(511, "row-3xn"),

    //Category Widgets

    LAYOUT_ACCORDION_VIEW(512, "accordion_menu"),
    LAYOUT_SMART_ICON_GRID(513, "smart-icon-grid"),

    //Header

    LAYOUT_HEADER_GRID(514, "grid_header"),

    //Footer

    LAYOUT_FOOTER_VIEW_MORE(517, "grid_footer"), //MAX INDEX
    //Divider

    LAYOUT_DIVIDER_LINE(515, "grid-divider-thin"),
    LAYOUT_DIVIDER_GAP(516, "grid-divider-gap");


    private int index;
    private String name;

    LayoutType(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public static LayoutType fromIndex(int index) {
        for (LayoutType type : LayoutType.values()) {
            if (type.index == index) {
                return type;
            }
        }
        return LAYOUT_IGNORE_TYPE;
    }

    public static LayoutType fromName(String name) {
        for (LayoutType type : LayoutType.values()) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return LAYOUT_IGNORE_TYPE;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public boolean isNewType() {
        return index >= 500 && index <= 517 ;
    }
}
