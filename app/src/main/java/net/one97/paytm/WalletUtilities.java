package net.one97.paytm;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by pankajtiwari on 22/7/16.
 */
public class WalletUtilities {
    private static volatile WalletUtilities utilityInstance;

    private WalletUtilities() {

    }





    /**
     * double locking singleton pattern
     * @return
     */
    public static WalletUtilities getwalletUtilityInstance() {
        if (null == utilityInstance) {
            synchronized (WalletUtilities.class) {
                if (null == utilityInstance) {
                    utilityInstance = new WalletUtilities();
                }
            }
        }
        return utilityInstance;
    }


    /**
     * convert string array to string arraylist
     *
     * @param stringarray array of string
     * @return ArrayList of string
     */
    public ArrayList<String> getstringArrayList(String[] stringarray) {
        ArrayList<String> stringarrayList = new ArrayList<>();

        try {
            stringarrayList = new ArrayList(Arrays.asList(stringarray));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringarrayList;
    }

    public  boolean isVersionMarshmallowAndAbove() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
    }

    public static boolean isMoneyTransferTabVisible(Context mContext) {
        //return GTMLoader.getInstance(mContext).getBooleanFromContainer2(ConstantServiceApi.KEY_IS_MONEY_TRANSFER_ENABLE,false);
        return true;
    }

    public static boolean isPayTabVisible(Context mContext) {
        //return GTMLoader.getInstance(mContext).getBooleanFromContainer2(ConstantServiceApi.KEY_IS_PAY_ENABLE,true);
        return true;
    }

    public static boolean isSavingAccVisible(Context mContext) {
      //  return GTMLoader.getInstance(mContext).getBooleanFromContainer2(ConstantServiceApi.KEY_IS_SAVING_ACC_ENABLE,false);
        return true;
    }

    public static String contactExists(Context context, String number) {
        try {
            /// number is the phone number
            Uri lookupUri = Uri.withAppendedPath(
                    ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                    Uri.encode(number));
            String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME};
            Cursor cur = context.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
            try {
                if (cur.moveToFirst()) {
                    return "1";
                }
            } finally {
                if (cur != null)
                    cur.close();
            }
            return "0";
        } catch (Exception e) {
            return "2";
        }
    }

}
