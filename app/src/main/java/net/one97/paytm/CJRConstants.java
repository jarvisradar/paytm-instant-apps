package net.one97.paytm;

import java.util.HashMap;
import java.util.Map;

public class CJRConstants {

    public static final int QR_TYPE_DYNAMIC = 2;
    public static final int QR_TYPE_STATIC = 1;
    public static final int QR_TYPE_SHOWCODE = 3;
    public static final int QR_TYPE_WEB_LOGIN = 4;

    public static final String EMPTY_STRING = "";
    public static final String COLON = ":";
    public static final String SPACE = " ";
    public static final String STAGING_SERVER = "Staging";
    public static final String DEVELOPMENT_SERVER = "Development";
    public static final String PRODUCTION_SERVER = "Production";
    // public static final String URL_SERVER_LIST =
    // "https://catalogapidev.paytm.com/services";
    public static final String URL_TYPE_FEATURED = "featured";
    public static final String EXTRA_INTENT_RESULTANT_FRAGMENT_POSITION = "resultant fragment " +
            "position";

    public static final String EXTRA_INTENT_ITEM_POSITION_FROM_CART = "item_position_from_cart";

    public static final String EXTRA_DATA = "extra_data";


    public static final String SHOW_NEW_UPDATES = "show_new_updates";

    //dropbox server
    //production
    //public static final String URL_SERVER_LIST = "https://dl.dropboxusercontent
    // .com/u/172568539/BusTicket/Production/server_list.json";
    //staging
    //public static final String URL_SERVER_LIST = "https://dl.dropboxusercontent
    // .com/u/172568539/BusTicket/Staging/server_list.json";

    //paytm server

    public static final String VIEW_TYPE = "view_type_brand";
    public static final String URL_TYPE_SEE_ALL = "See All View";
    public static final String EXTRA_IS_FROM_SMART_LIST = "smart_list";
    public static final String PAYTM_HOME = "Paytm Home";
    public static final String EXTRA_INTENT_ITEM = "extra_home_data";
    public static final String EXTRA_CURRENT_SERVER = "current_server";
    public static final String EXTRA_VALUE = "extra_value";
    public static final String EXTRA_CURRENT_CATALOG = "current_catalog";
    public static final String EXTRA_IS_CANCEL = "is_cancel";
    public static final String EXTRA_PG_SCREEN = "pg_screen";
    public static final String EXTRA_IS_PAYMENT = "is_payment";
    public static final String EXTRA_INTENT_ITEM_LIST = "extra_intent_item_list";
    public static final String EXTRA_INTENT_ITEM_POSITION = "extra_intent_item_position";
    public static final String EXTRA_INTENT_URL = "url";
    public static final String EXTRA_ORIGIN = "origin";
    public static final String EXTRA_INTENT_BACK_PAGE = "calling activity";
    public static final String EXTRA_INTENT_IS_FROM_SEARCH = "is_from_search";
    public static final String EXTRA_INTENT_DO_NOT_MODIFY_URL = "modify_url";
    public static final String EXTRA_INTENT_IS_FROM_GRID = "is_from_grid";
    public static final int TREE_VIEW_LEVEL_NUMBER = 4;
    public static final String EXTRA_INTENT_OFFER_URL = "offers url";
    public static final String EXTRA_INTENT_TITLE = "title";
    public static final String EXTRA_INTENT_RESULTANT_ACTIVITY = "resultant activity";
    public static final String EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE = "resultant fragment type";
    public static final String EXTRA_INTENT_CART_DATA = "cart_data";
    public static final String EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE = "resultant activity_bundle";
    public static final String EXTRA_INTENT_HOME_PAGE_TAB_ITEMS = "home_page_tab_item";
    public static final String EXTRA_INTENT_HEADER_EDITABLE = "header_editable";
    public static final String EXTRA_PRODUCT_NAME = "extra_product_name";
    public static final String EXTRA_PRODUCT_SUB_TEXT = "extra_product_sub_text";
    public static final String EXTRA_PRODUCT_ICON_URL = "extra_icon_url";
    public static final String EXTRA_REPLACE_TITLE = "extra_replace_title";
    public static final String EXTRA_INTENT_IS_FROM_REQ_DELIVERY = "is_from_req_delivery";

    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET_CODE = "client_secret";
    // public static final String VALUE_CLIENT_ID = "market-app";
    // public static final String VALUE_CLIENT_SECRET_CODE =
    // "9a071762-a499-4bd9-914a-4361e7c3f4bc";
    public static final String CART_ID = "cart_id";
    public static final String RECENT_SELECTED_PROMOCODE = "recentpromocode";
    public static final String RECENT_SELECTED_PROMOCODE_MESSAGE = "recentpromocodemessage";
    public static final String SESSSION_TOKEN = "session_token";
    public static final int PICK_CONTACT = 100;
    public static final int PICK_SEND_MONEY_CONTACT = 101;
    public static final int PICK_REQUEST_MONEY_CONTACT = 102;
    public static final int REQUEST_CODE_WALLET_UPDATES_ACTIVITY = 103;
    public static final int REQUEST_CODE_WALLET_PG = 104;
    public static final int REQUEST_CODE_QR_SCAN = 105;
    public static final int REQUEST_CODE_GET_IFSC_CODE = 106;
    public static final int REQUEST_CODE_GET_BANK_CODE = 107;
    public static final int REQUEST_CODE_GET_STATE_CODE = 108;
    public static final int REQUEST_CODE_GET_CITY_CODE = 109;
    public static final int REQUEST_CODE_GET_BRANCH_CODE = 110;
    public static final int REQUEST_CODE_TRAIN_APPLY_PROMO = 111;
    public static final int REQUEST_CODE_ACCEPT_MONEY_MODE = 121;
    public static final int REQUEST_CODE_ACCEPT_MONEY_FILL_AMT = 122;
    public static final int PROFILE_ADHAR_PAN_UPDATE = 312;

    public static final int MOBILE_NUMBER_LENGTH = 10;
    public static final int START_INDEX_ZERO = 0;
    public static final int START_INDEX_ONE = 1;
    public static final int START_INDEX_TWO = 2;
    public static final int START_INDEX_THREE = 3;


    public static final String BRAND_IMAGE_URL = "brand_image_url";

    public static final String SEARCH_URL = "search.html?userQuery=";
    public static final String MY_ORDERS = "myOrders?";
    public static final String CART_URL = "cart.html?";
    public static final String EXPRESS_CART = "expresscart.html?";
    public static final String PLACE_ORDER = "placeorder.html?";
    public static final String PAYTM_CASH = "paytmcash.html?";
    public static final String GET_MESSAGES = "getMessages.html?";
    public static final String ADD_TOKEN = "addtoken.html?";
    public static final String DELETE_TOKEN = "deletetoken.html?";

    public static final String VIEW_HTML = "/view.html?";
    // public static final String AUTH_TOKEN_BODY =
    // "&redirect_uri=https%3A%2F%2Fwww.paytm
    // .com&client_id=market-app&client_secret=9a071762-a499-4bd9-914a-4361e7c3f4bc&scope=paytm
    // &grant_type=authorization_code";
    // public static final String AUTHORIZE_BODY =
    // "response_type=code&client_id=market-app&do_not_redirect=true&redirect_uri=http://www
    // .paytm.com&scope=paytm&username=";
    public static final String AUTH_TOKEN_BODY = "&scope=paytm&grant_type=authorization_code";
    // public static final String AUTHORIZE_BODY =
    // "response_type=code&do_not_redirect=true&redirect_uri=http://www.paytm
    // .com&scope=paytm&client_id=";
    // public static final String AUTHORIZE_DEFAULT_PARAMS =
    // "?response_type=code&client_id=market-app&scope=paytm&redirect_uri=https://www.paytm.com";
    public static final String AUTHORIZE_REDIRECT_URL = "https://www.paytm.com/?code";

    // public static final String AUTHORIZATION_VALUE =
    // "Basic bWFya2V0LWFwcDo5YTA3MTc2Mi1hNDk5LTRiZDktOTE0YS00MzYxZTdjM2Y0YmM=";
    public static final String APPLICATION_URLENCODED = "application/x-www-form-urlencoded";
    public static final String AUTHORIZATION = "Authorization";
    public static final String USER_TOKEN = "User-Token";
    public static final String REQUEST_TOKEN = "Request-Token";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String USER_AGENT = "User-Agent";
    public static final String DATA = "data";
    public static final String SUCCESS = "SUCCESS";
    public static final String JSON_STRING_PREPAID = "json_string_prepaid";
    public static final String JSON_STRING_POSTPAID = "json_string_postpaid";
    public static final String URL_TYPE_JSON_PREPAID = "url_type_jspn_prepaid";
    public static final String URL_TYPE_JSON_POSTPAID = "url_type_jspn_postpaid";
    public static final String SSO_TOKEN = "sso_token=";
    public static final String WALLET_SSO_TOKEN = "wallet_sso_token=";
    public static final String WALLET_TOKEN_EXPIRE = "wallet_token_expire=";
    public static final String SHARED_PREFFERENCE_FIL_NAME = "javis";
    public static final String TRANS_PATTERN = "http://Trans:";
    public static final String PG_CALL_BACK = "http://trans-";
    public static final String PG_CALL_BACK_NULL = "http://trans-null/";
    public static final String PG_CALL_BACK_ZERO = "http://trans-0/";
    public static final String CONTINGENCY_CALL_BACK = "http://cta";
    public static final String CONTINGENCY_CALL_BACK_SECURE = "https://cta";
    public static final String RECHARGE_PAYMENT_INFO = "Recharge_Payment_info";
    public static final String GTM_TRACKING_INFO = "tracking_info";
    public static final String MAIL_CALL_BACK = "mailto:care@paytm.com";
    public static final String HOME_URL = "home_url";
    public static final String OPEN_TAB = "openTab";

    public static final String B2C_ANDROID = "B2C_ANDROID";
    public static final String APPLICATION_JSON = "application/json";
    public static final String PRODUCT_ID = "product_id";
    public static final String PRICE = "price";
    public static final String CUSTOM_FIELD_1 = "custom_field_1";
    public static final String CUSTOM_FIELD_2 = "custom_field_2";
    public static final String CUSTOM_FIELD_3 = "custom_field_3";
    public static final String PROMOCODE = "promocode";
    public static final String PROMOTEXT = "promotext";

    public static final int OPERATOR_REQ_CODE = 1;
    public static final int SIGN_REQ_CODE = 3;
    public static final int SETFAV_SIGN_REQ_CODE = 4;
    public static final int ZOOM_PDP_IMAGE_REQ = 5;
    public static final int ADD_TO_WISHLIST = 6;

    public static final int WALLET_SIGN_REQ_CODE = 301;
    public static final int UPDATE_SIGN_REQ_CODE = 302;

    public static final int INDICATIVE_PLANS_REQ_CODE = 2;
    public static final long ANIM_VIEWPAGER_DELAY = 3000;
    public static final int READ_DATA_MODEL_OBJECT_FROM_FILE = 111;
    public static final int WRITE_DATA_MODEL_OBJECT_TO_FILE = 112;
    public static final int DELETE_DATA_MODEL_OBJECT__FILE = 113;
    public static final int REQUEST_CODE_SELECT_DATE = 200;
    public static final int REQUEST_CODE_SOURCE_CITY = 201;
    public static final int REQUEST_CODE_DESTINATION_CITY = 202;
    public static final int REQUEST_CODE_TRAVELLERS_DETAILS = 205;
    public static final int REQUEST_CODE_CLASS_SELECTION = 211;
    public static final int REQUEST_CODE_SELECT_RETURN_DATE = 204;
    public static final int REQUEST_CODE_WISH_LIST = 203;
    public static final int REQUEST_CODE_SELLER_SELECTION = 206;
    public static final int REQUEST_CODE_LANGUAGE_SELECTION = 206;
    public static final int REQUEST_CODE_SECURITY_ENABLE_PAYSEND = 207;
    public static final int REQUEST_CODE_SECURITY_ENABLE_PASSBOOK = 208;
    public static final int REQUEST_CODE_APP_FROM_BACKGROUND = 210;
    public static final int REQUEST_CHECK_SETTINGS = 220;


    public static final int CART_ITEM_UPDATION = 111;
    public static final int CART_ITEM_DELETION = 112;
    public static final int CART_ITEM_ADD = 110;
    public static final int DEAL_DETAIL = 549;
    public static final int DEAL_NOT_ADDED = 849;
    public static final int FREE_PAID_DEAL = 603;
    public static final int CST_RECENT_ORDERS = 1000001;
    public static final int CST_ADD_MONEY_TAB = 1000002;
    public static final int CST_PASSBOOK_ORDERS = 1000003;
    public static final int CST_LOGIN_ISSUES = 1000005;
    public static final int CST_PRIVACY_SECURITY_ISSUES = 1000006;
    public static final int CST_NEAR_BY_ORDERS = 1000008;
    public static final int CST_PAYMENT_BANK = 1000010;
    public static final int CST_SAVING_BANK_ACCOUNT = 1000004;


    public static final String LAYOUT_HOME_PAGE_CAROUSEL_1 = "carousel_1";
    public static final String LAYOUT_HOME_PAGE_CAROUSEL1 = "carousel-1";
    public static final String LAYOUT_HOME_PAGE_CAROUSEL_2 = "carousel_2";
    public static final String LAYOUT_HOME_PAGE_CAROUSEL2 = "carousel-2";

    public static final String REQUEST_PAYSEND_PASSBOOK = "paysend";
    /*
     * public static final String LAYOUT_HOME_PAGE_TRENDING = "Trending"; public
     * static final String LAYOUT_HOME_PAGE_FEATURED = "Featured";
     */
//    public static final String LAYOUT_HOME_PAGE_PRODUCT_ROW = "row";
    public static final String LAYOUT_HOME_PAGE_CATALOG = "catalog";
    //    public static final String LAYOUT_HOME_PAGE_TEXT_LINK = "textlinks";
//    public static final String LAYOUT_HOME_PAGE_TEXT_LINK2 = "textlinks2";
    public static final String LAYOUT_HOME_PAGE_HTML = "html";
    public static final String LAYOUT_HOME_PAGE_TABS = "tabs";
    public static final String LAYOUT_HOME_PAGE_PRODUCT_ROW = "row";
//    public static final String LAYOUT_HOME_PAGE_HORIZONTAL_TEXT_LIST = "horizontal_text_list";
//    public static final String LAYOUT_HOME_VIEW_PAGER = "pager";
//    public static final String LAYOUT_HOME_PAGE_FOOTER = "footer_home_page";
//    public static final String LAYOUT_GAMES_HOME_PAGE_INFO_TEXT = "games_apps_info_text";

    public static final String URL_TYPE_GRID = "grid";
    public static final String URL_TYPE_SMART_LIST = "smart_list";
    public static final String URL_TYPE_LIST = "list";
    public static final String URL_TYPE_PRODUCT = "product";
    public static final String URL_TYPE_HOME_PAGE = "homepage";
    public static final String URL_TYPE_SECONDARY_HOME_PAGE = "homepage_secondary";
    public static final String URL_TYPE_MOBILE_PREPAID_1 = "mobile_prepaid";
    public static final String URL_TYPE_MOBILE_POSTPAID_1 = "mobile_postpaid";
    public static final String URL_TYPE_DATACARD_PREPAID_1 = "datacard_prepaid";
    public static final String URL_TYPE_DATACARD_POSTPAID_1 = "datacard_postpaid";
    public static final String URL_TYPE_MOBILE_PREPAID_2 = "mobile-prepaid";
    public static final String URL_TYPE_MOBILE_POSTPAID_2 = "mobile-postpaid";
    public static final String URL_TYPE_DATACARD_PREPAID_2 = "datacard-prepaid";
    public static final String URL_TYPE_DATACARD_POSTPAID_2 = "datacard-postpaid";
    public static final String URL_TYPE_DTH_1 = "dth";
    public static final String URL_TYPE_DTH_2 = "DTH";
    public static final String URL_TYPE_TOLLCARD = "tollcard";
    public static final String URL_TYPE_SEARCH = "search";
    public static final String URL_TYPE_EMBED = "embed";
    public static final String URL_TYPE_AE_EMBED = "ae_embed";
    public static final String URL_TYPE_EXTERNAL = "external";
    public static final String URL_TYPE_RECHARGES = "recharges";
    public static final String URL_TYPE_LANDLINE = "landline";
    public static final String URL_TYPE_BROADBAND = "broadband";
    public static final String URL_TYPE_ORDER_SUMMARY = "order_summary";
    public static final String URL_TYPE_HOTEL_ORDER_SUMMARY = "hotel_order_summary";
    public static final String URL_TYPE_TRAIN_ORDER_SUMMARY = "train_order_summary";
    public static final String URL_TYPE_FLIGHT_ORDER_SUMMARY = "flight_order_summary";
    public static final String URL_TYPE_MY_ORDER = "my_orders";
    public static final String URL_TYPE_CART = "cart";
    public static final String URL_TYPE_PAYTM_CASH_LEDGER = "cash_ledger";
    public static final String URL_TYPE_PAYTM_P2M_SCAN = "scan_p2m";
    public static final String URL_TYPE_PAYTM_REQUEST_MONEY = "request_money_dl";
    public static final String URL_TYPE_UPGRADE_WALLET = "upgrade_wallet";
    public static final String URL_TYPE_PAYTM_SCANNER = "scanner";
    public static final String URL_TYPE_PAYTM_SCANNER_ONLY = "scanner_only";
    public static final String URL_TYPE_CASH_WALLET = "cash_wallet";
    public static final String URL_TYPE_PROFILE = "profile";
    public static final String URL_TYPE_MY_PROFILE = "myprofile";

    public static final String URL_TYPE_ELECTRICITY = "electricity";
    public static final String URL_TYPE_ELECTRICITY_BOARDS = "electricity boards";
    public static final String DEEP_LINKING_URL = "deep_linking_url";
    public static final String URL_TYPE_GAS = "gas";
    public static final String URL_TYPE_WATER = "water";
    public static final String URL_TYPE_INSURANCE = "insurance";
    public static final String URL_TYPE_GOOGLE_PLAY = "google play";
    public static final String URL_TYPE_TOLL = "toll";
    public static final String URL_TYPE_TOLL_TAG = "toll tag";
    public static final String URL_TYPE_APARTMENTS = "apartments";


    public static final String URL_TYPE_EDUCATION = "education";
    public static final String URL_TYPE_EDUCATION_MAIN = "Education";
    public static final String URL_TYPE_FEE = "Fee";
    public static final String URL_TYPE_FEE_LOWER_CASE = "fee";
    public static final String URL_TYPE_SCHOOL_FEE = "School Fee";
    public static final String URL_TYPE_FEES = "fees";

    public static final String URL_TYPE_BUSTICKET = "busticket";
    public static final String URL_TYPE_CABBOOKING = "cabbooking";
    public static final String PACKAGE_INFO_UBER = "com.ubercab";

    public static final String UTILITY_ELECTRICITY_ASSETS = "utility_electricity_response.json";
    public static final String UTILITY_EDUCATION_ASSETS = "utility_education_response.json";
    public static final String UTILITY_GAS_ASSETS = "utility_gas_response.json";
    public static final String UTILITY_INSURANCE_ASSETS = "utility_insurance_response.json";
    public static final String UTILITY_LANDLINE_ASSETS = "utility_landline_response.json";
    public static final String UTILITY_METRO_ASSETS = "utility_metro_response.json";
    public static final String UTILITY_WATER_ASSETS = "utility_water_response.json";
    public static final String UTILITY_LOAN_ASSETS = "utility_loan_response.json";
    public static final String UTILITY_GOOGLE_PLAY_ASSETS = "utility_google_play_response.json";
    public static final String UTILITY_TOLLS_ASSETS = "utility_tolls_response.json";
    public static final String UTILITY_CHALLAN_ASSETS = "utility_challan_response.json";
    public static final String UTILITY_APARTMENTS_ASSETS = "utility_apartments_response.json";


    public static final String URL_TYPE_GAMES_AND_APPS = "gamesandapp";
    public static final String URL_TYPE_GAMES_APPS_INMOBI = "inmobi_gamesandapp";
    public static final String URL_TYPE_GAMES_APPS_EXTERNAL = "external";
    public static final String GAMES_APPS_KEY_DEVICE_ID = "devid";
    public static final String GAMES_APPS_KEY_PAYTM_ID = "paytmid";

    public static final String URL_TYPE_SAVED_CARD = "saved_card";
    public static final String URL_TYPE_ADD_CARD = "add_card";
    public static final String ORIGIN_DEEP_LINKING = "deeplinking";
    public static final String ORIGIN_PUSH_NOTIFICATION = "pushnotification";
    public static final String ORIGIN_SLIDE_CART = "slide_cart";
    public static final String ORIGIN_SELLER_BRAND_STORE = "seller_brand_store";

    public static final String SUBFOLDER_JARVIS = "/paytm";
    public static final String FILE_HEADER_SERVER_LIST = "server_list";
    public static final String FILE_CATALOG_LIST = "flyout_catalog_list";
    public static final String FILE_HEADER_OPERATOR_LIST = "operator";
    public static final String FILE_HEADER_FREQUENT_ORDER_LIST = "frequent_order_list";
    public static final String FILE_HEADER_HELP_SERVICE_VIDEO_LIST = "help_service_video_list";
    public static final String FILE_HEADER_FAVORITE_NUMBER_LIST = "favorite_number_list";
    public static final String FILE_HEADER_LEDGER_LIST = "ledger_list";
    public static final String FILE_HEADER_CATEGORIES = "categories_list";
    public static final String FILE_TAB_LIST = "tab_menu";
    public static final String FILE_HEADER_SELLER_RATING_LIST = "seller_rating_list";

    public static final String FILE_HOME_DATA = "homepage_layout";

    public static final String FILE_SERVER_LIST = "server_list.json";
    public static final String HOME_DATA_FROM_ASSETS = "home_page_response.json";
    public static final String CATALOG_FROM_ASSETS = "catalog_response.json";
    //public static final String TAB_FROM_ASSETS = "tab_menu_response.json";
    public static final String TWOG_DATA_ASSECTS = "twog_response.json";


    public static final String KEY_PASSWORD = "password";
    public static final String KEY_USERNAME = "username";
    public static final String IP_ADDRESS = "ip_address";
    public static final String IOS_APP_IP_ADDRESS = "iOSAppIPAddress";
    public static final String SOURCE_TYPE = "source_type";
    public static final String WEB = "WEB";
    public static final String LOGIN_TYPE = "login_type";
    public static final String PAYTM = "paytm";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_DATACARD = "datacard";
    public static final String KEY_EMAIL = "email";
    public static final String RECHARGE_TYPE_PREPAID = "prepaid";
    public static final String RECHARGE_TYPE_POSTPAID = "postpaid";
    // public static final String KEY_USER_ID ="user id";

    public static final String PAYTM_APP_KEY = "X_PAYTMAPP_KEY";
    public static final String PAYTM_APP_KEY_VALUE = "9dff6af8c6506fb63116f5139c0f6941";
    public static final String PAYTM_APP_USERNAME = "X_PAYTMAPP_USERNAME";
    public static final String PAYTM_APP_USERNAME_VALUE = "paytm_androidapp";

    public static final String KEY_OPERATOR = "operator";
    public static final String KEY_CIRCLE_INDEX = "Circle_index";
    public static final String KEY_CIRCLE = "circle";
    public static final String KEY_TYPE = "Type";

    public static final String TAG_FRAGMENT_CART = "fragment_cart";
    public static final String TAG_FRAGMENT_PROFILE = "fragment_profile";
    public static final String TAG_FRAGMENT_ADDRESS = "fragment_address";
    public static final String TAG_FRAGMENT_PAYMENT = "fragment_payment";
    public static final String TAG_FRAGMENT_SIGN_IN = "fragment_sign_in";
    public static final String TAG_FRAGMENT_SIGN_UP = "fragment_sign_up";
    public static final String KEY_PAYMENT = "payment";
    public static final String BACK = "Back";
    public static final String SELECT_OPERATOR = "Select Operator";
    public static final String OPERATOR_URL_TYPE = "operator_url_type";
    public static final String OPERATOR_URL = "operator_url";
    public static final String UTILITY_OPERATOR_LIST = "utility_operator_list";

    public static final String MOB_NO_REG_EX = "^([7,8,9]{1}+[0-9]{9})$";
    public static final String MOB_NO_REG_EX_5_DIGIT = "^([7,8,9]{1}+[0-9]{4})$";
    public static final String PAYTM_URL_REGEX = "(https?:\\/\\/(?:www\\.|(?!www))(([a-zA-Z]*\\.)|())(paytm.com)|www\\.(paytm.com))";
    public static final String GENERAL_URL_REGEX = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?(\\?.+)?";//"((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.([\\w\\d\\-_]+\\.)?\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)(\\?.+)?";

    public static final String STATUS_SUCCESS = "SUCCESS";
    public static final String STATUS_FAILED = "FAILED";
    public static final String STATUS_PAYMENT_FAILED = "PAYMENT_FAILED";
    public static final String STATUS_PAYMENT_PENDING = "PAYMENT_PENDING";
    public static final String STATUS_PENDING = "PROCESSING";
    public static final String ORDER_STATUS_PENDING = "PENDING";
    public static final String STATUS_BILL_CANCELLED = "BILL_CANCELLED";
    public static final String STATUS_PAYMENT_CANCELLED = "PAYMENT_CANCELLED";
    public static final String EXTRA_INTENT_NEW_ADDRESS = "new_address";
    public static final int REQUEST_CODE_ADD_NEW_ADDRESS = 7;
    public static final String EXTRA_SERIALIZABLE_SHIPPING_ADDRESS = "shipping_address";
    public static final String EXTRA_SERIALIZABLE_CART_ITEM = "cart_item";
    public static final String EDIT_CART = "EDIT CART";
    public static final String EDIT_ADDRESS = "EDIT ADDRESS";
    public static final String EDIT_DONE = "Done";
    public static final String ALL = "all";
    public static final String ORDER_TYPE_RECHARGE = "recharge";
    public static final String ORDER_TYPE_SHOPPING = "shopping";
    public static final String ORDER_TYPE_TICKETS = "alltickets";
    public static final String ORDER_TYPE_TRAVEL = "travel";

    public static final String EXTRA_INTENT_PARENT_ACTIVITY = "parent_activity";
    public static final String EXTRA_INTENT_TEXT = "text";

    public static final String AMOUNT_REG_EX = "^([1-9]{1}+[0-9]{0,MAX_FROM_SERVER})$";

    public static final String POPULAR_RECHARGE_MOBILE_TYPE = "mobile";
    public static final String POPULAR_RECHARGE_DATA_CARD_TYPE = "datacard";
    public static final String POPULAR_RECHARGE_DTH_TYPE = "dth";

    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_MOBILE_NUMBER = "mob_number";
    public static final String KEY_PRODUCT_ID = "product_id";
    public static final String KEY_PRODUCT_NAME = "product_name";
    public static final String KEY_PRODUCT_URL = "product_url";
    public static final String KEY_PROMOCODE = "promocode";

    public static final int SCREEN_DENSITY_LOW = 1;
    public static final int SCREEN_DENSITY_MEDIUM = 2;
    public static final int SCREEN_DENSITY_HIGH = 4;
    public static final int SCREEN_DENSITY_EXTRA_HIGH = 8;

    public static final String GRID_LDPI_PARAM = "101x105/";
    public static final String GRID_MDPI_PARAM = "135x141/";
    public static final String GRID_HDPI_PARAM = "202x211/";
    public static final String GRID_XHDPI_PARAM = "270x282/";

    public static final String HEADER_LDPI_PARAM = "160x14/";
    public static final String HEADER_MDPI_PARAM = "320x28/";
    public static final String HEADER_HDPI_PARAM = "480x42/";
    public static final String HEADER_XHDPI_PARAM = "640x56/";

    public static final String CAROUSEL_1_LDPI_PARAM = "210x123/";
    public static final String CAROUSEL_1_MDPI_PARAM = "280x164/";
    public static final String CAROUSEL_1_HDPI_PARAM = "420x246/";
    public static final String CAROUSEL_1_XHDPI_PARAM = "560x328/";

    public static final String CAROUSEL_2_LDPI_PARAM = "107x56/";
    public static final String CAROUSEL_2_MDPI_PARAM = "143x75/";
    public static final String CAROUSEL_2_HDPI_PARAM = "214x112/";
    public static final String CAROUSEL_2_XHDPI_PARAM = "286x150/";

    public static final String PRODUCT_ROW_LDPI_PARAM = "76x76/";
    public static final String PRODUCT_ROW_MDPI_PARAM = "102x102/";
    public static final String PRODUCT_ROW_HDPI_PARAM = "153x153/";
    public static final String PRODUCT_ROW_XHDPI_PARAM = "204x204/";

    public static final String PRODUCT_DETAIL_LDPI_PARAM = "208x187/";
    public static final String PRODUCT_DETAIL_MDPI_PARAM = "278x250/";
    public static final String PRODUCT_DETAIL_HDPI_PARAM = "417x375/";
    public static final String PRODUCT_DETAIL_XHDPI_PARAM = "556x500/";

    public static final String FOOTER_LDPI_PARAM = "137x30/";
    public static final String FOOTER_MDPI_PARAM = "184x40/";
    public static final String FOOTER_HDPI_PARAM = "274x60/";
    public static final String FOOTER_XHDPI_PARAM = "366x81/";

    public static final String MERCHANT_LOGO_LDPI_PARAM = "208x187/";
    public static final String MERCHANT_LOGO_MDPI_PARAM = "278x250/";
    public static final String MERCHANT_LOGO_HDPI_PARAM = "71x23/";
    public static final String MERCHANT_LOGO_XHDPI_PARAM = "53x17/";

    public static final String PAGE_COUNT_PARAM = "page_count";
    public static final String ITEMS_PER_PAGE_PARAM = "items_per_page";
    public static final int ITEMS_PER_PAGE = 30;
    public static final String SKIP_FILTERS = "skipFilters=1";
    public static final String TYPE_1 = "_type=1";

    public static final int IMAGE_TYPE_CAROUSEL_1 = 1;
    public static final int IMAGE_TYPE_CAROUSEL_2 = 2;
    public static final int IMAGE_TYPE_PRODUCT_ROW = 3;
    public static final int IMAGE_TYPE_HEADER = 4;
    public static final int IMAGE_TYPE_GRID = 5;
    public static final int IMAGE_TYPE_PRODUCT_DETAIL = 6;
    public static final int IMAGE_TYPE_FOOTER = 7;
    public static final int IMAGE_TYPE_MERCHANT_LOGO = 8;
    public static final int IMAGE_TYPE_SAVED_CARD = 9;
    public static final int IMAGE_TYPE_HOMEPAGE = 15;

    public static final String SELECT_ROUTE = "Select Route";
    public static final String ENTER_SMART_TAG_NUMBER = "Enter Smart Tag Number";
    public static final String TOLLCARD_RECHAGE = "Tollcard Recharge";

    public static final String PREPAID_MOBILE_NUMBER = "Prepaid Mobile Number";
    public static final String POSTPAID_MOBILE_NUMBER = "Postpaid Mobile Number";
    public static final String MOBILE_RECHARGE = "Mobile Recharge";
    public static final String MOBILE_BILL_PAYMENT = "Mobile Bill Payment";
    public static final String PREPAID_DATACARD_NUMBER = "Prepaid Datacard Number";
    public static final String POSTPAID_DATACARD_NUMBER = "Postpaid Datacard Number";
    public static final String DATACARD_RECHARGE = "Datacard Recharge";
    public static final String DATACARD_BILL_PAYMENT = "Datacard Bill Payment";
    public static final String ENTER_SUNSCRIBER_ID = "Enter Consumer ID";
    public static final String DTH_RECHARGE = "Recharge DTH Subscription";
    public final static String TEST_MERCHANT_ID = "1";
    public final static String IS_CUSTOMER_TEST_MERCHANT_ID = "is_customer_test_merchant_id";

    public final static String BUYER_EMAIL = "abcdxyzudupi@gmail.com"; // xyz3456@gmail.com
    public final static String BUYER_PHONE = "9886849936";
    public final static String BUYER_FIRST_NAME = "Piyush";
    public final static String BUYER_LAST_NAME = "Sharma";
    // public final static String BUYER_OAUTH_ID = "<auth-id>";
    public final static String BUYER_OAUTH_TOKEN = "<auth-token>";
    public final static String USER_EMAIL = "user_email";
    public final static String USER_PHONE = "user_phone";
    public final static String USER_FIRST_NAME = "user_first_name";
    public final static String USER_LAST_NAME = "user_last_name";
    public final static String USER_OAUTH_TOKEN = "user_oauth_token";
    public final static String MERCHANT_ID = "merchant_id";
    public final static String MERCHANT_NAME = "merchant_name";
    public final static String PRODUCT_DESCRIPTION = "product_description";
    public final static String PRODUCT_IMAGE_URL = "product_image_url";
    public final static String PRODUCT_OFFER_PRICE = "product_offer_price";
    public final static String AGENT_NAME = "agent_name";
    public final static String PRODUCT_CONTEXT = "\"PRDCNTXT\"";
    public final static String CLOSE_MESSAGE = "\"CLSCHAT\"";
    public static final String PLUSTXT_APP_ID = "YOUR_APP_ID_HERE";
    // public static String MERCHANT_ID = "t8ouhs";

    public static final String INDICATIVE_PALN_LIST = "indicative_paln_list";
    public static final int SIGN_IN_SIGN_UP_FOR_PROMO_APPLY = 123;
    public static final int SIGN_IN_SIGN_UP = 111;
    public static final int REQUEST_CODE_GRID_PAGE = 222;
    public static final int REQUEST_CODE_REFRESH_PAGE = 223;

    //    public static final int REQUEST_CODE_GRID_PAGE_REFINE = 333;
    public static final String PREFERENCE_SELECTED_SERVER = "selected server";
    public static final String EMAIL_REG_EX =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String NUMBER_REG_EX = "[\\d.]+";
    public static final int PASSWORD_LENGTH = 4;
    public static final String KEY_UNIQUE_ID = "uniqueid";
    public static final String KEY_IMEI = "imei";
    public static final String KEY_SIM_SERIAL = "simSerial";
    public static final int NUMBER_OF_BOX_IN_ROW = 16;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR = 1;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_GRID_PAGE_CAROUSEL_IMAGE = 8;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_GRID_PAGE_CAROUSEL_TEXT = 3;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_GRID_PAGE_TAB_HEADER = 2;
    public static final double NUNBER_OF_BOX_IN_HEIGHT_GRID_ROW = 5.5;
    public static final double NUNBER_OF_BOX_IN_WIDTH_GRID_ROW = 5.5;
    public static final double NUNBER_OF_BOX_IN_HEIGHT_SMALL_CAROUSEL = 4.5;
    public static final double NUNBER_OF_BOX_IN_WIDTH_SMALL_CAROUSEL = 7.5;


    public static final double NUNBER_OF_BOX_IN_HEIGHT_SMALL_CAROUSEL_TAB = 2.7;///3.37;
    public static final double NUNBER_OF_BOX_IN_WIDTH_SMALL_CAROUSEL_TAB = 4.5;//5.62;
    public static final double NUNBER_OF_BOX_IN_HEIGHT_GRID_ROW_TAB = 3.3;
    public static final double NUNBER_OF_BOX_IN_WIDTH_GRID_ROW_TAB = 3.3;

    public static final double NUNBER_OF_BOX_IN_HEIGHT_ROW_PRODUCT_TAB = 4.98;

    public static final double NUNBER_OF_BOX_IN_HEIGHT_MAIN_CAROUSEL = 8.5;
    public static final double NUNBER_OF_BOX_IN_WIDTH_MAIN_CAROUSEL = 16;
    public static final double NUNBER_OF_BOX_IN_HEIGHT_PRODUCT_CAROUSEL = 25;
    public static final int NUNBER_OF_BOX_IN_WIDTH_GRID_ITEM = 7;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_GRID_ITEM = 10;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_GRID_ITEM_IMAGE = 7;
    public static final int NUNBER_OF_BOX_IN_WIDTH_OF_ITEM_PRICE = 2;
    public static final String ORDER_ID = "order_id";
    public static final int REPLACEMENT_REQUEST_CODE = 2;

    public static final double PDP_SIMILAR_PRODUCTS_MUTIPLIER = 8.8;

    public static final String FROM = "From";
    public static final String FROM_PUSH_NOTIFICATION = "push_notification";
    public static final String RECHARGE = "Recharge";
    public static final String GOLD = "Gold";
    public static final String KYC = "kyc";
    public static final String ADD_TO_PAYTM_CASH = "Add_to_paytm_cash";
    public static final String SHOPPING_CART = "Shopping_cart";
    public static final String PAYMENT = "Payment";
    public static final String ORDER_HISTORY = "Order_history";
    public static final String ORDER_SUMMARY = "Order_summary";
    public static final String CONTINGENCY = "Contingency";
    public static final String AUTH = "Auth";
    public static final String CLOSE = "Close";
    public static final String MAINTENANCE = "Maintenance";
    public static final String SOURCE_KEYWORD = "source_keyword";
    public static final String DESTINATION_KEYWORD = "destination_keyword";
    public static final String SOURCE_POSITION = "source_position";
    public static final String DESTINATION_POSITION = "destination_position";
    public static final String DIGITAL_CREDIT_PAYMENT = "digital_credit_payment";

    public static final String FILTER_TYPE_SLIDER = "slider";

    public static final String FILTER_BY_PRICE = "Price";
    public static final String FILTER_BY_BRAND = "Brand";
    public static final String FILTER_BY_COLOR = "Color";
    public static final String FILTER_BY_SIZE = "Size";
    public static final String FILTER_BY_DISCOUNT = "Discount";
    public static final String FILTER_TYPE_RANGE_SLIDER = "range-slider";

    public static final String KEY_PRODUCT_DETAILS = "product_details_bargin";
    public static final String PAYTM_CARE_EMAIL_ID = "care@paytm.com";
    public static final String PAYTM_FEEDBACK_EMAIL_ID = "feedback@paytm.com";

    public static final String FILTER_TYPE_LIST = "list";
    public static final String FILTER_TYPE_CIRCULAR = "circular";
    public static final String FILTER_TYPE_RECTANGULAR = "rectangular";
    public static final String FILTER_TYPE_TOGGLE = "boolean";
    public static final String FILTER_TYPE_LINEAR_REACTANGULAR = "linear-rectangular";
    public static final String FILTER_TYPE_TREE = "tree";

    public static final int NUMBER_OF_BOX_IN_HEIGHT = 56;

    public static final String SESSION_ID = "session_id";
    public static final String ACCEPT = "Accept";
    public static final String ACCEPT_VALUE = "*/*";
    public static final String TOGGLE_ON = "True";
    public static final String TOGGLE_OFF = "False";
    public static final String INDICATIVE_OP = "%OP";
    public static final String INDICATIVE_CIRCLE = "%CIR";
    public static final String INDICATIVE_TYPE = "%TYPE";

    public static final int GRID_UNIT_HALF = 2;

    public static final String CART_ITEM_COUNT = "cart_item_qty";
    public static final String DELIVERY_CHARGE_PINCODE = "pincode";
    public static final String ORDER_TOTAL = "order_total";
    public static final String FIRST_NAME = "first name";
    public static final String LAST_NAME = "last name";

    public static final int NUNBER_OF_BOX_IN_WIDTH_FILTER_SHOW_BUTTON = 13;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_FILTER_SHOW_BUTTON = 2;
    public static final int NUNBER_OF_BOX_IN_PADDING_FILTER_SHOW_BUTTON = 2;
    public static final int NUNBER_OF_BOX_IN_WIDTH_FILTER_CONTAINER = 14;
    public static final double NUNBER_OF_BOX_IN_HEIGHT_FILTERD_ITEM_CONTAINER = 2.5;
    public static final double NUNBER_OF_BOX_IN_HEIGHT_SUGGESTION_CONTAINER = 1.5;
    public static final String TEXT_SEARCH_RESULTS = "Search Results";
    public static final String KEY_WISHLIST_DONTSHOW = "wishlist_dontshow";
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_USER_NAME = "userName";
    public static final String KEY_USER_IMAGE = "userImage";
    public static final String KEY_USER_TYPE_SD_MERCHANT = "userType";
    public static final String KEY_IS_VERIFIED_MOBILE = "isVerifiedMobile";
    public static final String KEY_IS_VERIFIED_EMAIL = "isVerifiedEmail";
    public static final String KEY_NEW_PASSWORD = "newPassword";
    public static final String KEY_CONFIRM_PASSWORD = "confirmPassword";
    public static final String KEY_OLD_PASSWORD = "oldPassword";
    public static final String KEY_ID = "id";
    public static final String KEY_IS_PRIME = "isPrime";
    public static final String KEY_KYC_TYPE = "kycType";
    public static final String KEY_KYC_PRIME_USER_STATUS = "kycPrimeUserStatus";
    public static final String KEY_TITLE = "title";
    public static final String KEY_NAME = "name";
    public static final String KEY_FULLADDRESS = "fulladdress";
    public static final String KEY_CITY = "city";
    public static final String KEY_STATE = "state";
    public static final String KEY_PIN = "pin";
    public static final String KEY_HOUSE_NO = "houseNo";
    public static final String KEY_STREET_NAME = "streetName";
    public static final String KEY_ADDRESS_1 = "address1";
    public static final String KEY_ADDRESS_2 = "address2";
    public static final String KEY_IS_DELETED = "isDeleted";
    public static final String KEY_QTY = "qty";
    public static final String KEY_PRICE = "price";
    public static final String KEY_DETAILS = "details";
    public static final String KEY_MAX_CLAIM_AMOUNT = "max_claim_amount";
    public static final String KEY_DATA = "data";
    public static final String KEY_TEXT2DISPLAY = "text_to_display";
    public static final String KEY_TC = "tc";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String LOGIN_PASSWORD = "loginPassword";
    // public static final String CART_AMOUNT = "cart_amount";
    public static final String ACTION_SETADDRESS = "setaddress";
    public static final String ACTION = "action";
    public static final String ADDRESS = "address";
    public static final String FONT_ROBOTO_REGULAR = "Roboto-Regular.ttf";
    public static final String FONT_ROBOTO_LIGHT = "Roboto-Light.ttf";
    public static final String FONT_ROBOTO_MEDIUM = "Roboto-Medium.ttf";
    public static final String FONT_ROBOTO_BOLD = "Roboto-Bold.ttf";

	/*public static final String FONT_HELVETICA_NEUE_REGULAR = "HelveticaNeue.ttf";
    public static final String FONT_HELVETICA_NEUE_BOLD = "HelveticaNeueBold.ttf";
	public static final String FONT_HELVETICA_NEUE_LIGHT = "HelveticaNeueLight.otf";*/

    public static final String[] MONTHS = new String[]{"", "Jan", "Feb",
            "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",
            "Dec"};
    public static final String TAB_PRICE = "price";
    public static final String TAB_NEW = "new";
    public static final String TAB_POPULAR = "popular";
    public static final String TAB_RELEVANCE = "relevance";

    public static final String READ = "read";
    public static final String WRITE = "write";
    public static final Object FILTER_TYPE_RANGE_SEPARATOR = "-";
    public static final String DEVICE_ANDROID = "?device=android";
    public static final String KEY_DOB = "user_dob";
    public static final String KEY_GENDER = "user_gender";
    public static final String SD_MERCHANT = "SD_MERCHANT";
    public static final String NO_MERCHANT = "NO_MERCHANT";

    public static final String KEY_FIRST_NAME = "first name";
    public static final String KEY_UNREAD_MESSAGE_COUNT = "unread message count";
    public static final String KEY_LAST_NAME = "last name";
    public static final int SORT_ASCENDING = 0;
    public static final int SORT_DESCENDING = 1;

    public static final String EXTRA_INTENT_TO_UPDATE_ADDRESS = "edit address";
    public static final String EXTRA_TYPE = "type";
    public static final String EXTRA_NAME = "name";
    public static final String EXTRA_INTENT_ADDRESS_TO_UPDATE = "address to update";


    public static final String INTENT_PRODUCT_DETAIL = "product_detail";
    public static final String INTENT_COUPON_ID = "coupon_id";
    public static final String INTENT_FOR_SINGLE_ITEM = "cart_item_url";
    public static final String INTENT_CART = "recharge cart";
    public static final String INTENT_GRID = "grid";
    public static final String RUPEES = "Rs";
    public static final String TAB_HEADING_PERSONAL_DETAILS = "Personal Details";
    public static final String TAB_HEADING_SHIPPING_ADDR = "Delivery Address";
    public static final String TAB_HEADING_SAVED_CARDS = "Saved Cards";
    public static final String KEY_CLIENT = "client";
    public static final String KEY_CLIENT_VALUE = "androidapp";

    public static final String URL = "url";
    public static final String URL_TYPE = "url_type";
    public static final String EXTRAS = "extras";

    public static final String PUSH_PRODUCT_ID = "product_id";
    public static final String PUSH_QUANTITY = "quantity";
    public static final String PUSH_PROMOCODE = "promo_code";
    public static final String PUSH_AMOUNT = "amount";
    public static final String PUSH_SHOW_POPUP = "showpopup";
    public static final String PUSH_TITLE = "title";
    public static final String PUSH_ALERT_MESSAGE_BODY = "alert_message";
    public static final String PUSH_UTM_SOURCE = "utm_source";
    public static final String PUSH_FEATURE_TYPE = "featuretype";
    public static final String PUSH_WALLET_CODE = "wallet_code";
    public static final String PUSH_RECIPIENT = "recipient";
    public static final String PUSH_COMMENT = "comment";
    public static final String UTM_MEDIUM = "utm_medium";
    public static final String UTM_TERM = "utm_term";
    public static final String UTM_CONTENT = "utm_content";
    public static final String UTM_CAMPAIGN = "utm_campaign";
    public static final String DEEP_LINKING = "deep_linking";
    public static final String PUSH_CHECK_IN_DATE = "check_in_date";
    public static final String PUSH_CHECK_OUT_DATE = "check_out_date";
    public static final String PUSH_CITY = "city";
    public static final String PUSH_CITY_VALUE = "city_value";
    public static final String PUSH_ROOMS_DETAILS = "rooms_details";
    public static final String PUSH_FILTER_DETAILS = "filter_params";
    public static final String PUSH_QUERY_STRING = "query_string";
    public static final String PUSH_SMOOTH_PAY = "showpopup";
    public static final String PUSH_HOTEL_NAME = "hotel_name";
    public static final String PUSH_HOTEL_ID = "hotel_id";
    public static final String PUSH_HOTEL_EXTRAS = "hotel_extras";
    public static final String PUSH_HOTEL_FINAL_PRICE_WITH_TAX = "hotel_final_price_with_tax";
    public static final String PUSH_RICH_PAGE_TYPE = "rich_page_type";
    public static final String PUSH_RICH_PAGE_ID = "rich_page_id";
    public static final String PUSH_RICH_PAGE_ACTION = "rich_page_action";
    public static final String PUSH_RICH_PAGE_TITLE = "rich_page_title";
    public static final String PUSH_RICH_PAGE_MESSAGE_CENTER = "message_center";
    public static final String PUSH_RICH_PAGE_LANDING_PAGE = "landing_page";
    public static final String PUSH_RECHARGE_BOTTOM_TAB = "bottom_tab";
    public static final String PUSH_RECHARGE_PREPAID_ON = "prepaid_on";
    public static final String PUSH_RECHARGE_FF = "ff";


    public static final String Title = "title";
    public static final String HEADER_VERIFICATION_NEEDED = "verification_detail_required";
    public static final String EXTRA_INTENT_USER_INFO = "user_info";

    public static final String LEFT_CONVERSTATION_MESSAGE = "You left the conversation";
    public static final String AGENT_LEFT_CONVERSTATION_MESSAGE = "Agent left the conversation";
    public static final String EXTRA_INTENT_IS_FROM_CATALOG = "launched_from_catalog";
    public static final String FILE_CATALOG_SLECTED_STATE = "catalog_state";
    public static final String EXTRA_INTENT_SIGN_IN_WITH_STEP_2 = "sign_in_sign_up_with_step_2";
    public static final String EXTRA_INTENT_SIGN_IN_WITH_STEP_2_TXN_FLOW = "sign_in_txn_flow";
    public static final String EXPRESS_CART_CHECKOUT_SUFFIX = "/checkout";
    public static final String EXPRESS_CART_VERIFY_SUFFIX = "/verify";
    public static final String SEARCH_URL_SUFFIX = "userQuery=";
    public static final String SEARCH_URL_KEYWORD_SUFFIX = "from=";
    public static final String SEARCH_URL_KEYWORD_CAT_TREE = "&cat_tree=1";
    public static final String SEARCH_URL_KEYWORD_CURATED = "&curated=1";
    public static final String SEARCH_URL_KEYWORD_POPULAR_SEARCH = "popularsearch";
    public static final String SEARCH_URL_KEYWORD_AUTO_SUGGEST = "autosuggest";
    public static final String SEARCH_URL_KEYWORD_ORGANIC = "organic";
    public static final String SEARCH_URL_KEYWORD_DID_YOU_MEAN = "didyoumean ";

    public static final int MESSAGE_LOAD_COUNT = 10;

    public static final String TITLE_EXPRESS_CART = "Complete Your Order";
    public static final String TITLE_COUPONS = "Care for a few Deals?";

    public static final String MOBILE_TYPE = "Mobile";
    public static final String DATA_CARD_TYPE = "Data Container";
    public static final String DTH_TYPE = "DTH";
    public static final String TOLL_CARD_TYPE = "Toll Container";
    public static final String LANDLINE_TYPE = "Landline";
    public static final String GAS_CONNECTION_TYPE = "GAS";
    public static final String ELECTRICITY_CONNECTION_TYPE = "Electricity";

    public static final String RECHARGE_TITLE = "Recharges";
    public static final String BILL_PAYMENTS_TITLE = "Bill Payments";

    // has offers
    public static final String HAS_OFFERS_ADVERTISER_ID = "11538";
    public static final String HAS_OFFERS_CONVERSION_KEY = "15da425ac691008d57d4758729816c88";
    public static final boolean HAS_OFFERS_ENABLED = false;

   /* // APSALAR
    public static final String APSALAR_APP_KEY = "paytm";
    public static final String APSALAR_APP_SECRET = "PSCSFpsi";*/
    //public static final boolean APSLAR_ENABLED = true;
    //public static final int CURRENT_APSALAR_LIST_VERSION = 2;


    //countly
    //public static final String COUNTLY_APP_KEY = "a071b8ce993d19d21e4a92a535fd7a9dae6b40f7";
    public static final String COUNTLY_APP_KEY = "4fdeeda571347270295c39a65f28143bb1d6d9e1";
    //new key

    public static final boolean COUNTLY_ENABLED = true;


    //Crittercism
    public static final String CRITTERCISM_APP_KEY = "52ea173ea7928a5b85000004";


    // Events used in both apsalar as well as Has offers
    public static final String EVENT_CLICK_DEEP_LINKING = "EventClickDeepLinking";
    public static final String EVENT_CLICK_ADDTOCART = "EventClickAddToCart";
    public static final String EVENT_CLICK_PRODUCT_CATEGORY_LEFT_MENU =
            "EventClickProductCategoryLeftMenu";
    public static final String EVENT_CLICK_SUB_PRODUCT_CATEGORY_LEFT_MENU =
            "EventClickSubProductCategoryLeftMenu";
    public static final String EVENT_CLICK_HOMEPAGE_PRODUCT_IMAGE =
            "EventClickHomepageProductImage";
    public static final String EVENT_CLICK_HOMEPAGE_PRODUCT_NAME = "EventClickHomepageProductName";
    public static final String EVENT_CLICK_CATEGORYPAGE_PRODUCT_IMAGE =
            "EventClickCategoryPageProductImage";
    public static final String EVENT_CLICK_CATEGORYPAGE_PRODUCT_NAME =
            "EventClickCategoryPageProductName";
    public static final String EVENT_CLICK_MOBILE_RECHARGE_PREPAID =
            "EventClickMobileRechargePrepaid";
    public static final String EVENT_MOBILE_RECHARGE_BILLPAYMENT =
            "EventClickMobileRechargeBillPayment";
    public static final String EVENT_CLICK_MOBILE_RECHARGE_DTH = "EventClickMobileRechargeDTH";
    public static final String EVENT_CLICK_MOBILE_RECHARGE_TOLLCARD =
            "EventClickMobileRechargeTollCard";
    public static final String EVENT_CLICK_MOBILE_RECHARGE_DATACARD_RECHARGE =
            "EventClickMobileRechargeDataCardRecharge";
    public static final String EVENT_CLICK_CATEGORY_PAGE_PRICE = "EventClickCategoryPagePrice";
    public static final String EVENT_CLICK_CATEGORY_PAGE_PRICE_POPULAR =
            "EventClickCategoryPagePricePopular";
    public static final String EVENT_CLICK_CATEGORY_PAGE_PRICE_POPULAR_REFINE =
            "EventClickCategoryPagePricePopularRefine";
    public static final String EVENT_CLICK_CATEGORY_PAGE_POPULAR = "EventClickCategoryPagePopular";
    public static final String EVENT_CLICK_CATEGORY_PAGE_POPULAR_REFINE =
            "EventClickCategoryPagePopularRefine";
    public static final String EVENT_CLICK_CATEGORY_PAGE_REFINE = "EventClickCategoryPageRefine";
    public static final String EVENT_CLICK_CATEGORY_PAGE_PRICE_REFINE =
            "EventClickCategoryPagePriceRefine";
    public static final String EVENT_CLICK_PRODUCT_SEARCH_ICON = "EventClickProductSearchIcon";
    public static final String EVENT_CLICK_SHOWCASE_HOME_PRODUCT_IMAGE =
            "EventClickShowcaseHomeProductImage";
    public static final String EVENT_CLICK_VIEW_YOUR_ORDERS = "EventClickViewYourOrders";
    public static final String EVENT_CLICK_SETTINGS = "EventClickSettings";
    public static final String EVENT_CLICK_HELP = "EventClickHelp";
    public static final String EVENT_CLICK_SIGN_OUT = "EventClickSignOut";
    public static final String EVENT_CLICK_CHECK_OUT = "EventClickCheckout";
    public static final String EVENT_CLICK_EDIT_CART_PRODUCT_REMOVE =
            "EventClickEditCartProductRemove";
    public static final String EVENT_CLICK_EDIT_CART_PRODUCT_QUANTITY_UPDATE =
            "EventClickEditCartProductQuantityUpdate";
    public static final String EVENT_CLICK_CART_PROMOCODE_APPLY = "EventClickCartPromocodeApply";
    public static final String EVENT_CLICK_EDIT_CART_ROMOCODE_APPLY =
            "EventClickEditCartPromocodeApply";
    public static final String EVENT_CLICK_EDIT_CART_ROMOCODE_APPLY_CANCEL_ORDER =
            "EventClickEditCartPromocodeApplyCancelOrder";
    public static final String EVENT_CLICK_CART_SELECT_SHIPPING = "EventClickCartSelectShipping";
    public static final String EVENT_CLICK_CART_SELECT_DELIVERY_ADDRESS =
            "EventClickCartSelectDeliveryAddress";
    public static final String EVENT_CLICK_BUTTON_SHIPPING_PAGE = "EventClickButtonShippingPage";
    public static final String EVENT_CLICK_SHOW_PWD_LOGIN_PAGE = "EventClickShowPWDLoginPage";
    public static final String EVENT_CLICK_FORGOT_PWD_LOGIN_PAGE = "EventClickForgotPWDLoginPage";
    public static final String EVENT_CLICK_CLICK_SIGNING_LOGIN_PAGE = "EventClickSigninLoginPage";
    public static final String EVENT_CLICK_ACCOUNT_LOGIN_PAGE = "EventClickAccountLoginPage";
    public static final String EVENT_CLICK_SIGN_UP = "EventClickSignup";
    public static final String EVENT_CLICK_PAYMENT_OPTION = "EventClickPaymentOption";
    public static final String EVENT_CLICK_DISPLAY_THANKYOU_PAGE = "EventDisplayThankYouPage";
    public static final String EVENT_CLICK_BARGIN_BUTTON = "EventClickBargainButton";
    public static final String EVENT_CLICK_HOME_PAGE_PRODUCT_FEATURED =
            "EventClickHomePageProductFeatured";
    public static final String EVENT_CLICK_HOME_PAGE_PRODUCT_COLLECTIONS =
            "EventClickHomePageProductCollections";
    public static final String EVENT_CLICK_HOME_PAGE_PRODUCT_TRENDING =
            "EventClickHomePageProductTrending";
    public static final String EVENT_CLICK_HOME_PAGE_PRODUCT_BEST_SELLERS =
            "EventClickHomePageProductBestSellers";
    public static final String EVENT_CLICK_SEE_ALL_UNDER_ALL_SECTIONS =
            "EventClickAllSectionSeeAll";
    public static final String EVENT_CLICK_HOME_PAGE_PRODUCT_BEST_BARGAINS =
            "EventClickHomePageProductBestBargains";
    public static final String EVENT_NETWORK_ERROR = "URLConnectionError";

    public static final String EVENT_BROWSE_PLAN_SELECTED = "BrowsePlanSelected";
    public static final String EVENT_FAST_FORWARD = "FastForward";
    public static final String EVENT_MARKET_PLACE_CONTINUE_SHOPPING = "MarketPlaceContinueShopping";
    public static final String EVENT_BROWSE_PLAN_TAB = "BrowsePlanTab";
    public static final String EVENT_YOUR_ORDER_REPEAT_RETRY_CLICK = "YourOrdersRepeatRetryClick";
    public static final String EVENT_PREPAID_POST_PAID_TOGGLE = "PrepaidPostPaidToggle";
    public static final String EVENT_TOPUP_SPL_RECHARGE_TOGGLE = "TopUpSpecialRechargeToggle";
    public static final String EVENT_CONTACT_ICON_CLICK = "ContactsIconClick";
    public static final String EVENT_CONATCT_SELECTED = "ContactSelected";
    public static final String EVENT_QUEING_ERROR = "QueingError";
    public static final String EVENT_SOFT_BLOCK = "SoftBlock";
    public static final String EVENT_PAGENAVIGATION = "PageNavigation";
    public static final String EVENT_GAS_OPERATOR = "GasOperator";
    public static final String EVENT_LANDLINE_OPERATOR = "LandlineOperator";
    public static final String EVENT_ELECTRICITY_OPERATOR = "ElectricityOperator";
    public static final String EVENT_OPERATOR = "Operator";

    public static final String SECTION_FEATURED = "FEATURED";
    public static final String SECTION_COLLECTIONS = "Collection";
    public static final String SECTION_TRENDING = "Trending";
    public static final String SECTION_BEST_SELLERS = "Best Sellers";
    public static final String SECTION_BEST_BARGAIN = "Best Bargains";

    //Parameters used in both apsalar and has offers
    public static final String PARAM_CATEGORY_NAME = "CategoryName";
    public static final String PARAM_SUB_CATEGORY_NAME = "SubCategoryName";
    public static final String PARAM_PRODUCT_ID = "ProductId";
    public static final String PARAM_BANNER_ID = "BannerId";
    public static final String PARAM_PRODUCT_NAME = "ProductName";
    public static final String PARAM_PROMO_CODE = "PromoCode";
    public static final String PARAM_PAYMENT_OPTION = "PaymentOption";
    public static final String PARAM_CUSTOMER_ID = "CustomerId";
    public static final String PARAM_ORDER_ID = "OrderId";
    public static final String PARAM_MESSAGE = "CategoryName";
    public static final String PARAM_URL = "CategoryName";
    public static final String PARAM_AMOUNT = "Amount";
    public static final String PARAM_BUCKET_NUMBER = "bucket-number";
    public static final String PARAM_BUTTON_TITLE = "ButtonTitle";
    public static final String PARAM_OFFER_PRICE = "offer_price";
    public static final String PARAM_TITLE = "title";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_NAVIGATION = "Navigation";

    public static final int MESSAGE_STATUS_UNREAD = 0;
    public static final String PREFERENCE_IS_FIRST_LAUNCH = "isFirstLaunch";
    public static final String IS_HOME_REFRESHED = "isHomeDataRefreshed";
    public static final String PREF_KEY_SEARCH_USER_ID = "search_user_id";
    public static final String PREFERENCE_IS_FIRST_PRODUCT_DETAIL_LAUNCH =
            "isFirstProductDetailLaunch";

    public static final String EXTRA_INTENT_MOBILE_LAYOUT_IN_HOME_SCREEN =
            "mobile_layout_in_home_screen";

    public static final String KEY_OTP = "sendotp";
    public static final String CHANGE_PASSWORD_CODE = "change_password_code";
    public static final String CHANGE_PASSWORD_NEW_API = "change_password_new_api";

    public static final String LAST_BARGAINED_PRODUCT_DETAILS_MESSAGE =
            "last_bargained_product_details_message";
    public static final String IS_FROM_SLIDE_MENU = "is_from_slide_menu";
    public static final String EXTRA_INTENT_RECHARGE_ITEM = "recharge_item";
    public static final String IS_FROM_HOME = "is_from_home";
    public static final String MERCHANT_ID_FOR_PRODUCT_DETAILS_MESSAGE =
            "merchant_id_for_product_details_message";
    public static final int NUNBER_OF_BOX_IN_HEIGHT_IN_SEAL = 3;
    public static final String EXTRA_INTENT_SERVER_AUTH_ERROR = "authError";

    public static final String KEY_NEW_UPDATE = "new_update";
    public static final String KEY_UPDATE_REMIND_DATE = "remind_date";
    public static final String OUT_OF_STOCK = "Out of stock";
    public static final int INITIAL_PAGE_COUNT = 1;
    public static final String[] STATE_LIST = {"Andaman & Nicobar Islands", "Andhra Pradesh",
            "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh",
            "Chhattisgarh", "Dadra & Nagar Haveli", "Daman & Diu", "Delhi",
            "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu & Kashmir",
            "Jharkhand", "Karnataka", "Kerala", "Lakshadweep",
            "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram",
            "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan",
            "Sikkim", "Tamil Nadu", "Tripura", "Uttarakhand", "Uttar Pradesh",
            "West Bengal"};
    public static final String EXTRA_FEATURED_CATEGORIES = "extra_featured_category";
    public static final String EXTRA_IS_DEEP_LINK_DATA = "is_deep_linking_data";
    public static final String EXTRA_IS_DEFERRED_DEEP_LINK_DATA = "is_deferred_deep_linking_data";
    public static final String EXTRA_DEEP_LINK_DATA = "deep_linking_data";

    public static final String EXTRA_IS_REF_DATA = "is_referral_data";
    public static final String EXTRA_REF_DATA = "referral_data";

    public static final String LAYOUT_HOME_PAGE = "home_page_layout";
    public static final String GENERICMESSAGEHTML = "<html><body>Please wait...</body></html>";
    //	public static final String ELECTRICITY_URL = "https://catalogapidev.paytm
    // .com/v1/mobile/electricity/24";
    //Apsalar Event List 2

	/*public static final String EVENT_APP_OPEN = "AppOpen";
    public static final String EVENT_HOME_MOBILE_PREPAID = "HomeMobilePrepaid";
	public static final String EVENT_HOME_MOBILE_POSTPAID = "HomeMobilePostpaid";
	public static final String EVENT_HOME_DTH = "HomeDTH";
	public static final String EVENT_HOME_DATACARD_PREPAID = "HomeDatacardPrepaid";
	public static final String EVENT_HOME_DATACARD_POSTPAID = "HomeDatacardPostpaid";
	public static final String EVENT_LEFT_MOBILE_PREPAID = "LeftMobilePrepaid";
	public static final String EVENT_LEFT_MOBILE_POSTPAID = "LeftMobilePostpaid";
	public static final String EVENT_LEFT_CATEGORY_NAME = "LeftCategoryName";
	public static final String EVENT_HOME_PREPAID_PLANS = "HomePrepaidPlans";
	public static final String EVENT_HOME_DTH_PLANS = "HomeDthPlans";
	public static final String EVENT_LEFT_PREPAID_PLANS = "LeftPrepaidPlans";
	public static final String EVENT_LEFT_DTH_PLANS = "LeftDthPlans";
	public static final String EVENT_SEARCH = "Search";
	public static final String EVENT_HOME_SHOWCASE = "HomeShowcase";
	public static final String EVENT_YOUR_ORDERS = "YourOrders";
	public static final String EVENT_SETTINGS = "Settings";
	public static final String EVENT_HELP = "Help";
	public static final String EVENT_SIGN_OUT = "SignOut";
	public static final String EVENT_CHECKOUT = "Checkout";
	public static final String EVENT_EDIT_CART_DELETE = "EditCartDelete";
	public static final String EVENT_EDIT_CART_UPDATE = "EditCartUpdate";
	public static final String EVENT_APPLY_PROMO_CART = "ApplyPromoCart";
	public static final String EVENT_APPLY_PROMO_CART_EDIT = "ApplyPromoCartEdit";
	public static final String EVENT_APPLY_PROMO_CART_CANCEL = "ApplyPromoCartCancel";
	public static final String EVENT_SELECT_SHIPPING = "SelectShipping";
	public static final String EVENT_SELECT_ADDRESS = "SelectAddress";
	public static final String EVENT_SHIPPING_PROCEED = "ShippingProceed";
	public static final String EVENT_SHOW_PASSWORD = "ShowPassword";
	public static final String EVENT_FORGOT_PASSWORD = "ForgotPassword";
	public static final String EVENT_SIGN_IN = "SignIn";
	public static final String EVENT_CREATE_ACCOUNT_LOGIN = "CreateAccountLogin";
	public static final String EVENT_SIGN_UP = "SignUp";
	public static final String EVENT_SELECT_PAYMENT = "SelectPayment";
	public static final String EVENT_THANK_YOU_SHOPPING = "ThankYouShopping";
//	public static final String EVENT_THANK_YOU_RECHARGE = "ThankYouRecharge";
	public static final String EVENT_BARGAIN = "Bargain";
	public static final String EVENT_HOME_FEATURED = "HomeFeatured";
	public static final String EVENT_HOME_COLLECTIONS = "HomeCollections";
	public static final String EVENT_HOME_TRENDING = "HomeTrending";
	public static final String EVENT_HOME_BEST_SELLERS = "HomeBestSellers";
	public static final String EVENT_HOME_SEE_ALL_FEATURED = "HomeSeeAllFeatured";
	public static final String EVENT_HOME_SEE_ALL_COLLECTIONS = "HomeSeeAllCollections";
	public static final String EVENT_HOME_SEE_ALL_BEST_SELLERS = "HomeSeeAllBestSellers";
	public static final String EVENT_HOME_SEE_ALL_TRENDING = "HomeSeeAllTrending";
	public static final String EVENT_SELECT_FREE_COUPON = "SelectFreeCoupon";
	public static final String EVENT_SELECT_PAID_COUPON = "SelectPaidCoupon";
	public static final String EVENT_RECHARGE_APPLY_PROMO = "RechargeApplyPromo";
	public static final String EVENT_RECHARGE_SKIP_COUPON = "RechargeSkipCoupon";
	public static final String EVENT_RECHARGE_USE_WALLET = "RechargeUseWallet";
	public static final String EVENT_RECHARGE_UNSELECT_WALLET = "RechargeUnselectWallet";
	public static final String EVENT_RECHARGE_SELECT_PAYMENT = "RechargeSelectPayment";
	public static final String EVENT_RECHARGE_PROCEED_TO_PAY = "RechargeProceedToPay";
	public static final String EVENT_THANK_YOU_RECHARGE = "ThankYouRecharge";
	public static final String EVENT_RECHARGE_CONTINUE_SHOP = "RechargeContinueShop";
	public static final String EVENT_ADDRESS_BOOK = "AddressBook";
	public static final String EVENT_CHANGE_SIZE = "ChangeSize";
	public static final String EVENT_DESCRIPTION = "Description";
	public static final String EVENT_REFINE = "Refine";
	public static final String EVENT_SORT_NEW = "SortNew";
	public static final String EVENT_SORT_POPULAR = "SortPopular";
	public static final String EVENT_SORT_PRICE = "SortPrice";*/

    //	Apsalar Event List 2
    public static final String EVENT_APP_OPEN = "AppOpen";
    public static final String EVENT_HOME_MOBILE_PREPAID = "HomeMobilePrepaid";
    public static final String EVENT_HOME_MOBILE_POSTPAID = "HomeMobilePostpaid";
    public static final String EVENT_HOME_DTH = "HomeDTH";
    public static final String EVENT_HOME_DATACARD_PREPAID = "HomeDatacardPrepaid";
    public static final String EVENT_HOME_DATACARD_POSTPAID = "HomeDatacardPostpaid";
    public static final String EVENT_LEFT_MOBILE_PREPAID = "LeftMobilePrepaid";
    public static final String EVENT_LEFT_MOBILE_POSTPAID = "LeftMobilePostpaid";

    public static final String EVENT_LEFT_DATACARD_PREPAID = "LeftDatacardPrepaid";
    public static final String EVENT_LEFT_DATACARD_POSTPAID = "LeftDatacardPostpaid";
    public static final String EVENT_LEFT_DTH = "LeftDTH";

    public static final String EVENT_LEFT_CATEGORY_NAME = "LeftCategoryName";
    public static final String EVENT_LEFT_CATALOG_MENU = "LeftCatalogMenu";

    public static final String EVENT_HOME_MOBILE_PLANS = "HomeMobilePlans";
    public static final String EVENT_HOME_DTH_PLANS = "HomeDthPlans";
    public static final String EVENT_LEFT_MOBILE_PLANS = "LeftMobilePlans";


    public static final String EVENT_HOME_DATACARD_PLANS = "HomeDatacardPlans";
    public static final String EVENT_LEFT_DATACARD_PLANS = "LeftDatacardPlans";

    public static final String EVENT_LEFT_DTH_PLANS = "LeftDthPlans";
    public static final String EVENT_SEARCH = "Search";
    public static final String EVENT_HOME_SHOWCASE = "HomeShowcase";
    public static final String EVENT_YOUR_ORDERS = "YourOrders";
    public static final String EVENT_SETTINGS = "Settings";
    public static final String EVENT_HELP = "Help";
    public static final String EVENT_SIGN_OUT = "SignOut";
    public static final String EVENT_CHECKOUT = "Checkout";
    public static final String EVENT_PUSH_NOTIFICATION_BUCKETS = "PushNotificationBuckets";
    public static final String EVENT_EDIT_CART_DELETE = "EditCartDelete";
    public static final String EVENT_EDIT_CART_UPDATE = "EditCartUpdate";
    public static final String EVENT_APPLY_PROMO_CART = "ApplyPromoCart";
    public static final String EVENT_APPLY_PROMO_CART_EDIT = "ApplyPromoCartEdit";
    public static final String EVENT_APPLY_PROMO_CART_CANCEL = "ApplyPromoCartCancel";
    public static final String EVENT_SELECT_SHIPPING = "SelectShipping";
    public static final String EVENT_SELECT_ADDRESS = "SelectAddress";
    public static final String EVENT_SHIPPING_PROCEED = "ShippingProceed";
    public static final String EVENT_SHOW_PASSWORD = "ShowPassword";
    public static final String EVENT_FORGOT_PASSWORD = "ForgotPassword";
    public static final String EVENT_SIGN_IN = "SignIn";
    public static final String EVENT_CREATE_ACCOUNT_LOGIN = "CreateAccountLogin";
    public static final String EVENT_SIGN_UP = "SignUp";
    public static final String EVENT_SELECT_PAYMENT = "SelectPayment";
    public static final String EVENT_THANK_YOU_SHOPPING = "ThankYouShopping";
    public static final String EVENT_THANK_YOU_RECHARGE = "ThankYouRecharge";
    public static final String EVENT_THANK_YOU_WALLET = "ThankYouWallet";
    public static final String EVENT_BARGAIN = "Bargain";
    public static final String EVENT_HOME_FEATURED = "HomeFeatured";
    public static final String EVENT_HOME_COLLECTIONS = "HomeCollections";
    public static final String EVENT_HOME_TRENDING = "HomeTrending";
    public static final String EVENT_HOME_BEST_SELLERS = "HomeBestSellers";
    public static final String EVENT_HOME_BEST_BARGAIN = "HomeBestBargain";
    public static final String EVENT_HOME_SEE_ALL_FEATURED = "HomeSeeAllFeatured";
    public static final String EVENT_HOME_SEE_ALL_COLLECTIONS = "HomeSeeAllCollections";
    public static final String EVENT_HOME_SEE_ALL_BEST_SELLERS = "HomeSeeAllBestSellers";
    public static final String EVENT_HOME_SEE_ALL_TRENDING = "HomeSeeAllTrending";
    public static final String EVENT_SELECT_FREE_COUPON = "SelectFreeCoupon";
    public static final String EVENT_SELECT_PAID_COUPON = "SelectPaidCoupon";
    public static final String EVENT_RECHARGE_APPLY_PROMO = "RechargeApplyPromo";
    public static final String EVENT_RECHARGE_SKIP_COUPON = "RechargeSkipCoupon";
    public static final String EVENT_RECHARGE_USE_WALLET = "RechargeUseWallet";
    public static final String EVENT_RECHARGE_UNSELECT_WALLET = "RechargeUnselectWallet";
    public static final String EVENT_RECHARGE_SELECT_PAYMENT = "RechargeSelectPayment";
    public static final String EVENT_RECHARGE_PROCEED_TO_PAY = "RechargeProceedToPay";
    //	public static final String EVENT_THANK_YOU_RECHARGE = "ThankYouRecharge";
    public static final String EVENT_RECHARGE_CONTINUE_SHOP = "RechargeContinueShop";
    public static final String EVENT_ADDRESS_BOOK = "AddressBook";
    public static final String EVENT_CHANGE_SIZE = "ChangeSize";
    public static final String EVENT_DESCRIPTION = "Description";
    public static final String EVENT_REFINE = "Refine";
    public static final String EVENT_SORT_NEW = "SortNew";
    public static final String EVENT_SORT_POPULAR = "SortPopular";
    public static final String EVENT_SORT_PRICE = "SortPrice";
    public static final String EVENT_GAS_MAHANAGAR = "GasMahanagar";
    public static final String EVENT_RAJDHANI_DELHI = "RajdhaniDelhi";
    public static final String EVENT_YAMUNA_DELHI = "YamunaDelhi";
    public static final String EVENT_MSEB = "MSEB";
    public static final String EVENT_RELIANCE_ENERGY = "RelianceEnergy";
    public static final String EVENT_TATA_DELHI = "TataDelhi";
    public static final String EVENT_LAND_AIRTEL = "LandAirtel";
    public static final String EVENT_LAND_MTNL = "LandMTNL";
    public static final String EVENT_LAND_TATA = "LandTata";
    public static final String EVENT_HOME_PRODUCT_IMAGE = "HomeProductImage";
    public static final String EVENT_HOME_PRODUCT_NAME = "HomeProductName";
    public static final String EVENT_CATEGORY_PRODUCT_NAME = "CategoryProductName";
    public static final String EVENT_CATEGORY_PRODUCT_IMAGE = "CategoryProductImage";
    public static final String EVENT_ADD_TO_CART = "AddToCart";
    public static final String EVENT_SORT_RELEVANCE = "SortRelevance";
    public static final String EVENT_CATEGORY_PRODUCT = "CategoryProduct";
    public static final String EVENT_HOME_PREFIX = "Home";
    public static final String EVENT_CLICK_HOME_SEE_ALL = "HomeSeeAll";
    public static final String EVENT_RECHARGE_REVENUE = "RechargeRevenue";
    public static final String EVENT_MARKET_PLACE_REVENUE = "MarketPlaceRevenue";

    public static final String EVENT_ORDER_SUMMARY_SHOWCASE = "OrderSummaryShowcase";
    public static final String EVENT_ORDER_SUMMARY_PREFIX = "OrderSummary";
    public static final String EVENT_CLICK_ORDER_SUMMARY_SEE_ALL = "OrderSummarySeeAll";


    //New Events

    public static final String EVENT_APP_OPEN_UNIQUE = "AppOpenUnique";
    public static final String EVENT_APP_OPEN_SIGNED_IN = "AppOpenSignIn";
    public static final String EVENT_APP_OPEN_SIGNED_OUT = "AppOpenSignOut";
    public static final String EVENT_END_BARGAIN_MID_WAY = "EndBargainMidWay";
    public static final String EVENT_BARGAIN_CHECKOUT = "BargainCheckout";
    public static final String EVENT_BARGAIN_APPLY_PROMOCODE = "BargainApplyPromocode";
    public static final String EVENT_COUPONS_TAB_CHANGE = "CouponsTabChange";
    public static final String PARAM_TAB_NAME = "Tab";
    public static final String EVENT_PAYTM_CASH_PAGE = "PaytmCashPage";
    public static final String EVENT_PAYTM_CASH_LEDGER_PAGE = "PaytmCashLedgerPage";
    public static final String EVENT_ADD_PAYTM_CASH = "AddPaytmCash";
    public static final String EVENT_PROFILE_TAB_INFO = "ProfileTabInfo";
    public static final String EVENT_PROFILE_TAB_SAVED_CARDS = "ProfileTabSavedCards";
    public static final String EVENT_PROFILE_TAB_ADDRESS = "ProfileTabAddress";
    public static final String EVENT_SEARCH_TEXT = "SearchText";
    public static final String PARAM_SEARCH_KEY = "SearchText";
    public static final String EVENT_LEDGER_BUTTON_CLICK = "LedgerButtonClick";
    public static final String PARAM_BUTTON_LABEL = "ButtonLabel";
    public static final String EVENT_FREQUENT_ORDER_SELECTED = "FrequentOrderSelected";
    public static final String EVENT_FREQUENT_ORDER_AMOUNT_CHANGED = "FrequentOrderAmountChanged";

    public static final boolean ENABLE_FLOATING_HINT = false;
    public static final String INTENT_EXTRA_FROM_CATALOG_HOME = "launch_home_from_category_menu";
    public static final String INTENT_EXTRA_RESET_FF = "Reset_fast_farward";
    public static final String EXTRA_IS_RECHARGE_ON_TOP = "home_screen_config";

    public static final String ORIGIN = "origin";
    public static final String QRCODE_ID = "qrcode_id";
    public static final String TIME_STAMP = "timestamp";
    public static final String DEEPLINK_URL = "deeplink";
    public static final String AFFILIATE_ID = "affiliateID";

    public static final String SUCCESS_MOBILE_PREPAID_NO = "SuccessMobilePrepaidNo";
    public static final String SUCCESS_MOBILE_PREPAID_OPERATOR = "SuccessMobilePrepaidOperator";
    public static final String SUCCESS_MOBILE_PREPAID_CIRCLE = "SuccessMobilePrepaidCircle";
    public static final String SUCCESS_MOBILE_POSTPAID_NO = "SuccessMobilePostpaidNo";
    public static final String SUCCESS_MOBILE_POSTPAID_OPERATOR = "SuccessMobilePostpaidOperator";
    public static final String SUCCESS_MOBILE_POSTPAID_CIRCLE = "SuccessMobilePostpaidCircle";

    public static final String SUCCESS_DATACARD_PREPAID_NO = "SuccessDatacardPrepaidNo";
    public static final String SUCCESS_DATACARD_PREPAID_OPERATOR = "SuccessDatacardPrepaidOperator";
    public static final String SUCCESS_DATACARD_PREPAID_CIRCLE = "SuccessDatacardPrepaidCircle";
    public static final String SUCCESS_DATACARD_POSTPAID_NO = "SuccessDatacardPostpaidNo";
    public static final String SUCCESS_DATACARD_POSTPAID_OPERATOR =
            "SuccessDatacardPostpaidOperator";
    public static final String SUCCESS_DATACARD_POSTPAID_CIRCLE = "SuccessDatacardPostpaidCircle";

    public static final String SUCCESS_DTH_NO = "SuccessDTHPNo";
    public static final String SUCCESS_DTH_OPERATOR = "SuccessDTHOperator";
    public static final String EXTRA_INTENT_FREQUENT_ORDERS = "frequent_orders";
    public static final int BARGAIN = 101;


    public static final String LAST_LIMIT = "lastLimit";
    public static final String START_LIMIT = "startLimit";
    public static final String USER_GUID = "userGuid";
    public static final String REQUEST = "request";
    //public static final String URL_LEDGER = "https://trust.paytm
    // .in/wallet-web/transactionWrapper";
    public static final String CREDIT = "CR";
    public static final String DEBIT = "DR";
    public static final String WEB_URL = "web_url";
    public static final int MAX_CHARACTER_STRIKE_OFF_PRICE = 11;
    public static final String PARSE_ERROR = "parsing_error";

    //	public static final String PINCODE_URL = "https://cart.paytm.com/v1/pincode/";

    public static final String FONT_FAMILY_SANS_SERIF = "sans-serif";

    public static final String FONT_FAMILY_SANS_SERIF_LIGHT = "sans-serif-light";
    public static final String FONT_FAMILY_SANS_SERIF_MEDIUM = "sans-serif-medium";
    public static final String IS_APP_RATED = "is_app_rated";

    public static final String INTENT_OPERATOR = "operator";
    public static final String INTENT_OPERATOR_TYPE = "operator_type";
    public static final String INTENT_CIRCLE = "cirlce";
    public static final String INTENT_TYPE = "type";
    public static final String Is_UARegistrationAPID_SEND = "is_ua_apid";

    public static final int ERROR_CODE_480 = 480;
    public static final int ERROR_CODE_499 = 499;
    public static final int ERROR_CODE_449 = 449;
    public static final int ERROR_CODE_502 = 502;
    public static final int ERROR_CODE_503 = 503;
    public static final int ERROR_CODE_504 = 504;
    public static final int ERROR_CODE_401 = 401;
    public static final int ERROR_CODE_402 = 402;
    public static final int ERROR_CODE_408 = 408;
    public static final int ERROR_CODE_410 = 410;
    public static final int ERROR_CODE_400 = 400;
    public static final int ERROR_CODE_412 = 412;
    public static final int ERROR_CODE_415 = 415;
    public static final int ERROR_CODE_416 = 416;
    public static final int ERROR_CODE_417 = 417;
    public static final int ERROR_CODE_753 = 753;
    public static final int ERROR_CODE_204 = 204;

    public static final String FAILURE = "failure";
    public static final String FAILURE_ERROR = "failure_error";
    public static final String LAST_APP_OPEN_DATE = "LastAppOpenDate";
    public static final String MAINTAINANCE_ERROR_503 = "maintaince_error_503";
    public static final String ALERT_TITLE = "alert_title";
    public static final String ALERT_MESSAGE = "alert_message";
    public static final String INDICATIVE_PLAN = "indicative_plan";
    public static final String FRAUDULENT_CUSTOMER = "CO_FRT_4011";

    public static final int NUNBER_OF_BOX_IN_WIDTH_TABLET_GRID_ITEM = 3;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_TABLET_GRID_ITEM = 5;
    public static final int NUNBER_OF_BOX_IN_HEIGHT_TABLET_GRID_ITEM_IMAGE = 3;

    public static final int ITEMS_PER_ROW_LANDSCAPE = 5;
    public static final int ITEMS_PER_ROW_PORTRAIT = 4;
    public static final String SPECIAL_RECHARGE_ID_1 = "7180";
    public static final String SPECIAL_RECHARGE_ID_2 = "7177";
    public static final String SPECIAL_RECHARGE_ID_3 = "7179";
    public static final String EVENT_APSALAR_BACK_PRESSED = "Back";

    public static final String GOOGLE_PLUS_SCOPE = "oauth2:profile email";
    public static final String SOCIAL_AUTH_TOKEN_PARAM_STRING =
            "grant_type=authorization_code&code=";
    public static final String KEY_SOCIAL_RESPONSE_TYPE = "response_type";
    public static final String KEY_SOCIAL_DO_NOT_REDIRECT = "do_not_redirect";
    public static final String KEY_SOCIAL_SCOPE = "scope";
    public static final String KEY_SOCIAL_LOGIN_CODE = "loginCode";
    public static final String KEY_SOCIAL_LOGIN_TYPE = "loginType";
    public static final String KEY_SOCIAL_CODE = "code";
    public static final String FACEBOOK = "facebook";
    public static final String GOOGLE = "google";
    public static final String KEY_SOCIAL_ID = "socialId";
    public static final String KEY_SOCIAL_PWD = "password";
    public static final String KEY_SOCIAL_AUTH_STATE = "authState";


    public static final String BUNDLE_EXTRA_AMOUNT = "bundle_extra_amount";
    public static final String BUNDLE_EXTRA_CATEGORY_ITEMS = "bundle_extra_category_items";
    public static final String BUNDLE_EXTRA_CATEGORY_ITEM = "bundle_extra_category_item";
    public static final String BUNDLE_EXTRA_SECONDARY_HOME = "bundle_extra_secondary_home";
    public static final String BUNDLE_EXTRA_LOAD_DATA = "bundle_extra_load_data";
    public static final String BUNDLE_EXTRA_SECONDARY_HOME_USER_VISIBLE =
            "bundle_extra_secondary_home_user_visible";

    public static final String URL_TYPE_MAIN = "main";
    public static final String URL_TYPE_DEALS = "deals";
    public static final String URL_TYPE_GOOGLE_NOW_PAYTM_WALLET = "wallet";
    public static final String URL_TYPE_UPDATES = "updates";
    public static final String URL_TYPE_MALL = "mall";
    public static final String SHOW_PASSWORD_SCREEN = "SHOW_PASSWORD_SCREEN";
    public static final String SHOW_SET_PASSWORD_SCREEN = "SHOW_SET_PASSWORD_SCREEN";
    public static final String PREF_KEY_HOME_FIRST_LAUNCH = "home_first_launch";
    public static final String PREF_KEY_ADD_MONEY_CLICKED = "PREF_KEY_ADD_MONEY_CLICKED";
    public static final String PREF_KEY_CLEAR_CACHE = "cleacr_cache";

    public static final String INTENT_EXTRA_BUS_SEARCH_FROM = "intent_extra_bus_search_from";
    public static final String INTENT_EXTRA_BUS_SEARCH_TO = "intent_extra_bus_search_to";
    public static final String INTENT_EXTRA_BUS_SEARCH_DATE = "intent_extra_bus_search_date";
    public static final String INTENT_EXTRA_BUS_SEARCH_NO_OF_PASSENGERS =
            "intent_extra_bus_search_no_of_passengers";
    public static final String INTENT_EXTRA_BUS_SEARCH_INPUT = "intent_extra_bus_search_input";
    public static final String INTENT_EXTRA_BUS_SEARCH_LOAD_DATA =
            "intent_extra_bus_search_load_data";
    public static final String INTENT_EXTRA_BUS_SEARCH_RESULT_ITEM =
            "intent_extra_bus_search_result_item";
    public static final String INTENT_EXTRA_BUS_SEARCH = "intent_extra_bus_search";
    public static final String INTENT_EXTRA_SELECT_SEAT_TRIP_LIST =
            "intent_extra_select_seat_trip_list";
    public static final String INTENT_EXTRA_BOARDING_POINT = "intent_extra_boarding_point";
    public static final String INTENT_EXTRA_SEAT_LIST = "intent_extra_seat_list";
    public static final String KEY_PARAM_BUS_SEARCH_SOURCE = "source";
    public static final String KEY_PARAM_BUS_SEARCH_DESTINATION = "destination";
    public static final String KEY_PARAM_BUS_SEARCH_DATE = "date";
    public static final String KEY_PARAM_BUS_SEARCH_COUNT = "count";
    public static final String KEY_PARAM_BUS_SEARCH_SOLD = "sold";
    public static final String KEY_PARAM_BUS_SEARCH_BLOCKED = "blocked";
    public static final String KEY_PARAM_BUS_SEARCH_MIN_FARE = "minFare";
    public static final String KEY_PARAM_BUS_SEARCH_MAX_FARE = "maxFare";
    public static final String KEY_PARAM_BUS_SEARCH_IS_SLEEPER = "isSleeper";
    public static final String KEY_PARAM_BUS_SEARCH_IS_AC = "isAc";
    public static final String KEY_PARAM_BUS_SEARCH_IS_LUXURY = "isLuxury";
    public static final String KEY_PARAM_BUS_SEARCH_MIN_DEPARTURE_TIME = "minDepartureTime";
    public static final String KEY_PARAM_BUS_SEARCH_MAX_DEPARTURE_TIME = "maxDepartureTime";
    public static final String KEY_PARAM_BUS_SEARCH_BOARDING_LOCATION = "boardingLocation";
    public static final String KEY_PARAM_BUS_SEARCH_DROPPING_LOCATION = "droppingLocation";
    public static final String KEY_PARAM_BUS_SEARCH_BUS_OPERATORS = "busOperators";
    public static final String KEY_PARAM_BUS_SEARCH_SORT_BY = "sortBy";
    public static final String KEY_PARAM_BUS_SEARCH_ORDER_BY = "orderBy";
    public static final String KEY_BUS_SEARCH_SORT_BY_FARE = "fare";
    public static final String KEY_BUS_SEARCH_SORT_BY_DURATION = "duration";
    public static final String KEY_BUS_SEARCH_SORT_BY_DEPARTURE_TIME = "departureTime";
    public static final String KEY_BUS_SEARCH_SORT_BY_NONE = "key_bus_search_sort_by_none";
    public static final int KEY_BUS_SEARCH_DESCENDING_ORDER = 1;
    public static final int KEY_BUS_SEARCH_ASCENDING_ORDER = 0;
    public static final String BUS_SEARCH_INPUT_TIME_FORMAT = "HHmm";
    public static final String BUS_SEARCH_DISPLAY_TIME_FORMAT = "h:mm a";
    public static final String BUS_SEARCH_INPUT_DATE_FORMAT = "yyyy-MM-dd";
    public static final String BUS_SEARCH_JOURNEY_DATE_FORMAT = "dd MMMM";
    public static final String BUS_SEARCH_DISPLAY_DATE_FORMAT = "EEE, dd MMM yy";
    public static final String BUS_SEARCH_PRICE_PATTERN = "###,###,###.##";
    public static final String INTENT_PASSENGER_DETAILS = "intent_extra_passenger_details";
    public static final String INTENT_BLOCK_TICKET_RESPONSE_DATA =
            "intent_block_ticket_response_data";
    public static final String PAYMENT_SUCCESS_FORMAT = "dd MMM, yyyy, hh:mm a";
    public static final String PUSH_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String PAYMENT_SUCCESS__CURRENT_TIME_FORMAT = "hh:mm a, dd MMM yyyy";
    public static final String PAYMENT_SUCCESS_BUS__CURRENT_TIME_FORMAT = "dd MMM yyyy , hh:mm:ss" +
            " a";

    public static final String INTENT_EXTRA_BUS_SEARCH_MAX_FARE =
            "intent_extra_bus_search_max_fare";
    public static final String INTENT_EXTRA_BUS_SEARCH_MIN_FARE =
            "intent_extra_bus_search_min_fare";
    public static final String INTENT_EXTRA_BUS_SEARCH_FILTER_ITEMS =
            "intent_extra_bus_search_filter_items";
    public static final String INTENT_EXTRA_SELECT_SEAT_ROW_COUNT =
            "intent_extra_select_seat_row_count";
    public static final String INTENT_EXTRA_SELECT_SEAT_COLUMN_COUNT =
            "intent_extra_select_seat_column_count";
    public static final String INTENT_EXTRA_SELECT_SEAT_IS_UPPER =
            "intent_extra_select_seat_is_upper";
    public static final String INTENT_EXTRA_SELECTED_SEATS = "intent_extra_selected_seats";
    public static final String INTENT_EXTRA_FILTERED_BOARDING_POINTS =
            "intent_extra_filtered_boarding_points";
    public static final String INTENT_EXTRA_LUXURY_BUS = "intent_extra_luxury_bus";

    public static final String INTENT_EXTRA_BUS_FILTER = "bus_filter";

    public static final String INTENT_EXTRA_LOCATION_TYPE = "intent_extra_location_type";
    public static final String INTENT_EXTRA_SELECTED_BOARDING_POINT =
            "intent_extra_selected_boarding_point";
    public static final String INTENT_EXTRA_SELECTED_DROPPING_POINT =
            "intent_extra_selected_dropping_point";
    public static final String INTENT_EXTRA_ORIGIN_CITY_LIST = "intent_extra_origin_city_list";
    public static final String INTENT_EXTRA_SELECTED_CITY_NAME = "intent_extra_selected_city_name";
    public static final String INTENT_EXTRA_SELECTED_CITY_POSITION = "intent_extra_selected_city_position";
    public static final String INTENT_EXTRA_LAST_KNOWN_SEARCH_KEYWORD = "intent_extra_last_known_search_keyword";
    public static final String INTENT_EXTRA_CITY_CATAGORY = "intent_extra_city_catagory";
    public static final String INTENT_EXTRA_GOLD_BUILDER = "intent_gold_builder";
    public static final String INTENT_EXTRA_GOLD_FILTER_BUILDER = "intent_gold_filter_builder";
    public static final String INTENT_EXTRA_SEND_GOLD_SUCCESS = "intent_gold_send_success";
    public static final String INTENT_EXTRA_GOLD_VALID_TIMER = "intent_gold_timer_count";
    public static final String INTENT_EXTRA_GOLD_KYC_ERROR = "intent_gold_error_text";
    public static final String INTENT_EXTRA_IS_GOLD = "intent_extra_is_gold";
    public static final String INTENT_EXTRA_GOLD_ITEM = "intent_gold_item";
    public static final String INTENT_EXTRA_GOLD_ORIGIN = "intent_gold_origin";
    public static final String INTENT_EXTRA_GOLD_SUMMARY = "intent_gold_summary";
    public static final String INTENT_EXTRA_GOLD_SELL_PRICE = "intent_gold_sell_price";
    public static final String INTENT_EXTRA_GOLD_BUY_PRICE = "intent_gold_buy_price";

    public static final int INTENT_EXTRA_GOLD_REQUEST_CODE = 345;
    public static final int INTENT_RESULT_CODE_KYC_FLAG = 122;
    public static final int INTENT_RESULT_TIMER_TIMEOUT = 123;
    public static final int INTENT_RESULT_UPDATE_USER_INFO = 124;

    public static final String INTENT_FILTER_HOME = "home";
    public static final String INTENT_FILTER_BUY = "buy";
    public static final String INTENT_FILTER_SELL = "sell";
    public static final String INTENT_FILTER_P2P = "p2p";
    public static final String INTENT_EXTRA_GOLD_USER_NAME = "gold_user_name";
    public static final String INTENT_EXTRA_GOLD_USER_PINCODE = "gold_user_pincode";



    public static final String INTENT_EXTRA_SELECTED_JOURNEY_DATE =
            "intent_extra_selected_journey_date";
    public static final String INTENT_EXTRA_BUS_START_CITY = "intent_extra_bus_start_city";
    public static final String INTENT_EXTRA_BUS_DEST_CITY = "intent_extra_bus_dest_city";
    public static final String INTENT_EXTRA_BUS_INSURANCE = "intent_extra_bus_insurance_enabled";
    public static final String INTENT_EXTRA_BUS_INSURANCE_PLAN = "intent_extra_bus_insurance_plan";
    public static final String INTENT_EXTRA_BUS_TRIP_DETAIL = "intent_extra_bus_trip_detail";

    public static final int INTENT_BUS_SEAT_REQ_CODE = 2;
    public static final int INTENT_BUS_SEAT_RESULT_CODE = 3;
    public static final int INTENT_BUS_RI_RESULT_CODE = 4;

    public static final int LOCATION_TYPE_BOARDING = 1;
    public static final int LOCATION_TYPE_DROPPING = 2;
    public static final String KEY_SEATS_TRIP_ID = "id";
    public static final String KEY_SEATS_INFO = "info";
    public static final String KEY_SEATS_PROVIDER_ID = "provider_id";
    public static final String KEY_SEATS_REQUEST_ID = "requestid";
    public static final String KEY_SEATS_DEPARTURE_DATE = "departureDate";

    public static final String KEY_PREPROCESS_SERVICE = "service";
    public static final String KEY_PREPROCESS_TRIP_ID = "tripId";
    public static final String KEY_PREPROCESS_PROVIDER_ID = "providerId";
    public static final String KEY_PREPROCESS_SOURCE = "source";
    public static final String KEY_PREPROCESS_DESTINATION = "destination";
    public static final String KEY_PREPROCESS_BOARDING_POINT_ID = "boardingPointId";
    public static final String KEY_PREPROCESS_BOARDING_POINT = "boardingPoint";
    public static final String KEY_PREPROCESS_DROPPING_POINT_ID = "droppingPointId";
    public static final String KEY_PREPROCESS_DROPPING_POINT = "droppingPoint";
    public static final String KEY_PREPROCESS_DATE = "date";
    public static final String KEY_PREPROCESS_APP_NAME = "appName";
    public static final String KEY_PREPROCESS_TOTAL_FARE = "totalFare";
    public static final String KEY_PREPROCESS_APPLY_COUPON = "applyCoupon";
    public static final String KEY_PREPROCESS_COUPON_CODE = "couponCode";
    public static final String KEY_PREPROCESS_PASSENGERS = "passengers";
    public static final String KEY_PREPROCESS_SSO_TOKEN = "sso_token";
    public static final String KEY_PREPROCESS_SSO_TOKEN_ENC = "sso_token_enc";
    public static final String KEY_PREPROCESS_USER_ID = "user_id";
    public static final String KEY_PREPROCESS_OTHER_TRIP_DETAILS = "otherTripDetails";
    public static final String KEY_PREPROCESS_NAME = "name";
    public static final String KEY_PREPROCESS_AGE = "age";
    public static final String KEY_PREPROCESS_TITLE = "title";
    public static final String KEY_PREPROCESS_GENDER = "gender";
    public static final String KEY_PREPROCESS_PRIMARY = "primary";
    public static final String KEY_PREPROCESS_SEAT_NUMBER = "seatNumber";
    public static final String KEY_PREPROCESS_MOBILE_NUMBER = "mobileNumber";
    public static final String KEY_PREPROCESS_ALTERNAIVE_MOBILE_NUMBER = "alternateMobileNumber";
    public static final String KEY_PREPROCESS_EMAIL = "email";
    public static final String KEY_PREPROCESS_ADDRESS = "address";
    public static final String KEY_PREPROCESS_ID_TYPE = "idType";
    public static final String KEY_PREPROCESS_ID_NAME = "idName";
    public static final String KEY_PREPROCESS_ID_NUMBER = "idNumber";
    public static final String KEY_PREPROCESS_TICKET_ID = "ticket_id";
    public static final String KEY_PREPROCESS_BLOCK_KEY = "block_key";
    public static final String KEY_PREPROCESS_DATE_OF_JOURNEY = "date_of_journey";
    public static final String KEY_PREPROCESS_IS_INSURANCE = "isInsurance";
    public static final String KEY_PREPROCESS_INSURANCE_DETAILS = "insurance_details";
    public static final String KEY_PREPROCESS_INSURANCE_ID = "id";

    public static final String KEY_INSURANCE_VERTICAL_ID = "vertical_id";
    public static final String KEY_INSURANCE_START_DATE = "start_date";
    public static final int VALUE_INSURANCE_VERTICAL_ID = 26;
    public static final String KEY_PREPROCESS_REQUESTID = "requestid";
    public static final String KEY_PREPROCESS_CHANNEL = "channel";
    public static final String KEY_PREPROCESS_VERSION = "version";
    public static final String KEY_CHANNEL_VALUE = "android";
    public static final int KEY_VERSION_VALUE = 2;

    public static final String KEY_VALIDATION_CART_ITEMS = "cart_items";
    public static final String KEY_VALIDATION_PRODUCT_ID = "product_id";
    public static final String KEY_VALIDATION_QTY = "qty";
    public static final String KEY_VALIDATION_CONFIGURATION = "configuration";
    public static final String KEY_VALIDATION_META = "meta_data";
    public static final String KEY_VALIDATION_FROM = "from";
    public static final String KEY_VALIDATION_TO = "to";
    public static final String KEY_VALIDATION_SOURCE = "source";
    public static final String KEY_VALIDATION_DESTINATION = "destination";
    public static final String KEY_VALIDATION_PRICE = "price";
    public static final String KEY_VALIDATION_TRAVEL_DATE = "travel_date";
    public static final String KEY_VALIDATION_TRAVEL_TIME = "travel_time";
    public static final String KEY_VALIDATION_PROVIDER_ID = "provider_id";
    public static final String KEY_VALIDATION_TRAVEL_NAME = "travelName";
    public static final String KEY_VALIDATION_PROMOCODE = "promocode";
    public static final String KEY_VALIDATION_UPSELL = "upsell";
    public static final String KEY_VALIDATION_CART = "cart";
    public static final String KEY_VALIDATION_SERVICE_OPTIONS = "service_options";
    public static final String KEY_VALIDATION_ACTIONS = "actions";
    public static final String KEY_VALIDATION_DISPLAY_VALUES = "displayValues";

    public static final String KEY_VALIDATION_PASSANGER_COUNT = "passengerCount";

    public static final String BUS_SEARCH_RESULT_DATA = "bus_search_result_data";

    public static final String BUS_TICKET_FILTER_TITLE_AC = "AC";

    public static final String BUS_TICKET_FILTER_TITLE_NON_AC = "Non AC";

    public static final String BUS_TICKET_FILTER_TITLE_SLEEPER = "Sleeper";

    public static final String BUS_TICKET_FILTER_TITLE_SEATER = "Seater";

    public static final String BUS_TICKET_FILTER_TITLE_LUXURY = "Luxury";
    public static final String BUS_TICKET_FILTER_TITLE_MULTI_AXLE = "Multi Axle";

    public static final String BUS_TICKET_FILTER_TITLE_VOLVO = "Volvo";
    public static final String BUS_TICKET_FILTER_TITLE_MERCEDES = "Mercedes";
    public static final String BUS_TICKET_FILTER_TITLE_SCANIA = "Scania";


    public static final String BUS_TICKET_FILTER_TITLE_DEPARTURE_TIME = "Deprture Time";
    public static final String BUS_TICKET_FILTER_TITLE_PRICE = "Price";
    public static final String BUS_TICKET_FILTER_TITLE_BOARDING_POINT = "B.Pt";
    public static final String BUS_TICKET_FILTER_TITLE_DROPPING_POINT = "D.Pt";
    public static final String BUS_TICKET_FILTER_TITLE_BUS_OPERATORS = "Opr";

    public static final String BUS_TICKET_FILTER_TYPE_BOOLEAN = "boolean";
    public static final String BUS_TICKET_FILTER_TYPE_RANGE_SLIDER = "range_slider";
    public static final String BUS_TICKET_FILTER_TYPE_TIME_RANGE_SLIDER = "time_range_slider";
    public static final String BUS_TICKET_FILTER_TYPE_LIST = "list";
    public static final String EXTRA_INTENT_MOBILE_NUMBER_REF = "mobileNumber";
    public static final String EXTRA_INTENT_AMOUNT_REF = "amount";
    public static final String EXTRA_INTENT_RECHARGE_TYPE_REF = "rechargeType";
    public static final String EXTRA_INTENT_REFERRAL_SOURCE = "referralSource";
    public static final String EXTRA_INTENT_LAUNCH_SIGN_UP = "launchSignUp";
    public static final String SIGN_IN_TITLE = "sign_in_title";
    public static final String SIGN_UP_TITLE = "sign_up_title";

    public static final String KEY_CONTACT_EMAIL = "email";
    public static final String KEY_CONTACT_MOBILE = "Mobile_Number__c";
    public static final String KEY_CONTACT_L2 = "l2";
    public static final String KEY_CONTACT_L3 = "l3";
    public static final String KEY_CONTACT_ORDER_ID = "orderId";
    public static final String KEY_CONTACT_TXNID = "txnId";
    public static final String KEY_CONTACT_QUERYTYPE = "queryType";
    public static final String KEY_CONTACT_MESSAGE = "message";
    public static final String KEY_CONTACT_NEEDSHIPPING = "needShipping";
    public static final String KEY_CONTACT_VERTICAL_LABLE = "verticalLabel";
    public static final String KEY_CONTACT_US_ITEMSTATUS = "itemStatus";
    public static final String KEY_CONTACT_US_ITEMNAME = "itemName";
    public static final String KEY_CONTACT_US_CHANNEL = "channel";
    public static final String KEY_CONTACT_US_VERSION = "version";
    public static final String KEY_CONTACT_US_SECURITY = "isSecurity";
    public static final String KEY_CONTACT_US_ITEMID = "item_id";
    public static final String KEY_CONTACT_US_SOURCE = "source";
    public static final String KEY_CONTACT_IMAGEURL = "imageUrl";
    public static final String KEY_CONTACT_IMAGENAME = "imageName";
    public static final String KEY_CONTACT_MIMETYPE = "mimeType";
    public static final String KEY_CONTACT_TXNITEMS = "txnItems";
    public static final String KEY_CONTACT_VERTICAL_LABEL = "verticalLabel";
    public static final String KEY_CONTACT_UTM_RESOURCE = "utm_source";
    public static final String KEY_CONTACT_SECURITY = "isSecurity";
    public static final String KEY_CONTACT_VERTICAL_ID = "vertical_id";
    public static final String KEY_CONTACT_CUSTOMER_ID = "Customer_Id__c";
    public static final String KEY_CONTACT_MOBILE_NUMBER = "Mobile_Number__c";
    public static final String KEY_CONTACT_WALLET_TXN_ID = "Wallet_txn_ID__c";
    public static final String KEY_CONTACT_US_BANK_ACCOUNT_NUMBER = "CBS_Account_number_c";
    public static final String KEY_CONTACT_US_BANK_TXN_DATE = "CBS_transaction_time_c";
    public static final String KEY_CONTACT_US_BANK_TXN_ID = "CBS_Txn_ID_c";

    public static final String SHIPPED_ON = "Shipped on ";
    public static final String INTENT_EXTRA_TRIP_DATA = "intent_extra_trip_data";

    public static final String KEY_CLIENT_ID = "clientId";

    public static final String KEY_SCOPE = "scope";
    public static final String KEY_VALUE_SCOPE = "paytm";
    public static final String KEY_SIGN_UP_STATE = "state";
    public static final String KEY_VALUE_SIGN_UP_STATE = "xyz";
    public static final String KEY_DO_NOT_REDIRECT = "doNotRedirect";
    public static final String KEY_RESPONSE_TYPE = "responseType";
    public static final String KEY_CODE = "code";
    public static final String KEY_STATUS = "status";


    public static final String SET_RESULT_REQUIRED = "set_result_required";

    public static final String PREF_KEY_USER_NOT_VERIFIED = "USER_NOT_VERIFIED";
    public static final String PREF_KEY_USER_SKIPPED_VERIFICATION = "USER_SKIPPED_VERIFICATION";
    public static final String EXTRA_INTENT_TRANSACTION_FLOW_STEP_2 = "txn_flow_step_2";
    public static final String EXTRA_INTENT_FROM_WALLET = "extra_intent_from_wallet";
    public static final String EXTRA_INTENT_FROM_WEAR_LAUNCH_FAVOURITES =
            "extra_intent_from_wear_launch_favourites";

    public static final String KEY_SIGN_UP_FIRST_NAME = "firstName";
    public static final String KEY_SIGN_UP_LAST_NAME = "lastName";
    public static final String KEY_SIGN_UP_DOB = "dob";
    public static final String KEY_SIGN_UP_GENDER = "gender";
    public static final String KEY_SIGN_UP_USER_DATA = "userData";
    public static final String KEY_SIGN_UP_TOKEN = "signupToken";
    public static final String KEY_SIGN_UP_OTP = "otp";

    public static final String KEY_CANCELLATION_POLICY_JSON = "cancellationPolicyJSON";
    public static final String KEY_DEPARTURE_HEADING = "departure_heading";
    public static final String KEY_POLICY_HEADING = "policy_heading";
    public static final String EXTRA_INTENT_FROM_WEAR = "intent_from_wear";
    public static final String BUS_CITY_URL_TYPE = "bus_city_url_type";

    public static final String COUNTLY_KEY_DATE = "date";
    public static final String COUNTLY_KEY_MOBILE_NUMBER = "mobile_number";
    public static final String COUNTLY_KEY_DEVICE_ID = "device_id";
    public static final String COUNTLY_KEY_DEVICE = "device";
    public static final String COUNTLY_KEY_APP_VERSION = "app_version";
    public static final String COUNTLY_KEY_TRANSACTION_COST = "transaction_cost";
    public static final String COUNTLY_KEY_TRANSACTION_TYPE = "transaction_type";
    public static final String COUNTLY_KEY_TRANSACTION_STATUS = "transaction_status";
    public static final String COUNTLY_KEY_TRANSACTION_CIRCLE = "transaction_circle";
    public static final String COUNTLY_KEY_OS = "os";
    public static final String COUNTLY_KEY_IP_ADDRESS = "ip_address";

    public static final String COUNTLY_EVENT_KEY_TRANSACTION = "transactions";
    public static final String COUNTLY_EVENT_KEY_ACTIVATION = "activations";
    public static final String COUNTLY_IS_ACTIVATION_TRACKED = "countly_activation_tracking";
    public static final String EXTRA_INTENT_TAB_POSITION_FIRST = "first_tab_home";
    public static final String EXTRA_INTENT_TAB_POSITION_THIRD = "third_tab_home";
    public static final long TIMEOUT_FOR_CONTAINER_OPEN_MILLISECONDS = 2000;

    public static final String CURRENT_SAVED_VERSION = "current_saved_version";

    public static final String API_KEY_VERSION = "version";
    public static final String API_KEY_NUMBER = "number";

    public static final String FAV_PRODUCT_ID = "productId";
    public static final String FAV_RECHARGE_NUMBER = "rechargeNumber";
    public static final String FAV_NUMBER_PRICE = "price";
    public static final String FAV__LABEL = "favLabel";
    public static final String FAV_LABEL_ID = "favLabelId";

    public static final String KEY_DEFAULT_DOB = "default_dob";

    public static final String RTB_FILTERED_USER_TXN_HISTORY = "FILTERED_USER_TXN_HISTORY";
    public static final String RTB_PLATFORM_NAME = "PayTM";
    public static final String RTB_MERCHANT_TO_PAYER_COMPLEX_REFUND =
            "MERCHANT_TO_PAYER_COMPLEX_REFUND";

    //twitter
    public static final String TWITTER = "twitter";
    public static final String TWITTER_KEY_TYPE = "type";
    public static final String KEY_SOCIAL_CREDENTIALS = "socialCredentials";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_SECRET = "secret";
    public static final String TWITTER_CALL_TYPE_CONNECT = "twitter_call_type_connect";
    public static final String TWITTER_CALL_TYPE_DISCONNECT = "twitter_call_type_disconnect";
    public static final String TWITTER_CONSUMER_KEY =
            "uAwwyLXpQrGL_eP6ZP6XwdqQYeKzc0qtytCvPxzXPGo=";
    public static final String TWITTER_CONSUMER_SECRET =
            "YHJiT4Ndcc6f8XWd3NCWutTfAZ2bWphFKHS0m1bPTwNcySpaqVsWmENG1WPiFhC2liqwn" +
                    "-MnM8fYaNh5of_Qvg==";
    public static final String KEY_CONSUMER_KEY = "consumerKey";

    public static final String BUS_FILTER_DATA = "bus_filter_data";
    public static final String AUTOMATIC_PRODUCT_ID = "product_id";
    public static final String AUTOMATIC_QTY = "qty";
    public static final String AUTOMATIC_CONFIGURATION = "configuration";
    public static final String AUTOMATIC_SUBSCRIBE = "subscribe";
    public static final String AUTOMATIC_SUBSCRIPTION_INFO = "subscriptionInfo";
    public static final String AUTOMATIC_INTERVAL = "interval";
    public static final String AUTOMATIC_EXPIRY_DATE = "expiryDate";
    public static final String AUTOMATIC_CART_ITEMS = "cart_items";
    public static final String AUTOMATIC_SUBSCRIPTION_AMOUNT = "amount";
    public static final String MARKETPLACE = "Marketplace";
    public static final String PREF_KEY_TWITTER_INFO_STORED_AT_SERVER =
            "pref_key_twitter_info_stored_at_server";
    public static final String INTENT_EXTRA_RETURN_TWITTER_CONNECT_RESULT =
            "intent_extra_return_twitter_connect_result";
    public static final String AUTOMATIC_CART = "cart";

    public static final String AUTOMATIC_RECHARGE_NUMBER = "recharge_number";
    public static final String AUTOMATIC_PRICE = "price";
    public static final String AUTOMATIC_SUBSCRIPTION_DURATION = "duration";
    public static final String AUTOMATIC_NEXT_DUE_DATE = "automatic_due_date";
    public static final String AUTOMATIC_RECHARGE_OPERATOR_URL = "automatic_operator_url";
    public static final String AUTOMATIC_NAME = "automatic_name";
    public static final String AUTO_RECHARGE = "Auto_Recharge";
    public static final String AUTOMATIC_TYPE = "type";
    public static final String AUTOMATIC_SAVED_CARD_ID = "savedCardId";
    public static final String AUTOMATIC_PAYMENT_MODE = "paymentMode";
    public static final String AUTOMATIC_TYPE_S2S = "S2S";
    public static final String STATUS_ERROR = "ERROR";

    public static final String INTENT_EXTRA_PRODUCT_IMAGES = "intent_extra_product_images";
    public static final String INTENT_EXTRA_CURRENT_PRODUCT_ITEM_POSITION =
            "intent_extra_current_product_item_position";
    public static final String INTENT_EXTRA_PAGE_TITLE = "intent_extra_page_title";

    //TargetingMantra
    public static final String TM_HOMEPAGE = "homepage";
    public static final String TM_CURRENCY = "Rs";
    public static final String TM_REALM = "asia";
    public static final String TM_BEST_SELLERS = "Best Sellers";
    public static final int TM_MID = 150273;

    public static final String IS_COMPLETED_AUTOMATIC_LIST = "completed_list";

    public static final int ALARM_SERVICE_REQUEST_CODE = 0;
    //    public static final String CHECK_CREDENTIALS_URL = "https://cart.paytm
    // .com/api/cards/validatetoken";
//    public static final String ADD_CREDENTIALS_URL ="https://cart.p`aytm.com/api/cards/authcode";
    public static final String REVOKE_CREDENTIALS_URL = "https://accounts.google" +
            ".com/o/oauth2/revoke";
    public static final String METHOD_EXTRA = "method";
    //    public static final String METHOD_EXTRA_CREDENTIALS = "credential";
    public static final String PARAMS_EXTRA = "params";
    //    public static final String USER_PARAM = "user";
    public static final String GOOGLE_NOW_BROADCAST_ACTION = "com.google.api.services.paytm.now" +
            ".paytmclient.BROADCAST";
    public static final String GOOGLE_NOW_SERVER_CLIENT_ID =
            "UnK_24esVfn6ALoNxAzM28qVGeHPeuUhvXKoEblqS8xprPKsUO5SFJzc1oEpdmeCOcNbLJfCJUah\n" +
                    "    FlrKRWcguAWZDzQnujGDHmAKoarMivM=";
    //"64647637672-q5q06ojbtjubsic6ra8sv3r338me6npm.apps.googleusercontent.com";
    public static final String ACCESS_TOKEN_EXTRA = "accessToken";
    public static final String AUTH_CODE_EXTRA = "authCode";
    public static final String AUTH_CODE_PARAM = "auth_code";
    public static final String GET_AUTH_CODE_METHOD = "GetAuthCode";
    public static final String VALID_CREDENTIALS_SERVER_RESPONSE = "SUCCESS";
    public static final String DATA_STATUS_EXTRA = "statusCode";
    public static final String DATA_RESPONSE_EXTRA = "responseText";

    public static final String PAYTM_CASH_FAQ = "https://pages.paytm.com/help.html#newcash";
    public static final String IS_HIDE_WEBVIEW_ZOOM = "hide_webview_zoom";
    public static final int LEDGER_UPDATE_TIME_IN_MINUTE = 10;
    public static final String PREF_KEY_FETCHING_USER_INFO = "pref_key_fetching_user_info";
    public static final String GTM_CATEGORY = "gtm_category";

    public static final String PREF_KEY_LATITUDE = "pref_key_latitude";
    public static final String PREF_KEY_LONGITUDE = "pref_key_longitude";
    public static final String PREF_KEY_WATCH_INSTALL = "pref_key_watch_install";

    public static final String PAYTM_AUTOMATIC = "paytm_automatic";

    public static final String FS_NAME_EVENT = "Entertainment Events";
    public static final String FS_NAME_TICKETS = "Tickets";
    public static final String FS_NAME_WALLET = "Wallet";
    public static final String FS_NAME_DEFAULT = "Default";
    public static final String FS_NAME_RECHARGE = "Recharge";
    public static final String FS_NAME_HOTELS = "Paytm Hotel";
    public static final String FS_NAME_EDUCATION = "Education";
    public static final String FS_NAME_INSURANCE = "Premium";
    public static final String FS_NAME_MOVIES = "Movie Tickets";
    public static final String FS_NAME_FLIGHTS = "Flights";
    public static final String FS_NAME_TRAINS = "Trains";
    public static final String FS_NAME_GIFT_CARDS = "Gift Cards";
    public static final String FS_NAME_THEME_PARKS = "Theme Parks";
    public static final String FS_NAME_DIGITAL_GOLD = "Digital Gold";
    public static final String FS_NAME_GOLD = "gold";

    public static final String HTTP_METHOD_POST = "POST";
    public static final String HTTP_METHOD_GET = "GET";

    public static final String FS_NAME_SHOPPING_CART = "Shopping_cart";

    public static final String APPSFLYER_DEV_KEY = "wpZN8Fuq9nbFGqBnnDLU3H";
    public static final String APPSFLYER_GCM_KEY = "64647637672";

    public static final String APPSFLYER_EVENT_PURCHASE = "Purchase";
    public static final String APPSFLYER_EVENT_FIRST_TRANSACTION = "First_Transaction";
    public static final String APPSFLYER_EVENT_TRAVEL_BOOKING = "af_travel_booking";
    public static final String APPSFLYER_EVENT_KEY_ORDER_ID = "af_order_id";

    public static final String GET_MOBILE_NUMBER = "GET_MOBILE_NUMBER";
    public static final String BUNDLE_EXTRA_FROM_SOCIAL_SIGN_UP =
            "bundle_extra_from_social_sign_up";
    public static final String BUNDLE_EXTRA_AUTH_STATE = "bundle_extra_auth_state";
    public static final String ORIGIN_RECOMMENDED = "recommended";
    public static final String INTENT_EXTRA_HIDE_OPTIONS_MENU_ICONS =
            "intent_extra_hide_options_menu_icons";
    public static final String URL_TYPE_CONTACT_US = "contactus";
    public static final String URL_TYPE_WITHDRAW_MONEY = "withdraw_money";
    public static final String WALLET_FEATURE_TYPE_SEND_MONEY = "send_money";
    public static final String WALLET_FEATURE_TYPE_REQUEST_MONEY = "request_money";
    public static final String WALLET_FEATURE_TYPE_ADD_MONEY = "add_money";
    public static final String WALLET_FEATURE_TYPE_SHOW_OTP = "show_code";
    public static final String WALLET_FEATURE_TYPE_SEND_MONEY_BANK = "send_money_bank";
    public static final String WALLET_FEATURE_TYPE_SEND_MONEY_P2P = "sendmoneymobile";
    public static final String SHOW_OTP_SCREEN = "SHOW_OTP_SCREEN";
    public static final String MOBILE_OTP_SUCCESS_TITLE = "There You Go !";
    public static final String MOBILE_OTP_SUCCESS_MSG = "Your number has been updated to";

    public static final String URL_TYPE_METRO = "metro";
    public static final String KEY_ADVERTISING_ID = "ADVERTISING_ID";
    public static final String EXTRA_INTENT_LOAD_WISHLIST = "need_wishlist_open";
    public static final String WISH_LIST_SOURCE = "wishlist_source";
    public static final String VARIANT_LIST = "VARIANT_LIST";
    public static final String KEY_META_DESCRIPTION = "meta_description";
    public static final String KEY_GROUP_FIELDS = "group_fields";
    public static final String URL_TYPE_UTILITY = "utility";
    public static final String KEY_RELATED_CATEGORY = "related_category";
    public static final String KEY_LABEL = "label";
    public static final String NEW_REGESTRATION_URL_TYPE = "new_registration";
    public static final String EXTRA_INTENT_PRODUCT = "recharge_product";
    public static final String CHECK_BOX = "checkbox";
    public static final String KEY_VERTICAL_EDUCATION = "vertical_name";
    public static final String RECHARGE_UTILITY_NAME = "recharge_utility_name";
    public static final String MIN_DATE = "min_date";
    public static final String MAX_DATE = "max_date";
    public static final String DATE_FORMAT = "date_format";
    public static final String DECIMAL_NUMBER_TYPE = "decimal_number_type";
    public static final String CART_ITEM = "cart_product";
    public static final String WISH_LIST_ITEM = "wish_list_item";
    public static final String CART_APPLIED_PROMO = "cart_applied_promo";
    public static final String COUPON_APPLIED_PROMO = "appliedPromo";
    public static final String SP_SCAN_RESULT = "scanResult";
    public static final String ADDRESS_ID = "address_id";
    public static final String ACTION_REMOVE_ADDRESS = "removeaddress";

    public static final String KEY_PACKAGE_NAME = "package_name";

    public static final String INTENT_EXTRA_IS_NORMAL_SEARCH = "is_normal_search";
    public static final String INTENT_EXTRA_FEEDBACK_OPTIONS = "feedback_options";
    public static final String INTENT_EXTRA_SEARCH_USER_ID = "search_user_id";
    public static final String INTENT_EXTRA_SEARCH_ID = "search_id";
    public static final String INTENT_EXTRA_SEARCH_KEY = "search_key";
    public static final String BILL_PAYMENT = "Bill Payment";
    public static final String EDUCATION_VERTICAL_TYPE = "education_type";
    public static final int REQUEST_CODE_SEND_FEEDBACK = 600;

    // SEARCH USER TRANSACTION HISTORY
    public static final String PAYER_NAME = "payerName";
    public static final String PAYEE_NAME = "payeeName";
    public static final String TXN_TYPE = "txnType";
    public static final String ID = "id";
    public static final String TXN_AMOUNT = "txnAmount";
    public static final String FROM_K = "from";
    public static final String TO = "to";
    public static final String all = "textToSearch";
    public static final String ACC_NUMBER = "accountNumber";
    public static final String IS_FROM_PDP = "is_from_pdp";
    public static final String PDP_CART_INTENT = "pdp_cart_intent";
    public static final String INTENT_EXTRA_CHILD_DEC_COUNT = "child_dec";
    public static final String INTENT_EXTRA_ADULT_MALE_DEC_COUNT = "adult_male_dec";
    public static final String INTENT_EXTRA_ADULT_FEMALE_COUNT = "adult_female_dec";
    public static final String INTENT_EXTRA_SENIOR_MALE_DEC_COUNT = "senior_male_dec";
    public static final String INTENT_EXTRA_SENIOR_FEMALE_DEC_COUNT = "senior_female_dec_count";
    public static final String INTENT_EXTRA_ADULT_FEMALE_DEC_COUNT = "adult_female_dec_count";

    public static final String PREF_KEY_EASY_PAY = "easyPay";
    public static final String VER_O2O_PREFIX = "0000000001";
    public static final String VER_BRTS_PREFIX = "000000000102";

    public static final int SUCCESS_CODE_200 = 200;

    public static final int ERROR_CODE_403 = 403;
    public static final String YOUR_ORDERS_ALL_LIST = "All";
    public static final String YOUR_ORDERS_RECHARGE_LIST = "RechargeAndBills";
    public static final String URL_TYPE_HOTELS = "hotel-booking";
    public static final String HOTEL_FILTER_TYPE_LIST = "list";
    public static final String HOTEL_FILTER_TYPE_RANGE_SLIDER = "range_slider";
    public static final String HOTEL_FILTER_TYPE_SINGLE_VALUE = "single_value";
    public static final int APPLY_OFFER = 200;
    public static final int EDIT_CART_ACTIVITY = 300;
    public static final int WISH_LIST_APPLY_OFFER = 400;
    public static final String ADDRESS_FRAGMENT = "address_fragment";
    public static final String NEW_ADDRESS_FRAGMENT = "new_address_fragment";
    public static final String ADDRESS_LIST = "address_list";
    public static final String KEY_PRIORITY = "priority";
    public static final String KEY_TITTLE = "title";
    public static final int SELECT_QTY_RESULT = 101;
    public static final String DEFAULT_ADDRESS = "Default_address";
    public static final String PREF_KEY_PENDING_REQUESTS_LIST_UPDATE_REQUIRED =
            "pref_key_pending_requests_list_update_required";
    public static final String PREF_KEY_YOUR_REQUESTS_LIST_UPDATE_REQUIRED =
            "pref_key_your_requests_list_update_required";
    public static final String PREF_KEY_WALLET_BALANCE = "pref_key_wallet_balance";
    public static final String PREF_KEY_SHOW_WALLET_APP_AD_BANNER =
            "pref_key_show_wallet_app_ad_banner";
    public static final String URL_TYPE_SELLER_STORE = "sellerstore";
    public static final String SELLER_HOME_PAGE_ROW = "row";
    public static final double CAROUSEL_ASP_RATIO_SELLERSTORE = 3.5;
    public static final double BANNER_ASP_RATIO_SELLERSTORE = 3;
    public static final double BANNER_ASP_RATIO_BRANDSTORE = 2;
    public static final String SELLER_HOME_PAGE_CAROUSEL_1 = "carousel-1";
    public static final String NO_ADDRESS_FOUND = "no address";
    public static final String URL_TYPE_FLIGHTTICKET = "flightticket";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_ORIGIN_KEY = "intent_extra_flight_search_from_key";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_DESTINATION_KEY = "intent_extra_flight_search_to_key";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_INPUT = "intent_extra_bus_search_input";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_FROM = "intent_extra_flight_search_from";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_ORIGIN_POS = "intent_extra_flight_search_from_pos";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_DESTINATION_POS = "intent_extra_flight_search_to_pos";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_TO = "intent_extra_flight_search_to";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_DATE = "intent_extra_flight_search_date";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_NO_OF_ADULT_PASSENGERS =
            "intent_extra_flight_search_no_of_adult_passengers";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_NO_OF_CHILD_PASSENGERS =
            "intent_extra_flight_search_no_of_child_passengers";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_NO_OF_INFANTS_PASSENGERS =
            "intent_extra_flight_search_no_of_infants_passengers";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_FILTER_ITEMS =
            "intent_extra_flight_search_filter_items";
    public static final String INTENT_EXTRA_FLIGHT_AIRLINE_SEARCH_LISTED =
            "intent_extra_flight_airline_search_listed";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_FILTER_ITEMS1 =
            "intent_extra_flight_search_filter_items1";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_LOAD_DATA =
            "intent_extra_flight_search_load_data";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_MAX_FARE =
            "intent_extra_flight_search_max_fare";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_MIN_FARE =
            "intent_extra_flight_search_min_fare";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_MAX_DURATION =
            "intent_extra_flight_search_max_duration";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_IS_ROUND_TRIP =
            "INTENT_EXTRA_FLIGHT_SEARCH_IS_ROUND_TRIP";
    public static final String INTENT_EXTRA_FLIGHT_SEARCH_MIN_DURATION =
            "intent_extra_flight_search_min_duration";
    public static final String FLIGHT_TICKET_FILTER_TITLE_PRICE = "Price";
    public static final String FLIGHT_TICKET_FILTER_TITLE_REFUNDABLE = "Refundable";
    public static final String FLIGHT_TICKET_FILTER_TITLE_NONREFUNDABLE = "Non-refundable";
    public static final String FLIGHT_TICKET_FILTER_TITLE_NOSTOP = "nostop";
    public static final String FLIGHT_TICKET_FILTER_TYPE_STOPS = "stops";
    public static final String FLIGHT_TICKET_FILTER_TYPE_AIRLINE = "airline";
    public static final String FLIGHT_TICKET_FILTER_TITLE_ONESTOP = "onestop";
    public static final String FLIGHT_TICKET_FILTER_TITLE_MORETHANONESTOP = "morethanonestop";
    public static final String FLIGHT_TICKET_FILTER_TITLE_ONWARD_DEPT_TIME = "Onward_Dept_Time";
    public static final String FLIGHT_TICKET_FILTER_TITLE_RETURN_DEPT_TIME = "Return_Dept_Time";
    public static final String FLIGHT_TICKET_FILTER_TITLE_DURATION = "Duration";
    public static final String FLIGHT_TICKET_FILTER_TYPE_RANGE_SLIDER = "range_slider";
    public static final String FLIGHT_TICKET_FILTER_TYPE_DURATION_SLIDER = "duration_slider";
    public static final String FLIGHT_TICKET_FILTER_TYPE_REFUNDABLE_CHECK = "refundable_check";
    public static final String FLIGHT_RECENT_SEARCHES = "Recent Searches";
    public static final String FLIGHT_MOST_VISITED_CITIES = "Popular Cities";
    public static final String INTENT_EXTRA_ADULT_PASSENGER = "intent_extra_adult_passenger";
    public static final String INTENT_EXTRA_CHILDREN_PASSENGER = "intent_extra_children_passenger";
    public static final String INTENT_EXTRA_INFANT_PASSENGER = "intent_extra_infant_passenger";
    public static final String INTENT_EXTRA_FLIGHT_TRIP_TYPE = "intent_extra_trip_type";
    public static final String INTENT_EXTRA_FLIGHT_SCREENTYPE_TO_LAUNCH = "screen_type";
    public static final String INTENT_EXTRA_FLIGHT_SELLER = "intent_extra_trip_type";
    public static final String INTENT_EXTRA_FLIGHT_DOMESTIC_ONEWAY =
            "intent_extra_flight_domestic_oneway";
    public static final String INTENT_EXTRA_FLIGHT_DOMESTIC_ROUND_TRIP =
            "intent_extra_flight_domestic_roundtrip";
    public static final String INTENT_EXTRA_FLIGHT_OPERATOR_LIST =
            "intent_extra_flight_operator_list";
    public static final String FLIGHT_ONE_WAY_TRIP = "one_way";
    public static final String FLIGHT_ROUND_TRIP = "round_trip";
    public static final String FLIGHT_CITY_URL_TYPE = "flight_city_url_type";
    public static final String FLIGHT_SEARCH_PRICE_PATTERN = "###,###,###.##";
    public static final String FLIGHT_SEARCH_DISPLAY_TIME_FORMAT = "h:mm a";
    public static final String FLIGHT_SEARCH_INPUT_TIME_FORMAT = "HHmm";
    public static final Integer INTENT_MAXIMUM_NUMBER_OF_ADULTS = 9;
    public static final Integer INTENT_MINIMUM_NUMBER_OF_ADULTS = 1;
    public static final Integer INTENT_MAXIMUM_NUMBER_OF_CHILD = 8;
    public static final Integer INTENT_MAXIMUM_NUMBER_OF_PASSENGERS = 9;
    public static final String FLIGHT_HOME_PAGE_CAROUSEL_1 = "carousel";
    public static final double CAROUSEL_ASP_RATIO_FLIGHT_BANNER = 2;
    public static final String SELLAR_RATING_ITEM_ID = "item_id";
    public static final String SELLAR_RATING_MERCHANT_ID = "merchant_id";
    public static final String SELLAR_RATING_REVIEW = "review";
    public static final String SELLAR_S1 = "s1";
    public static final String SELLAR_S2 = "s2";
    public static final String SELLAR_S3 = "s3";
    public static final String INTENT_EXTRA_FLIGHT_RETURN_DATE = "intent_extra_flight_return_date";
    public static final String HOTEL_SEARCH_INPUT_DATA = "hotel_search_input_data";
    public static final String INTENT_EXTRA_FLIGHT_CLASS_TYPE = "intent_extra_flight_class_type";
    public static final String INTENT_EXTRA_FLIGHT_FARES_API = "intent_extra_flight_fares_api";
    public static final String KEY_FLIGHT_SEARCH_DESCENDING_ORDER = "reverse";
    public static final String KEY_FLIGHT_SEARCH_ASCENDING_ORDER = "forward";
    public static final String KEY_FLIGHT_SEARCH_SORT_BY_FARE = "price";
    public static final String KEY_FLIGHT_SEARCH_SORT_BY_DEPARTURE_TIME = "departure";
    public static final String KEY_FLIGHT_SEARCH_SORT_BY_DURATION = "duration";
    public static final String KEY_FLIGHT_SEARCH_SORT_BY_NONE = "key_flight_search_sort_by_none";
    public static final String INTENT_EXTRA_FLIGHT_SOURCE_CITY_CODE =
            "intent_extra_flight_source_city_code";
    public static final String INTENT_EXTRA_FLIGHT_DESTINATION_CITY_CODE =
            "intent_extra_flight_destination_city_code";
    public static final String FLIGHT_LIST_SEARCH_INPUT_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String HOTEL_LIST_SEARCH_INPUT_TIME_FORMAT = "yyyy-MM-dd'+'HH:mm";
    public static final String FLIGHT_RECENT_CITY_SEARCH_TYPE_SOURCE = "flight-recent-city-source";
    public static final String FLIGHT_RECENT_CITY_SEARCH_TYPE_DESTINATION =
            "flight-recent-city-destination";
    public static final String PREFIX_FLIGHT_ORDERS = "order_";
    public static final String OFFER_URL = "offers_url";
    public static final int MOVE_TO_WISHLIST_SIGNIN = 100;
    public static final String URL_TYPE_WISHLIST = "wishlist";
    public static final String URL_TYPE_BRAND_STORE = "brandstore";
    public static final String PREF_KEY_LAUNCH_APPSFLYER_DEEP_LINKING =
            "pref_key_launch_appsflyer_deep_linking";
    public static final String TAG_CAROUSEL_1_BANNER = "carousel_1_banner";
    public static final String QR_KEY_FLOW = "FLOW";
    public static final String QR_KEY_AMOUNT = "amount";
    public static final String QR_KEY_COMMENT = "comment";
    public static final String QR_KEY_EXPIRABLE = "expirable";
    public static final String QR_KEY_TXN_AMOUNT = "TXN_AMOUNT";
    public static final String QR_KEY = "1234567890123456";
    public final static int QR_KEY_WHITE = 0xFFFFFFFF;
    public final static int QR_KEY_BLACK = 0xFF000000;
    public final static int QR_KEY_BLUE = 0xFF012B72;
    public static final long QR_KEY_EXPIRE = 6 * 60 * 60 * 1000;
    public static final String QR_KEY_CODE_REQUEST_MONEY = "QR_CODE_REQUEST_MONEY";
    public static final Double WALLET_MONEY_TRANSACTION_LIMIT = 5000d;
    public static final String KEY_IS_FAST_FORWARD = "fast_forward";
    public static final String TEXT_INPUT = "text";
    public static final String TEXT_AND_NUMBER = "text_and_number";
    public static final String NUMBER = "number";
    public static final String RECHARGE_PRODUCT = "recharge_product";
    public static final int RECHARGE_VERTICAL_SIGN_REQ_CODE = 7;
    public static final int LOCATION_REQ_CODE = 500;
    public static final String DROP_DOWN = "dropdown";
    public static final String RADIO_GROUP = "radio";
    public static final String STORE_NAME = "store_name";
    public static final String STORE_DESCRIPTION = "store_description";
    public static final String INTENT_EXTRA_URL_TYPE = "intent_extra_url_type";
    public static final String URL_TYPE_PRIMARY_HOMEPAGE = "homepage_primary";
    public static final String SEARCH_URL_KEYWORD_RESOLUTION = "resolution=960x720";
    public static final String SEARCH_URL_KEYWORD_QUALITY = "&quality=high";
    public static final String SEARCH_URL_KEYWORD_CATEGORY = "&category=";
    public static final String SEARCH_URL_KEYWORD_BRAND = "&brand=";

    public static final String SEARCH_URL_KEYWORD_SITE_ID = "&site_id=";
    public static final String SEARCH_URL_KEYWORD_CHILD_SITE_ID = "&child_site_id=";


    public static final String SEARCH_URL_KEYWORD_MERCHANT = "&merchant=";
    public static final String CART_DETAILS = "cart";
    public static final String ORIGIN_HORIZONTAL_MENU = "horizontalMenu";
    public static final String URL_TYPE_HOTEL_CITY_SEARCH = "hotel-city-search";
    public static final String RETURN_ITEM_NAME = "Return_Item";
    public static final String REPLACE_ITEM_NAME = "Replace_Item";
    public static final String URL_TYPE_HOTEL_DETAILS = "hotel-details";
    public static final String ADWORKS_WEBVIEW_NAME = "Offer";
    public static final String TERMS_AND_CONDITION = "Terms & Condition";
    public static final String LIST_NAME = "list_name";
    public static final String LIST_POSITION = "list_position";
    public static final String SEARCH_TYPE = "search_type";
    public static final String SEARCH_CATEGORY = "search_category";
    public static final String SEARCH_TERM = "search_term";
    public static final String SEARCH_RESULT_TYPE = "search_result_type";
    public static final String LIST_ID_TYPE = "list_id_type";
    public static final String CONTAINER_ID = "container_id";
    public static final String PARENT_ID = "parent_id";
    public static final String LIST_NAME_DIRECT = "direct";
    public static final String KEY_REFERRER = "referrer";
    public static final String EXTRA_INTENT_LOAD_PAGE_DATAMODEL = "load_page_datamodel";
    public static final String EXTRA_INTENT_LOAD_BANNER_FROM_ORDERSUMMARY =
            "bundle_to_load_banner_from_order_summary";
    public static final String UTILITY_KEY_ATTRIBUTES = "attributes";
    public static final String UTILITY_KEY_INPUT_FIELDS = "input_fields";
    public static final String UTILITY_KEY_PRODUCT_ID = "product_id";
    public static final String UTILITY_KEY_CONFIGURATION = "configuration";
    public static final String UTILITY_KEY_AMOUNT = "amount";
    public static final String UTILITY_KEY_DESCRIPTION = "description";
    public static final String UTILITY_KEY_OPERATOR_IMAGE = "operator_image";
    public static final String UTILITY_KEY_VARIANTS = "variants";
    public static final String UTILITY_KEY_PRODUCTS = "products";
    public static final String UTILITY_KEY_ID = "id";
    public static final String UTILITY_KEY_GROUP_FIELD = "group_field";
    public static final String UTILITY_KEY_OPERATOR = "operator";
    public static final String UTILITY_KEY_INACTIVE = "INACTIVE";
    public static final String UITLITY_KEY_SOFTBLOCK = "SOFTBLOCK";
    public static final String ORDER_SUMMARY_SUBSCRIPTION_DATE_FORMAT = "dd MMM yyyy";
    public static final String ORDER_SUMMARY_SSO_TOKEN = "ssotoken";
    public static final String MODAL_REQUEST_IP_ADDRESS = "ipAddress";
    public static final String MODAL_REQUEST_PLATFORM_NAME = "platformName";
    public static final String MODAL_REQUEST_OPERATION_TYPE = "operationType";
    public static final String MODAL_REQUEST_METADATA = "metadata";
    public static final String MODAL_REQUEST_MERCHANT_GUID = "merchantGuid";
    public static final String MODAL_REQUEST_TXN_TYPE = "txnType";
    public static final String MODAL_REQUEST_MERCHANT_ORDER_ID = "merchantOrderId";
    public static final String APPSFLYER_CURRENCY = "INR";
    public static final String ORDER_SUMMARY_APPLICATION_PDF = "application/pdf";
    public static final String FEE_STRUCTURE_APPLICATION_OCTETSTREAM_PDF =
            "application/octetstream";
    public static final String FEE_STRUCTURE_APPLICATION_PDF = "application/pdf";
    public static final String ORDER_SUMMARY_DOCS_URL = "http://docs.google" +
            ".com/gview?embedded=true&url=";
    public static final String ORDER_SUMMARY_PAYMENT_DETAILS = "paymentDetails";
    public static final String ORDER_SUMMARY_STATUS_DELIVERED = "Delivered";
    public static final String ORDER_SUMMARY_TYPE_RADIO = "radio";
    public static final String ORDER_SUMMARY_TYPE_BUTTON = "button";
    public static final String ORDER_SUMMARY_TYPE_INVOICE = "Invoice";
    public static final String ORDER_SUMMARY_TYPE_BROWSER = "browser";
    public static final String ORDER_SUMMARY_STATUS_COLOR_RED = "red";
    public static final String ORDER_SUMMARY_STATUS_COLOR_GREY = "grey";
    public static final String ORDER_SUMMARY_STATUS_COLOR_GREEN = "green";
    public static final String ORDER_SUMMARY_STATUS_NULL = "null";
    public static final String ORDER_SUMMARY_IS_FROM_API = "apiResponse";
    public static final String ORDER_SUMMARY_IS_FROM_CACHE = "cache";
    public static final String ORDER_SUMMARY_STATUS_FLOW = "statusFlow";
    public static final String ORDER_SUMMARY_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String ORDER_SUMMARY_DEFAULT_API_DATE_FORMAT = "dd-MMM-yyyy hh:mm a";
    public static final String ORDER_SUMMARY_DEEP_LINKING = "Deep Linking";
    public static final String ORDER_SUMMARY_PAGE = "summary";
    public static final String ORDER_SUMMARY_P2P_TRANSFER = "P2P_TRANSFER";
    public static final String ORDER_SUMMARY_MODAL = "Modal";
    public static final String ORDER_SUMMARY_URL = "&actions=0";
    public static final String ORDER_HISTORY_URL = "&actions=1";
    public static final String ORDER_PG_SCREEN_URL = "&pg_screen=";
    public static final String ORDER_SUMMARY_CAROUSEL = "carousel";
    public static final String ORDER_SUMMARY_HOTEL_BANNER_DEEPLINK_URL = "paytmmp://";
    public static final String DEEPLINK_URL_UTILITY_SCHEME = "paytmmp://";
    public static final String DEEPLINK_APPEND_URL = "?url=";
    public static final String CLEAR_MOBILE_DATA = "CLEAR_MOBILE_DATA";
    public static final String EXTRA_INTENT_ORDER_NUMBER = "orderNumber";
    public static final String EXTRA_INTENT_ITEM_NAME = "itemName";
    public static final String EXTRA_INTENT_ORDER_DATE = "orderDate";
    public static final String EXTRA_INTENT_ORDER_ITEMS = "orderItems";
    public static final String EXTRA_INTENT_ITEM_COUNT = "itemCount";
    public static final String EXTRA_INTENT_PRODUCT_IMAGE = "productImage";
    public static final String EXTRA_INTENT_ALL_ITEM_NAME = "allItemName";
    public static final String EXTRA_INTENT_VERTICAL_NAME = "verticalName";
    public static final String EXTRA_INTENT_COMMON = "Common";
    public static final String EXTRA_INTENT_ORDER_VERTICAL = "orderVertical";
    public static final String EXTRA_INTENT_ITEM_STATUS = "itemStatus";
    public static final String EXTRA_INTENT_ORDER_STATUS = "orderStatus";
    public static final String EXTRA_INTENT_ORDER_LABEL = "orderLabel";
    public static final String ORDER_SUMMARY_STATUS_FLOW_DATE_FORMAT = "MMMM dd-yyyy  hh:mm a";
    public static final String ORDER_SUMMARY_BUS_TICKETS_DATE_FORMAT = "MMM dd,yy";
    public static final String ORDER_SUMMARY_BUS_TICKET_DATE_FORMAT = "dd MMM yy";
    public static final String ORDER_SUMMARY_COMMON_DATE_FORMAT = "dd-MMM-yy";
    public static final String ORDER_SUMMARY_TIME_ZONE_FORMAT = "UTC";
    public static final String YOUR_ORDERS_LISTTYPE_ORIGINAL = "originalList";
    public static final String YOUR_ORDERS_LISTTYPE_RECHARGE = "rechargeList";
    public static final String BRAND_STORE_KEY_ENTITY_ASSOCIATED_WITH = "/?cat_tree=1&brand=";
    public static final String BRAND_STORE_KEY_BRAND_ID = "?brand_id=";
    public static final String BRAND_STORE_LIST_TYPE = "Seller List";
    public static final String ENCODE_CHARSET_NAME = "UTF-8";
    public static final String BUS_USER_INFO_DETAILS = "bus_user_info_details";

    public static final String BUS_CURRENT_MONTH_DATE_FORMAT = "MMMM yyyy";
    public static final String EXTRA_INTENT_PASSENGER_NO = "passengerNo";
    public static final String SINGLE_PASSENGER_TYPE = "singlePassenger";
    public static final String MULTIPLE_PASSENGER_TYPE = "multiplePassengers";
    public static final String EXTRA_INTENT_DATE_OF_JOURNEY = "date_value";
    public static final String EXTRA_INTENT_BUS_TICKET_SOURCE = "Source";
    public static final String EXTRA_INTENT_BUS_TICKET_DESTINATION = "Destination";
    public static final String EXTRA_INTENT_CALENDER_DATE_TODAY = "today";
    public static final String EXTRA_INTENT_CALENDER_DATE_TOMORROW = "tomorrow";
    public static final String EXTRA_INTENT_SELECT_CITY_ACTIVITY_NAME = "activity_name";
    public static final String EXTRA_INTENT_BUS_JOURNEY_SELECTED_DATE = "selected_date";
    public static final String BUS_TICKET_TARGET_DATE_FORMAT = "yyyyMMdd";
    public static final String BUS_TICKET_CURRENT_DAY_OF_MONTH = "current_day_of_month";
    public static final String BUS_TICKET_DATE_PREVIOUS_TO_TODAY_IN_MONTH =
            "day_previous_to_today_in_month";
    public static final String BUS_TICKET_NORMAL_DAYS_IN_MONTH = "normal_days_in_month";
    public static final String BUS_TICKET_CALENDER_DATE_FORMAT = "dd-MMM-yyyy";
    public static final String BUS_TICKET_EMPTY_PASSENGER_DETAILS = "Passenger";
    public static final String INTENT_EXTRA_SERVER_TIME = "intent_extra_server_time";
    public static final String INDIAN_TIME_ZONE = "GMT+05:30";
    public static final String URL_TYPE_BUS_SEARCH = "bus-search";
    public static final String PUSH_SOURCE_CITY_SHORT_NAME = "source_city_short_name";
    public static final String PUSH_SOURCE_CITY_NAME = "source_city_name";
    public static final String PUSH_DESTINATION_CITY_SHORT_NAME = "destination_city_short_name";
    public static final String PUSH_DESTINATION_CITY_NAME = "destination_city_name";
    public static final String PUSH_DATE = "date";
    public static final String PUSH_PASSENGER_COUNT = "passenger_count";
    public static final String URL_TYPE_IN_MOBI = "InMobiGrid";
    public static final String INTENT_EXTRA_START_LOGIN = "intent_extra_start_login";
    public static final String PUSH_RECHARGE_NUMBER = "recharge_number";
    public static final String PUSH_RECHARGE_PRICE = "price";
    public static final String PUSH_RECHARGE_PROMO = "promo";
    public static final String INTENT_PROMO_CODE = "intent_promo_code ";
    public static final String RECCO_CATEGORY_ID = "?category_id=";
    public static final String EXTRA_INTENT_FROM_BUS_TICKET_RETRY = "from_bus_retry";
    //    public static final String LANGUAGE_ENGLISH = "en";
//    public static final String LANGUAGE_HINDI = "hi";
    public static final String KEY_UTILITY_META_DATA = "meta_data";
    //Wallet
    public static final String TAB_HEADING_ADD_MONEY = "Add";
    public static final String TAB_SEND_MONEY = "Send";
    public static final String TAB_REQUEST_MONEY = "Request";
    public static final String KEY_PENDING_PUSH = "pendingPush";
    public static final String KEY_PENDING_PUSH_COUNT = "pendingPushCount";
    public static final String KEY_SSO_TOKEN = "ssotoken";
    public static final String KEY_SSO_TOKEN_NEW = "sso_token";
    public static final String KEY_WALLET_TOKEN = "wallet_token";
    public static final String GENDER_MR = "Mr";
    public static final String GENDER_MS = "Ms";
    public static final String GENDER_MRS = "Mrs";
    public static final String GENDER_MASTER = "Mstr";
    public static final String GENDER_MALE = "male";
    public static final String GENDER_FEMALE = "female";
    public static final String URL_TYPE_MOVIETICKET = "movietickets";
    public static final String URL_TYPE_IMAXPAGE = "imaxpage";
    public static final String URL_TYPE_MOVIES_SHOW_TIME = "movies-show-time";
    public static final String PUSH_TYPE = "type";
    public static final String PUSH_CODE = "code";
    public static final String CINEMA_MOVIE_CODE = "cinema_movie_code";
    public static final String TRAVEL_VERTICALS_HOMESCREEN_DATE_FORMAT = "dd MMMM yy";
    public static final String TRAVEL_FLIGHT_HOMESCREEN_DATE_FORMAT = "E, dd MMM yy";
    public static final String TRAVEL_FLIGHT_SUMMARY_DATE_FORMAT = "dd MMM, E";
    public static final String HOTEL_SEARCH_CANCEL_DATE_FORMAT = "MMM dd";

    public static final String TRAVEL_FLIGHT_SUMMARY_DATE_FORMAT_HEADER = "MMMM dd, yyyy hh:mm aa";
    public static final String BUS_HOMESCREEN_DISPLAY_DATE_FORMAT_WITH_YEAR = "dd MMM yy, EEE";

    public static final String[] MONTHS_ARRAY = {"January", "February", "March", "April", "May",
            "June", "July", "August", "September", "October", "November", "December"};
    public static final String KEY_PUSH_LABEL = "label";
    public static final String KEY_PUSH_RELATED_CATEGORY = "related_category";
    public static final String KEY_MOVIE_TIME_SLOT_FLAG = "movie_time_slot_flag";
    public static final String MOVIE_TICKET_NOT_AVAILABLE = "Not Available";
    public static final String MOVIE_TICKET_FAST_FILLING = "Filling fast";
    public static final String MOVIE_TICKET_ALMOST_FILLED = "Almost filled";
    public static final String MOVIE_TICKET_AVAILABLE = "Available";
    public static final String INTENT_UTILITY_META_DATA = "meta_data";
    public static final String SUMMARY_PAGE = CJRConstants.ORDER_SUMMARY_PAGE;
    public static final String INTENT_EXTRA_LOAD_TARGET_PAGE_URL_TYPE =
            "intent_extra_load_target_page_url_type";
    public static final String FILTER_TYPE_LINEAR_REACTANGULAR_RADIO = "linear-rectangular-radio";
    public static final String FIRST_TRANSACTION_EVENT_SENT = "first_transaction_event_sent";

    public static final String INTENT_EXTRA_ADULT_COUNT = "adult_count";
    public static final String INTENT_EXTRA_CHILDRENS_COUNT = "childrens_count";
    public static final String INTENT_EXTRA_SENIOR_MALE_COUNT = "senior_men";
    public static final String INTENT_EXTRA_SENIOR_WOMEN_COUNT = "senior_women";
    public static final String INTENT_EXTRA_MENS_COUNT = "mens_count";
    public static final String INTENT_EXTRA_WOMENS_COUNT = "womens_count";
    public static final int REQUEST_CODE_TRAVELLER_DETAILS = 650;
    public static final String PASSENGER_DETAIL = "passenger_info";
    public static final int REQUEST_CODE_PASSENGER_DETAIL = 656;
    public static final String ADULT_TEXT = "Adult";
    public static final String ADULT_MALE_TEXT = "Adult (M)";
    public static final String ADULT_FEMALE_TEXT = "Adult (F)";
    public static final String INFANT_TEXT = "Infant";
    public static final String ADULTS_TEXT = "Adults";
    public static final String CHILDREN_TEXT = "Child";
    public static final String MEN_TEXT = "Sr.Men";
    public static final String WOMEN_TEXT = "Sr.Women";
    public static final String INTENT_EXTRA_PASSENGER_TYPE = "type";

    public static final String EXTRA_TRAIN_CLASS = "train_class_details";
    public static final String TRAIN_DETAIL = "train_detail";
    public static final String TRAIN_DETAIL_BODY = "train_detail_body";
    public static final String SOURCE_EXTRA_INTENT = "source";
    public static final String DESTINATION_EXTRA_INTENT = "destination";
    public static final String FROM_TRAIN_MODULE = "from_train_module";
    public static final String IRCTC_REDIRECT = "irctc_redirect";
    public static final String IRCTC_LOGIN_CALL_BACK = "irctc/notification";
    public static final String IRCTC_CANCEL_CALL_BACK = "irctc/notification?cancelbutton=y";


    public static final String EXTRA_TRAIN_REQUEST_ID = "request_id";
    public static final String TRAIN_META_DESCRIPTION = "meta_description";
    public static final String EXTRA_TRAIN_CLASS_CODE = "train_class_code";
    public static final String INTENT_EXTRA_INFANT_COUNT = "infant_count";
    public static final String INTENT_EXTRA_MALE_ADULT_COUNT = "male_adult_count";
    public static final String INTENT_EXTRA_FEMALE_ADULT_COUNT = "female_adult_count";
    public static final String INTENT_EXTRA_PASSENGER_TITTLE = "passenger_tittle";
    public static final String INTENT_EXTRA_CATEGORY_NAME = "category_name";

    public static final String URL_TYPE_TRAIN = "trainticket";
    public static final String KEY_TRAIN_SOURCE = "source";
    public static final String KEY_TRAIN_SOURCE_NAME = "source_name";
    public static final String KEY_TRAIN_DESTINATION = "destination";
    public static final String KEY_TRAIN_DESTINATION_NAME = "destination_name";
    public static final String KEY_TRAIN_DEPARTURE_DATE = "departureDate";
    public static final String KEY_TRAIN_IS_ROUND_TRIP = "is_round_trip";

    public static final String KEY_TRAIN_SEARCH_DESCENDING_ORDER = "reverse";
    public static final String KEY_TRAIN_SEARCH_ASCENDING_ORDER = "forward";
    public static final String KEY_TRAIN_SEARCH_SORT_BY_DEPARTURE_TIME = "departure";
    public static final String KEY_TRAIN_SEARCH_SORT_BY_ARRIVAL_TIME = "arrival";
    public static final String KEY_TRAIN_SEARCH_SORT_BY_DURATION = "duration";
    public static final String KEY_TRAIN_SEARCH_SORT_BY_AVAILIBILITY = "availibility";
    public static final String KEY_TRAIN_SEARCH_SORT_BY_NONE = "key_flight_search_sort_by_none";
    public static final String TRAIN_SEARCH_INPUT_DATE_FORMAT = "yyyy-MM-dd";
    public static final String TRAIN_SEARCH_DISPLAY_DATE_FORMAT = "EEE, dd MMM yy";
    public static final String TRAIN_AVALBLTY_DATE_FORMAT = "dd MMM yy";
    public static final String TRAIN_AVALBLTY_DATE_FORMAT_SELECTED_FLIGHT = "dd MMM, EEE";
    public static final String INTENT_EXTRA_TRAIN_SEARCH_LOAD_DATA =
            "intent_extra_TRAIN_search_load_data";
    public static final String INTENT_EXTRA_TRAIN_SEARCH_INPUT = "intent_extra_train_search_input";
    public static final String EXTRA_INTENT_SELECTED_TRAIN = "extra_selected_train_detail";
    public static final String EXTRA_INTENT_HOLIDAY_LIST = "extra_intent_holiday_list";
    public static final String EXTRA_INTENT_ALREADY_FILTER_APPLIED = "alreadyFilterApplied";

    public static final String INTENT_EXTRA_PNR_STATUS_DETAILS = "intent_extra_pnr_status_details";
    public static final String PUSH_FLIGHT_TRIP_TYPE = "flight_trip_type";
    public static final String URL_TYPE_NO_LINK = "nolink";
    public static final String PUSH_FLIGHT_CLASS = "flight_class";
    public static final String PUSH_FLIGHT_DEPART_DATE = "flight_depart_date";
    public static final String PUSH_FLIGHT_RETURN_DATE = "flight_return_date";
    public static final String CANCEL_BUTTON_ORDER_SUMMARY = "CANCEL";
    public static final String TRAIN_EXTRA_QUOTA_CODE = "quota_code";
    public static final String EXTRA_INTENT_TRAIN_NUMBER = "train_number";
    public static final String EXTRA_INTENT_DEPARTURE_DATE = "departure_date";
    public static final String EXTRA_INTENT_SOURCE_STATION = "source_station";
    public static final String EXTRA_INTENT_FLAG = "flag";
    public static final int REQUEST_CODE_SIGN_IN = 4;
    public static final String EXTRA_TRAIN_QUOTA = "quota";
    public static final String SIGN_UP_SUCCESS = "login_success";
    public static final String IRCTC_REGISTERED_USER_ID = "registered_user_id";
    public static final String IRCTC_REGISTERED_MOBILE_NUMBER = "registered_irctc_mobile_number";
    public static final String IRCTC_REGISTERED_EMAIL_ID = "registered_irctc_email_id";
    public static final String TRAIN_EXTRA_QUOTA_NAME = "quota_name";
    public static final String INTENT_EXTRA_INFANTS_COUNT = "infants";
    public static final String INTENT_OFFER_LIST = "offers";
    public static final String INTEN_TRAIN_DESTINATION_CITY = "destination_city";
    public static final String PASSBOOK_OFFLINE = "passbook_offline";
    public static final String PASSBOOK_SUBWALLET_OFFLINE = "passbook_subwallet_offline";
    public static final int DYNAMIC_QR_LENGTH = 24;
    public static final String TXN_MSG = "txnMessage";
    public static final String FLIGHTS_FARE_PROVIDER_DATA = "flights_fare_provider_data";
    public static final int VER_2_LENGTH = 24;
    public static final String MOBILE_EMAIL_FLAG = "mobile_email";
    public static final String VERIFY_MOBILE_EMAIL = "verify_mobile_email";
    public static final int REQUEST_CODE_EMAIL_MOBILE = 211;
    public static final int SEAT_FOOD_AND_BEVERGES = 568;
    public static final String INTENT_BOOLEAN_EXTRA_ISPROMO_APPLIED =
            "intent_boolean_extra_ispromo_applied";
    public static final int INTENT_PASSENGER_DETAIL_ACTIVITY_START = 989;
    public static final String INTENT_EXTRA_PASSWORD_CLICKED = "password_reset_clicked";
    public static final int INTENT_PASSWORD_RESET = 198;
    public static final String EXTRA_INTENT_STARTED_FROM_RECHARGE =
            "started_activity_from_recharge";
    public static final int REQUEST_CODE_CAMERA_UPLOAD = 212;
    public static final int REQUEST_CODE_PIC_UPLOAD = 213;
    public static final int REQUEST_CODE_IMAGE_CROP = 214;
    public static final String KEY_PAN_NUMBER = "pan_number";
    public static final String KEY_PAN_NUMBER_TIME_STAMP = "pan_card_time_stamp";
    public static final String INTENT_EXTRA_HOTEL_REVIEWS = "intent_extra_hotel_reviews";
    public static final String VERIFICATION_TYPE = "verification_type";
    public static final String ADD_MONEY_EXTRA_PARAMETER = "add_money_extra_item";
    public static final int REQUEST_CODE_PROMO_ADD_MONEY = 215;
    public static final String CARD_NUMBER = "card_number";
    public static final String CARD_TYPE = "card_type";
    public static final String CST_NEAR_BY_VERTICAL = "nearby";
    public static final String CUSTOMER_ID = "Customer_Id__c";
    public static final String TERMINAL_ID = "Terminal_Id__c";
    public static final String RESOURCE_ID = "resId";
    public static final String EXTRA_OPENED_SAVING_ACCOUNT = "extraOpenedSavingAccount";
    public static final String ACCOUNT_TYPE = "account_type";
    public static final String ACCOUNT_SUBTYPE = "account_subtype";
    public static final String DOB = "dob";
    public static final String KEY_PINCODE = "pin_code";
    public static final String NO_NOMINEE = "no_nominee";
    public static final String NOMINEE_ID = "nominee_id";
    public static final String RELATIONSHIP = "relationship";
    public static final String USER_MOBILE = "user_mobile";
    public static final String PROMO_VIEW = "promoView";
    public static final String PROMO_CLICK = "promoClick";

    public static final String EXTRA_INTENT_NEW_ADDRESS_ID = "kyc_new_address_ID";
    public static final String PUSH_QRID = "qrid";
    public static final String INITIATIVE_SERVICE = "noble";
    public static final String VIA_QR = "via_qr";
    public static final int TRANSACTION_HISTORY_COUNT = 10;
    public static final String SESSION_TOKEN = "session_token";
    public static final String USER_ID = "userId";
    public static final String RESOURCE_OWNER_ID = "resource_owner_id";
    public static final String IS_MERCHANT = "is_merchant";
    public static final String PASSBOOK_FRAGMENT_POSITION = "passbook fragment position";
    public static final Integer RECEIVED = 2;
    public static final String SD_MERCHANT_TXT = "SELF_DECLARED_MERCHANT";
    public static final String IS_ACCEPT_PAYMENT_FLOW = "accept_payment";
    public static final String KEY_BANK_CUSTOMER_STATUS = "bank_customer_status";
    public static final int REQUEST_CODE_PICK_IMAGE = 3112;
    public static final int REQUEST_CODE_P2P = 3113;
    public static final int REQUEST_CODE_P2M = 3114;
    public static final String ADD_MONEY_AMOUNT = "add_money_amount";
    public static final String KEY_ROOT_STATUS = "uninst_lib_root_status";
    public static final String EXTRA_INTENT_LAUNCH_SHOW_DIL = "extra_intent_launch_show_dil";
    public static final String SHOW_CODE_QR = "2810050501011O7KV4V6FDGU";
    public static final String INDEPENDANCE_DAY_QR = "281005050101120VOZL5CUS6";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String RECHARGE_DEALS_FAST_FORWARD = "dealsFastForward";
    public static final String SCAN_DEEP_LINKING_URL = "paytmmp://cash_wallet?featuretype=scanner";
    public static final String FROM_SHORTCUT = "from_shortcut";
    public static final String DEEPLINK = "deeplink";
    public static final String API = "api";
    public static final String URL_TYPE_UPDATES_CUSTOM_VIEW = "updates_custom_view";
    public static final String URL_TYPE_UPDATES_FLIP_VIEW = "updates_flip_view";
    public static final String URL_TYPE_UPDATES_STORY_VIEW = "updates_stories_view";
    public static final String URL_TYPE_UPDATES_PAGER_VIEW = "updates_pager_view";
    public static final String URL_TYPE_UPDATES_STORY_BLOG_VIEW = "updates_story_blog_view";
    public static final String URL_TYPE_UPDATES_HOROSCOPE_DETAIL_VIEW = "update_horoscope_detail_view";
    public static final String URL_TYPE_UPDATES_VIDEO_DETAIL_VIEW = "update_video_detail";
    public static final String URL_TYPE_UPDATES_SHOW_COMMENTS = "updates_comments";
    public static final String URL_TYPE_DMRC = "DMRC";
    public static final String URL_TYPE_SCAN = "scan";
    public static final String KEY_PRODUCT_TYPE = "product-type";

    public static final String KEY_IS_SCAN_ONLY = "scan_only";
    public static final String FLIGHT_FARE_API_CALLED = "flight-fare-api-called";
    public static final String EXTRA_ORDERED_ITEM = "ordered_item";

    public static final String KYC_BOTH = "both";
    public static final String KYC_AADHAR = "aadhar";
    public static final String KYC_PAN = "pan";
    public static final String KYC_EDITABLE_EXCLAMATED = "EDITABLE_EXCLAMATED";
    public static final String KYC_NON_EDITABLE_TICKED = "NON_EDITABLE_TICKED";
    public static final String KYC_NON_EDITABLE_WAITING = "NON_EDITABLE_WAITING";
    public static final String PAN_ADHAR_BOTH_UPDATE = "update_both_pan_adhar";
    public static final String SAVED_CARD = "saved_card";
    public static final String AMOUNT_AUTO_ADD = "amount_auto_add";
    public static final String IS_AUTO_ADD_EDITMODE = "isEditMode";
    public static final String SAVED_CARDS = "saved_card";
    public static final String AUTO_ADD_STATUS_FAILURE = "auto_add_status";
    public static final String AUTO_ADD_MESSAGE = "auto_add_message";
    public static final String ACTION_STORY_INDEX_CHANGED = "story_index_changed";
    public static final String USER_SOCIAL_PIC_PARENT_PATH = "/Android/data/net.one97.paytm/Social";
    public static final String EXTRA_FILE_PATH = "extra_file_path";
    public static final String EXTRA_CARDS_COUNT = "extra_cards_count";
    public static final String DOC_SELF_NOT_UPLOADED = "is_doc_not_uploaded";
    public static final String DOC_SELF_VERIFIED = "is_doc_verified";
    public static final String KEY_DISPLAY_NAME = "display_name";
    public static final String KEY_IS_ADHAR_VERIFIED = "is_adhar_verified";
    public static final String KEY_IS_PAN_VERIFIED = "is_pan_verified";
    public static final String KEY_KYC_FIRST_NAME = "kyc_first_name";
    public static final String KEY_KYC_LAST_NAME = "kyc_last_name";
    public static final String KEY_KYC_GENDER = "kyc_gender";
    public static final String KEY_KYC_DOB = "kyc_dob";
    public static final String ADD_MONEY_SUCCESS_AMOUNT = "add_money_success_amount";
    public static final String AMOUNT = "amount";
    public static final String CARD_ID = "card_id";

    public static final String FROM_PAYMENT = "is_from_pg_page";
    public static final String ORDER_SUMMARY_CANCELLATION = "order-summary-cancellation";

    public static final String ORDER_SUMMARY_SUCCESS = "order_summary_post";
    public static final String P2B_BENEFICIARY = "p2b_beneficiary";
    public static final String P2B_HASHMAP = "p2b_hashmap";
    public static final String IMPS_PASSCODE_FLOW = "imps_passcode_flow";
    public static final String IMPS_PASSCODE_DATA = "imps_data";
    public static final int REQUEST_CODE_PASSCODE_BANK = 3333;
    public static final String CHANNEL_ID = "channel_id";
    public static final String KEY_SHOW_INSURANCE = "key_show_insurance";
    public static final String KEY_INSURANCE_URL = "insurance_url";
    public static final String SITE_IDENTIFIER = "site_identifier";
    public static final String DATE_OF_BIRTH = "date_of_birth";
    public static final String CUST_ID = "cust_id";
    public static final String DEVICE_DPI = "device_dpi";
    public static final String EXTRA_KEY_IS_FROM_PAYMENT_BANK = "is_from_passbook";
    public static final String ISSUED = "ISSUED";
    public static final String CLOSED = "CLOSED";
    public static final String FLIGHT_IMAGE_DEFAULT_URL = "http://assets.paytm.com/travel/flights/iata/v1/order_actions/small/DEFAULT.png";
    public static final String INSURANCE = "Insurance";
    public static final String POWER_PLAY_CLIENTID = "one97-power-play";

    public static final String INTENT_MASTER_NUMBER = "intent_master_no";
    public static final String INTENT_MASTER_PRICE = "intent_master_price";
    public static final String INTENT_MASTER_PRODUCT_ID = "intent_master_product_id";
    public static final String MIN_AMOUNT = "min_amount";
    public static final String AUTO_AMOUNT = "auto_amount";
    public static final String TOP_UP_AMOUNT = "top_up_amount";
    public static final String TRACKING_URL = "TRACKING_URL";
    public static final String TOKEN_TYPE = "token_type";
    public static final String SUBSCRIPTION_TYPE_USER = "USER";
    public static final String AUTOMATIC_SUBSCRIPTION_RESPONSE = "auto_subscribe_response";
    public static final String POSTCARD_DATE_FORMAT = "yyyy-MM-dd";
    public static final String POSTCARD_EXPIRY_DATE_FORMAT_SRC = "yyyy-MM-dd";
    public static final String POSTCARD_EXPIRY_DATE_FORMAT_TARGET = "dd-MMM";
    public static final String POSTCARD_VIEW_EXPIRY_DATE_FORMAT_TARGET = "dd-MMM-yyyy";
    public static final String POSTCARD_CREATE_DATE_FORMAT_SRC = "yyyy-MM-dd hh:mm:ss";
    public static final String POSTCARD_ACTIVATED = "ACTIVATED";
    public static final String POSTCARD_CLOSED = "CLOSED";
    public static final String POSTCARD_EXPIRED = "EXPIRED";
    public static final String POSTCARD_REQUEST_PENDING = "REQUEST_PENDING";
    public static final String POSTCARD_SUCCESS = "SUCCESS";
    public static final int MY_POSTCARD_REQUEST_CODE = 1010;
    public static final String CE_0000 = "CM_0000";
    public static final String POSTCARD_RECIPIENT_OBJ = "recipientListObj";
    public static final String FETCH_SUBS_STATUS = "fetch_status";
    public static final int EDIT_SUBSCRIPTION = 123;
    public static final int RESULT_CODE_AUTO_ADD = 532;
    public static final String AUTOMATIC_FREQUENCY_UNIT = "frequencyUnit";
    public static final String AUTOMATIC_RETRY_COUNT = "retryCount";
    public static final String AUTOMATIC_ENABLE_RETRY = "enableRetry";
    public static final String DAY = "DAY";
    public static final String PPI = "PPI";
    public static final String MAX_DATE_AUTO_SUBS = "2019-1-1";
    public static final int BIN_NUM_LENGTH_6_DIGIT = 6;
    public static final int MASK_CREDIT_LENGTH_4_DIGIT = 4;
    public static final String MASK_STRING_20X = "XXXXXXXXXXXXXXXXXXXX";
    public static final String VERTICAL_ID_INSURANCE = "vertical_id";
    public static final String MERCHANT_ID_INSURANCE = "merchant_id";
    public static final String OPERATOR_NAME_INSURANCE = "operator";
    public static final String PAYTYPE_KEY_INSURANCE = "paytype";
    public static final String SERVICE_INSURANCE = "service";
    public static final String BRAND_INSURANCE = "brand";
    public static final String KEY_POSTCARD_DL = "postcard_dl";
    public static final String KEY_POSTCARD_BENEFICIARY_NAME = "postcard_beneficiary_name";
    public static final String KEY_POSTCARD_BENEFICIARY_ACCOUNT = "postcard_beneficiary_account";
    public static final String KEY_POSTCARD_TYPE = "postcard_type";
    public static final String KEY_POSTCARD_BENEFICIARY_AMOUNT = "postcard_beneficiary_amount";
    public static final int REQUEST_CODE_THANKS = 1007;
    public static final String THEME_DATA = "themes_data";
    public static final String KEY_SHOULD_POSTCARD_FETCHED = "should_fetch_on_launch";
    public static final String THEMEID_THANKS = "d594f202-7852-11e7-83c6-1866da8588af";
    public static final String WEB_LOGIN_SUB_STR = "90";
    public static final String RECENTS_DATA = "recents_data";

    public static final String ATTACHMENT_TYPE_IMAGE = "image";
    public static final String ATTACHMENT_TYPE_PDF = "pdf";
    public static final String KEY_CONTACT_US_DEVICE_ID = "deviceId";
    public static final String KEY_PENDING = "Pending";
    public static final String SERVER_ERROR_500 = "500";

    public static final String BUS_VERTICAL_LABEL = "bus";
    public static final String INTENT_EXTRA_CST_ORDER_ID = "cst-order-id";


    public static String ACTION_UPDATE_CHANNEL = "com.urbanairship.push.sample" +
            ".ACTION_UPDATE_CHANNEL";
    public static String SELECTED_LOCATION = "selected_location";
    public static String IS_FROM_UTIL_SCREEN = "is_from_vertical";
    public static String IS_FROM_CART = "is_from_cart";
    public static String IS_ADDRESS_LOADED = "is_address_loaded";
    public static String HOTEL_LIST_TYPE_HEADER = "hotel_list_type_header";
    public static String URL_TYPE_HOTEL_RESULTS = "hotel-results";
    public static String INTENT_EXTRA_HEADER = "intent_extra_header";
    public static String LOCATION_LIST = "location_list";
    public static int NUMBER_OF_BOX_IN_TEXT_LIST = 2;
    public static String FACEBOOK_PROFILE_URL = "https://www.facebook.com/Paytm";//https://www
    // .facebook.com/paytm"
    public static String TWITTER_PROFILE_URL = "https://twitter.com/paytm";//https://twitter
    // .com/paytm
    public static String INSTAGRAM_PROFILE_URL = "https://www.instagram.com/paytm/";
    public static String FACEBOOK_PACKAGE_NAME = "com.facebook.katana";
    public static String TWITTER_PACKAGE_NAME = "com.twitter.android";
    public static String INSTAGRAM_PACKAGE_NAME = "com.instagram.android";
    public static String FACEBOOK_PROFILE_SCHEMA = "fb://page/137725316293048";
    //fb://page/{id}//fb://profile/paytm
    public static String TWITTER_PROFILE_SCHEMA = "twitter://user?user_id=144792607";
    public static String SELL_ON_PAYTM_LINK = "https://seller.paytm.com";
    public static String PARTNER_WITH_PAYTM_LINK = "https://paytm.com/partner";
    public static String OUR_BLOG_PAYTM_LINK = "https://blog.paytm.com/";
    public static String PAYTM_COMMUNITY_FORUMS_LINK = "http://discuss.paytm.com/";
    //"twitter://user?user_id=USERID"//twitter://user?user_id=144792607
    public static String INSTAGRAM_PROFILE_SCHEMA = "instagram://user?username=paytm";
    //instagram://user?username=paytm

    public static boolean isLogged = false;

    public static String UTILITY = "utility";

    public static String MCONTAINER_INSTANCE_ID = "mcontainer_instance_id";
    //deeplinking constant for send to bank

    public static String DEEPLINK_BANK_ACC_NO = "recipient";
    public static String DEEPLINK_BANK_USER_NAME = "name";
    public static String DEEPLINK_BANK_IFSC = "ifsc";
    public static String DEEPLINK_BANK_AMOUNT = "amount";
    public static String DEEPLINK_BANK_SEND_COMMENT = "comment";


    // send money deep link url for paysendactivity

    public static String DEEPLINK_P2P_NUMBER = "recipient";
    public static String DEEPLINK_P2P_AMOUNT = "amount";
    public static String DEEPLINK_P2P_SEND_COMMENT = "comment";

    //kyc upgrade wallet ******************************
    public static String KYC_MODE = "mode";
    public static String KYC_LANDING_PAGE = "landing_page";
    public static String KYC_USER_NAME = "name";
    public static String KYC_USER_POI_NUMBER = "aadhar_number";
    public static String KYC_SAVED_DATA_STAGE_INFIELD = "in-field";
    public static String KYC_SAVED_DATA_STAGE_ASSIGNED = "assigned";
    public static String KYC_SAVED_DATA_STAGE_NEW = "new";

    public static String SHOW_CODE_OVERLAY = "overlay";

    //********** names for pay send fragment tabs****************

    public static String TAB_SEND_TO_BANK = "Send to Bank";
    public static String TAB_SCAN_CODE = "Scan Code";
    public static String TAB_MOBILE_NO = "Mobile No.";
    public static String TAB_SHOW_CODE = "Show Code";
    public static String TAB_PAYMENT_REQUEST = "Payment Request";
    public static final String URL_TYPE_EVENTS = "events";
    public static final String URL_TYPE_GOLD_PASSBOOK = "gold-passbook";
    public static final String URL_TYPE_GOLD = "digital-gold";
    public static final String URL_TYPE_DIGITAL_GOLD = "gold";
    public static final String GOLD_CALENDER_DATE_FORMAT = "ddMMyy";
    public static final String URL_TYPE_MUTUAL_FUND = "mutualfunds";
    public static final String URL_TYPE_SIGNIN_PROFILE = "signin_profile";
    public static final String URL_TYPE_AMUSEMENT_PARK = "amusement";
    public static final String URL_TYPE_AMPARK_DL = "amusement";
    public static final String PUSH_FEATURE_TYPE_AMUSEMENT_PARK = "ampark_detail";
    public static boolean isDontKeepActivitiesWarningShown = false;
    public static final String TRANSACTION_ID = "transaction_id";
    public static final String IRCTC_USER_ID = "irctc_user_id";

    public static String INTENT_EXTRA_FROM_REVIEW_ITERNARY = "intent_extra_from_review_iternary";
    public static String INTENT_EXTRA_HOTEL_REVIEW_PUBLISHER =
            "intent_extra_hotel_review_publisher";
    public static String INTERNATIONAL_FLIGHTS = "international";
    public static String YOUTUBE_DEV_KEY = "AIzaSyDop_x0_q8qWsBK-YQ9sBXGVdyXzof706E";
    public static String INTENT_STRING_EXTRA_YOUTUBE_VIDEO_ID =
            "intent_string_extra_youtube_video_id";
    public static String INTENT_PUT_SERIALIZABLE_EXTRA_MOVIE_SELECTED =
            "intent_put_serializable_extra_movie_selected";
    public static String P2BStatus = "p2b_status";
    public static String BANK_LIST_OFFLINE = "bank_list_offline";
    public static String BANK_LIST_OFFLINE_TIME = "bank_list_offline_time";
    public static String CONTACT_NAME_SELECTED = "selected_contact_name";
    public static String CONTACT_NUMBER_SELECTED = "selected_contact_number";

    public static String TNC_List = "tnCList";
    public static final String BANK_CUSTOMER = "BANK_CUSTOMER";
    public static final String BANK_LEAD = "BANK_LEAD";
    public static int POSTCARD_TYPE_THANKS = 1901;


    public static void resetAppVariables() {
        isDontKeepActivitiesWarningShown = false;
    }

    public static String EXPERIMENT_ID = "";

    public static void setExperimentId(String experimentId) {
        EXPERIMENT_ID = experimentId;
    }

    //Train
    public static final String PUSH_TRAIN_SOURCE_CITY = "source";
    public static final String PUSH_TRAIN_DESTINATION_CITY = "destination";
    public static final String PUSH_TRAIN_DEPARTURE_DATE = "departureDate";
    public static final String PUSH_TRAIN_SOURCE_CITY_NAME = "source_name";
    public static final String PUSH_TRAIN_DESTINATION_CITY_NAME = "destination_name";
    public static final String PUSH_TRAIN_IS_ROUND_TRIP = "is_round_trip";
    public static final String BOOLEAN_EXTRA_IS_CONTOL_FROM_DEEP_LINK =
            "boolean_extra_is_contol_from_deep_link";
    public static final String INTENT_EXTRA_SELECTED_DATE_WITH_YEAR =
            "intent_extra_selected_journey_date";
    public static final String INTENT_EXTRA_SELECTED_DATE_TIME = "intent_extra_selected_date_time";
    public static final String INTENT_EXTRA_NOTIFICATION = "intent_extra_notification";
    public static final String EXTRA_RECNT_BOOKING_ITEMS = "extra_recnt_booking_items";

    public static final String INTENT_EXTRA_CST_ORDER_ITEM = "intent_extra_cst_order_item";
    public static final String INTENT_EXTRA_CST_PARENT_REASON_L0 = "intent_extra_cst_parent_reason_l0";
    public static final String INTENT_EXTRA_CST_ORDER_REASONS = "intent_extra_cst_order_reasons";
    public static final String INTENT_EXTRA_CST_ORDER_REASONS_L3 =
            "intent_extra_cst_order_reasons_l3";
    public static final String INTENT_EXTRA_CST_ORDER_LIST_TABS =
            "intent_extra_cst_order_list_tabs";
    public static final String INTENT_EXTRA_CST_QUERY_LEVEL = "intent_extra_cst_query_level";
    public static final String INTENT_EXTRA_CST_CONTACT_TYPE = "intent_extra_cst_contact_type";
    public static final String INTENT_EXTRA_CST_FRAG_POSITION = "intent_extra_cst_frag_position";
    public static final String INTENT_EXTRA_GOLD_PAGINATION = "intent_extra_gold_pagination";

    public static final String CST_SECOND_QUERY_LEVEL = "cst_second_query_level";
    public static final String UTM_SOURCE = "utmsource";
    public static final String CST_CONTACT_US = "ContactUs";

    public static int INTENT_IRCTCT_SIGN_UP_SUCCESS = 10;
    public static final String TRAIN_SUMMARY_PASSENGER_NAME = "passenger_name";

    public static String SENDER_NAME = "senderName";
    public static String MSG_BODY = "msgbody";
    public static String SMS_SENT = "smsSent";
    public static String SMS_SEND_TIME = "smsSentTime";
    public static String SEND_MESSAGE = "sendMessage";
    public static String TOKEN_ID_SMS = "token_id_sms";
    public static String CUSTOMER_ID_SMS = "customer_id_sms";
    public static final String MOVIE_ZOOM_TOASTMSG_DISPLAYED = "movie_zoom_toast_msg_dispalyed";

    public static final String SYSTEM_TXN_ID = "systemTxnId";
    public static final String PLATFORM_NAME = "platformName";
    public static final String IPADDRESS = "ipAddress";
    public static final String CHANNEL = "channel";
    public static final String VERSION = "version";
    public static final String OPERATION_TYPE = "operationType";

    public static String EXPIRY_TIME_BUS = "expiryBrts";


    public static final String VERTICAL_PAYMENT_NAME = "payment";
    public static final String BUS_TICKET_FOOTER_DATE_FORMAT = "dd MMMM yyyy";
    public static final String STATUS_BOOKED = "BOOKED";
    public static final String STATUS_CANCELLED = "CANCELLED";
    public static final String ALARM_REGISTERED = "alarmRegiserNew";
    public static final String LANGUAGE_SELECTED = "languageSelected";

    public static final String PUSH_EVENTS_NAME = "events_name";
    public static final String PUSH_EVENTS_ID = "events_id";
    public static final String MOVIE_CAST_DEATILS_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String MOVIE_CAST_DETAILS_DATE_OUTPUT_FORMAT = "dd MMM, yyyy";

    public static final String PUSH_EVENTS_PROVIDER_ID = "provider_id";
    public static final String PUSH_EVENTS_CITY_NAME = "city_name";
    public static final String PUSH_EVENTS_CATEGORY_NAME = "category_name";
    public static final String PUSH_EVENTS_SUB_CATEGORY_NAME = "sub_category_name";
    public static final String EXTRA_FILTERS = "extra_filters";
    public static final String EXTRA_VIDEO_ID = "extra_video_id";
    public static final String CONTACT_US_OBJECT = "contact_us_object";
    public static final String CONTACT_US_OBJECT_2 = "contact_us_object_2";

    public static final String LOGO_URL = "logo_url";
    public static final String BADGE_URL = "badge_url";
    public static final String BANNER_URL = "banner_url";
    public static final String DISPLAY_TITLE = "display_title";
    public static final String EXTRA_GALLARY_IMAGE = "gallary_image";


    public static final String SITE_ID = "site_id";
    public static final String CHILD_SITE_ID = "child_site_id";

    //amusement park
    public static final String PUSH_PARK_NAME = "park_name";
    public static final String PUSH_PARK_ID = "park_id";
    public static final String PUSH_PARK_PROVIDER_ID = "provider_id";
    public static final String PUSH_PARK_CITY_NAME = "city_name";
    public static final String PUSH_PARK_CATEGORY_NAME = "category_name";
    public static final String PUSH_PARK_CITY_LABEL = "cityLabel";
    public static final String PUSH_PARK_CITY_VALUE = "cityValue";
    //Gift cardList
    public static final String GIFT_CARD_URL = "url";

    public static final String ACQUISITION_SOURCE = "acquisition_source";
    public static final String ACQUISITION_MEDIUM = "acquisition_medium";
    public static final String ACQUISITION_CAMPAIGN = "acquisition_campaign";
    public static final String ACQUISITION_GCLID = "acquisition_gclid";

    public static final String SEARCH_AB_VALUE = "search_ab_value";

    public static final String ACQUISITION_CONTENT = "acquisition_content";

    public static final String REPLACEMENT_PARENT_ORDER = "replacement_parent_id";
    public static final String IS_CHILD_ORDER = "is_child_order";

    public static final String REPLACEMENT_ORDER_BUTTON = "replacement_order_button";
    public static final String REPLACEMENT_BUTTON_TEXT = "View replacement order";

    public static final String INTENT_EXTRA_TRAIN_SUMMARY = "intent_extra_train_summary";
    public static final String INTENT_EXTRA_TRAIN_CAROUSEL = "intent_extra_train_carousel";
    public static final String ORDER_SUMMARY_TYPE_CANCEL_ITEM = "Cancel_Item";

    public static final String INTENT_EXTRA_ENTRY_POINT = "intent_extra_entry_point";
    public static final String INTENT_EXTRA_PG_SCREEN_URL = "intent_extra_pg_screen_url";
    public static final String INTENT_EXTRA_IS_TRAIN_TICKET = "is_train_ticket";
    public static final String INTENT_EXTRA_IS_FLIGHT_TICKET = "is_flight_ticket";

    public static final String[] visaTyps = {"Tourist", "Business", "Student", "Dependent",
            "Resident", "Migrant"};
    public static final String mLogo = "https://s3-us-west-2.amazonaws" +
            ".com/paytm-travel/travel_db/flights/app+icons/paytmBadge.png";
    public static final String urlBaggage = "https://s3-us-west-2.amazonaws" +
            ".com/paytm-travel/travel_db/flights/app+icons/noBaggage.png";

    public static final String INTENT_EXTRA_TRAIN_CAROUSEL_DATA =
            "intent_extra_train_carousel_data";
    public static final String TRAIN_STATUS_PENDING = "PENDING";
    public static final String INTENT_EXTRA_IS_MOVIE_TICKET = "is_movie_ticket";
    public static final String KEY_PROFILE_IMAGE = "profilePic";
    public static final String EXTRA_ADDRESS_FROM_ACCOUNT = "address_from_account";
    public static final String EXTRA_SAVED_CARD_FROM_ACCOUNT = "saved_card_from_account";
    public static final String USER_PROFILE_PIC_BASE_PATH = "/Android/data/";
    public static final String USER_PROFILE_PIC_FILENAME = "/profilePicture.jpg";

    public static final String USER_PROFILE_PIC_PARENT_PATH = "/Android/data/net.one97.paytm";

    public static final String KEY_PROFILE_PIC_UPDATE = "profile_pic_update";

    public static final String NPS_FEEDBACK_RECHARGE = "npsFeedback-recharge";
    public static final String NPS_FEEDBACK_MOVIES = "npsFeedback-movies";
    public static final String NPS_FEEDBACK_TRAIN = "npsFeedback-train";
    public static final String INTENT_EXTRA_TEMP_TRANS_ID = "temptransid";
    public static final String INTENT_EXTRA_CINEMA_ID = "cinemaid";
    public static final String INTENT_EXTRA_MOVIE_SESSION = "moviesession";
    public static final String INTENT_EXTRA_SELECTED_MOVIE = "selectedmovie";
    public static final String INTENT_EXTRA_SEAT_SELECTION_SOURCE = "seatselectionsource";
    public static final String INTENT_EXTRA_CITY_SEARCHED = "citysearched";
    public static final String INTENT_EXTRA_SELECTED_SEATS_MOVIE = "selectedseats";
    public static final String INTENT_EXTRA_PRODUCT_ID = "productid";
    public static final String INTENT_EXTRA_CONVINIENCE_FEE = "conviniencefee";
    public static final String INTENT_EXTRA_CURRENT_SELECTED_AREA = "currentselectedarea";
    public static final String INTENT_EXTRA_CURRENT_SELECTED_SEAT = "curentselectedseat";
    public static final String INTENT_CST_ORDER_VERTICAL = "orderVertical";
    public static final String INTENT_CST_SHOW_CST_TITLE = "showCstTitle";

    public static final String MOVIES_NO_NETWORK_CACHED_SUMMARY_LIST =
            "movies_no_network_cached_summary_list";
    public static final String MOVIES_NO_NETWORK_CACHED_YOUR_ORDERS_LIST =
            "movies_no_network_cached_your_orders_list";
    public static final String CST_WALLET_VERTICAL = "wallet";

    public static final String BRANDSTORE_PHOTOS_LIST = "brandstore_photos_list";
    public static final String BRANDSTORE_PHOTO_INDEX = "brandstore_photo_index";

    public static final String EXTRA_GALLARY_VIDEO = "extra_gallary_video";

    public static final String INTENT_EXTRA_SEND_TICKET_ORDER_ID =
            "intent_extra_send_ticket_order_id";
    public static final String INTENT_EXTRA_SEND_TICKET_COUNT = "intent_extra_send_ticket_count";
    public static final String INTENT_EXTRA_IS_BUS_TICKET = "is_bus_ticket";
    public static final String INTENT_EXTRA_IS_FROM_BUS_TICKET = "is_from_bus_ticket";
    public static final String INTENT_EXTRA_IS_FROM_CONTACT_US = "is_from_contact_us";

    public static final String NPS_STATUS_SUCCESS = "7";
    public static final String NPS_STATUS_FAILED = "6";
    public static final String EXTRA_BRAND_STORE = "Brand Store";
    public static final String BRAND_CONTEXT = "brand_context";
    public static final String BRAND_TAG_SLOT = "brand_tag_slot";

    public static final String TRAIN_PAYMENT_STATUS_PENDING = "Payment Pending";
    public static final String TRAIN_PAYMENT_STATUS_FAILED = "Payment Failed";

    public static final String TRAIN_BOOKING_STATUS_FAILED = "Booking Failed";
    public static final String TRAIN_BOOKING_STATUS_SUCCESSFUL = "Booking Successful";
    public static final String TRAIN_BOOKING_STATUS_PENDING = "Booking Pending";
    public static final String TRAIN_ORDER_STATUS_PENDING = "Pending";
    public static final String TRAIN_ORDER_STATUS_CANCEL_PENDING = "Cancel Pending";

    public static final int BRAND_STORE_ABOUT_TEXT_LENGTH_CONSTANT = 200;

    public static final String BRAND_STORE_SCREEN_NAME = "/BrandStore";
    public static final String URL_TYPE_GIFT_CARD = "giftcards";
    public static final String URL_TYPE_GIFTCARD_DL = "giftcard";
    public static final String INTENT_EXTRA__MOVIES_APLLIED_PROMOCODE = "intent_extra__movies_apllied_promocode";
    public static final String INTENT_EXTRA_MOVIES_APLLIED_PROMOCODE_TEXT = "intent_extra_movies_apllied_promocode_text";

    public static final String FLIGHT_TICKET_FILTER_TYPE_NONREFUNDABLE_CHECK = "non_refundable_check";
    public static final String[] sortByArray = {"Departure Time - Earliest to Late", "Departure Time - Late to Earliest",
            "Duration - Shortest to Longest", "Duration - Longest to Shortest",
            "Arrival Time - Late to Earliest", "Arrival Time - Earliest to Late"};
    public static final String INTENT_EXTRA_GRID_FILTERS = "intent_extra_grid_filters";
    public static final String INTENT_EXTRA_GRID_SELECTED_FILTERS_POSITION = "intent_extra_grid_selected_filters_position";
    public static final String INTENT_EXTRA_GRID_FILTERED_BY_LIST = "intent_extra_grid_filtered_by_list";
    public static final String INTENT_EXTRA_GRID_PRICE_FILTERS = "intent_extra_grid_price_filters";
    public static final String INTENT_EXTRA_NEW_CATEGORY_SELECTED = "intent_extra_new_category_selected";
    public static final int REQUEST_CODE_GRID_FILTERS = 1212;

    // Utilities V2
    public static final String INTENT_EXTRA_UTILITY_PRODUCT_LIST_V2 = "intent_extra_utility_product_list_v2";
    public static final int REQUEST_CODE_SELECT_LOCATION = 115;
    public static final String INTENT_EXTRA_ITEM_INDEX = "intent_extra_item_index";
    public static final String INTENT_EXTRA_CHILD_INDEX = "intent_extra_child_index";
    public static final String INTENT_EXTRA_USER_SELECTED_LOCATION_DATA = "selected_location";
    public static final String ADDRESS_CITY = "address_city";
    public static final String ADDRESS_STATE = "address_state";
    public static final String LOCATION_TITLE = "location_title";
    public static final String LOCATION_SEARCH_HINT = "location_search_hint";
    public static final String ACCORDION = "accordion";
    public static final String UTILITY_VARIANT = "operator_variant";
    public static final String GRID = "grid";
    public static final String LIST_TYPE = "list_type";
    public static final int UTILITY_DEFAULT_API_REQ_CODE = 601;
    public static final int UTILITY_FILTERED_API_REQ_CODE = 602;
    public static final int UTILITY_PAYMENT_MODE_REQ_CODE = 604;
    public static final String UTILITY_PRODUCT_VISIBILITY_HARD = "hard";
    public static final String UTILITY_PRODUCT_VISIBILITY_SOFT = "soft";
    public static final String UTILITY_PRODUCT_VISIBILITY_NONE = "none";
    public static final String UTILITY_LOAN_PROVIDER = "loan";
    public static final String UTILITY_CHALLAN_URL_TYPE = "challan";
    public static final String UTILITY_GOLD_LOAN_PROVIDER = "gold loan";
    public static final String LOCATION_REQUIRED = "location_required";


    //**********************nearby feature deeplink constant***********
    public static final String URL_TYPE_PAYTM_NEARBY = "nearby";
    public static final String IS_NEARBY_MESSAGE = "isNearbyTutorialMessageDone";
    public static final String IS_BANK_MESSAGE_VISIBLE = "isbankMessageShownLimitAchived";

    public static final String INTENT_FILTER_WALLET_RELOAD_SUBWALLET = "net.one97.paytm.wallet.RELOAD_SUBWALLET";
    public static final String INTENT_EXTRA_SUB_WALLET_TYPE = "SUB_WALLET_TYPE";

    public static final String FROM_PROFILE = "from_profile";

    public static final String PREF_LANGUAGE = "Preflanguage";

    public static final String URL_TYPE_PAYMENT_BANK = "payment_bank";

    public static final String BANK_STATUS_ACCOUNT_INITIATED = "ACCOUNT_INITIATED";
    public static final String BANK_STATUS_CIF_OPENED = "CIF_OPENED";
    public static final String BANK_STATUS_ACCOUNT_OPENED = "ACCOUNT_OPENED";
    public static final String BANK_STATUS_DEBIT_CARD_ISSUED = "DEBIT_CARD_ISSUED";
    public static final String BANK_STATUS_ACCOUNT_ACTIVATED = "ACCOUNT_ACTIVATED";
    public static final String BANK_STATUS_FAILED = "FAILED";

    public static final String ENGLISH_CODE = "en";
    public static final String MALYALAM_CODE = "ml";
    public static final String TELUGU_CODE = "te";
    public static final String TAMIL_CODE = "ta";
    public static final String ORIYA_CODE = "or";
    public static final String MARATHI_CODE = "mr";
    public static final String GUJRATI_CODE = "gu";
    public static final String PUNJABI_CODE = "pa";
    public static final String BANGALI_CODE = "bn";
    public static final String HINDI_CODE = "hi";
    public static final String KANNADA_CODE = "kn";
    public static final String ENGLISH_CODE_US = "en_US";


    public static final int ENGLISH_ID = 1;
    public static final int MALYALAM_ID = 6;
    public static final int TELUGU_ID = 9;
    public static final int TAMIL_ID = 8;
    public static final int ORIYA_ID = 4;
    public static final int MARATHI_ID = 5;
    public static final int GUJRATI_ID = 10;
    public static final int PUNJABI_ID = 11;
    public static final int BANGALI_ID = 3;
    public static final int HINDI_ID = 2;
    public static final int KANNADA_ID = 7;


    public static final Map<String, Integer> LANGUAGE_MAP_ID = new HashMap<String, Integer>() {{
        put(ENGLISH_CODE, ENGLISH_ID);
        put(MALYALAM_CODE, MALYALAM_ID);
        put(TELUGU_CODE, TELUGU_ID);
        put(TAMIL_CODE, TAMIL_ID);
        put(ORIYA_CODE, ORIYA_ID);
        put(MARATHI_CODE, MARATHI_ID);
        put(GUJRATI_CODE, GUJRATI_ID);
        put(PUNJABI_CODE, PUNJABI_ID);
        put(BANGALI_CODE, BANGALI_ID);
        put(HINDI_CODE, HINDI_ID);
        put(KANNADA_CODE, KANNADA_ID);
        put(ENGLISH_CODE_US, ENGLISH_ID);
    }};


//*******************deeplink for send to bank fill details screen********************

    public static final String URL_TYPE_PAYTM_SEND_MONEY_BANK_FILL_DETAILS = "transfer_money_bank";
    public static final String KEY_ENTITY_MERCHANT_ID = "entity_merchant_ID";
    public static final String KEY_SD_MERCHANT_BOOL = "is_sd_merchant";
    public static final String KEY_ACCEPT_MONEY_BANK_SEL = "is_accept_money_bank_sel";

    public static final String NEARBY_CASH_POINT = "CASH_POINT";
    public static final String NEARBY_KYC_POINT = "KYC_POINT";
    public static boolean ROAMING_ALRM_SET = false;
    public static final String NEARBY_PAYMENT_POINT = "PAYMENT_POINT";
    public static final String BROADCAST_ACTION_LOCATION_RECEIVED = "net.one97.paytm.ACTION_LOCATION_RECEIVED";
    public static final String EXTRA_LOCATION_LATITUDE = "EXTRA_LOCATION_LATITUDE";
    public static final String EXTRA_LOCATION_LONGITUDE = "EXTRA_LOCATION_LONGITUDE";

    public static final String PREF_KEY_NEARBY_CITY_STORE_FRONT = "naerby_city_store_front";
    public static final String PREF_NEARBY_CITY_STORE_FRONT = "nearby_store_front";

    public static final String INTENT_FILTER_WALLET_TYPE_CHANGED = "net.one97.paytm.wallet.CHANGED";
    public static final String INTENT_FILTER_RETRY_BANK_SERVICE = "net.one97.paytm.wallet.RETRY_BANK_SERVICE";
    public static final String INTENT_EXTRA_TAB_NUMBER = "TAB_NUMBER";
    public static final String IRCTC_USER_ID_ERROR = "irctc_user_id_error";
    public static final String TRAIN_TRAVELLER_INFO_ERROR = "train_traveller_info_error";


    public static final String BRAND_STORE_SEARCH_ACTIVITY_KEY = "DEFAULT";

    public static final int ANDROID_LOLLYPOP = 21;
    public static final String PREFETCH_1 = "1";
    public static final String PREFETCH_0 = "0";

    // Language Ids
    public static final int ENGLISH_CODE_ID = 1;
    public static final int HINDI_CODE_ID = 2;
    public static final int BANGALI_CODE_ID = 3;
    public static final int ORIYA_CODE_ID = 4;
    public static final int MARATHI_CODE_ID = 5;
    public static final int MALYALAM_CODE_ID = 6;
    public static final int KANNADA_CODE_ID = 7;
    public static final int TAMIL_CODE_ID = 8;
    public static final int TELUGU_CODE_ID = 9;
    public static final int GUJRATI_CODE_ID = 10;
    public static final int PUNJABI_CODE_ID = 11;

    public static final String TRAIN_CANCEL_REFUND_PREFIX = "http://drive.google.com/viewerng/viewer?embedded=true&url=";
    public static final String TRAIN_CANCEL_REFUND_POLICY = "http://contents.irctc.co.in/en/REFUND%20RULES%20wef%2012-Nov-15.pdf";

    public static final String PREF_MERCHANT_QR_ID = "merchant_qr_id";
    public static final String URL_TYPE_PAYTM_ACCEPT_PAYMENT = "accept_money";

    public static final String EXTRA_NOMINEE_ITEM = "NOMINEE_ITEM";
    public static final String EXTRA_ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    public static final String EXTRA_CARD_ID = "CARD_ID";
    public static final String EXTRA_CARD_DETAILS = "CARD_DETAILS";
    public static final String EXTRA_CUST_PRODUCT = "CUST_PRODUCT";
    public static final String EXTRA_CUSTOMER_PROFILE = "CUSTOMER_PROFILE";
    public static final String EXTRA_ISA_DETAILS = "ISA_DETAILS";
    public static final String EXTRA_DO_CREATE_ACCOUNT = "DO_CREATE_ACCOUNT";
    public static final String EXTRA_HEADER = "ENTER_HEADER";
    public static final String EXTRA_TITLE = "ENTER_TITLE";
    public static final String EXTRA_ACCOUNT_LIST = "EXTRA_ACCOUNT_LIST";
    public static final String EXTRA_ACTION_ADD_BALANCE_ITEMS = "net.one97.paytm.payment_bank.ACTION_ADD_BALANCE_ITEMS";
    public static final String EXTRA_ADD_FIXED_DEPOSIT = "ADD_FIXED_DEPOSIT";
    public static final String EXTRA_FIXED_DEPOSIT_BALANCE = "FIXED_DEPOSIT_BALANCE";
    public static final String REDIRECT_TO = "REDIRECT_TO";
    public static final String EXTRA_ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String EXTRA_FROM = "FROM";
    public static final String EXTRA_BLOCK_CARD = "BLOCK_CARD";
    public static final String EXTRA_UNBLOCK_CARD = "UNBLOCK_CARD";
    public static final String EXTRA_FIXED_DEPOSIT = "FIXED_DEPOSIT";
    public static final String EXTRA_SAVING_ACCOUNT_DETAILS = "SAVING_ACCOUNT_DETAILS";

    //Roaming feature variables
    public static final String KEY_MOBILE_FOR_ROAMING = "roaming_mobile_no";
    public static final String KEY_ROAMING_ALARM = "key_roaming_alarm";
    public static final String KEY_TRAVEL_ALARM = "key_travel_alarm";
    public static final String ROAMING_SET_TWICE = "roaming_set_twice";
    public static final String TRAVEL_SET_TWICE = "travel_set_twice";
    public static final String ROAMING_ENABLED = "roaming";
    public static final String IS_POSTPAID = "is_postpaid";

    public static final String URL_TYPE_FREE_MOVIE = "freemovie";
    public static final String FREE_MOVIE_FILTER_PARAM = "freemovie";
    public static final String PASSBOOK_EXTENDED_TXN_INFO = "EXTENDED_TXN_INFO";
    public static final String WALLET_TYPE = "walletType";
    public static final String EXTRA_CUSTOMER_ACCOUNT_NUMBER = "saving account number";

    public static final String SCREEN_NAME = "screenName";
    public static final String EXTRA_CARD_TYPE = "card type";
    public static final String IS_PREAUTH_MESSAGE_VISIBLE = "isPreAuthMessageVisible";

    public static final String TAB_REQUEST_A_VISIT = "Request";
    public static final String TAB_VISIT_STATIC = "Static";
    public static final String TAB_VISIT_DYNAMIC = "Dynamic";

    public static final String KEY_USER_TYPE_SIGN_IN_SHOWN = "key_user_is_sign_in_shown";
    public static final String PASSENGER_TITLE = "passenger_title";

    public static final String KEY_VAL_OTP = "OTP";
    public static final String KEY_VAL_PASSCODE = "PASSCODE";

    public static final String GOOGLE_TNC_URL = "https://play.google.com/intl/ALL_in/about/card-terms.html";
    public static final String CATEGORY_ID_GOOGLE_PLAY = "99932";

    public static final String RECHARGE_GA_DATA_FOR_SUMMARY = "recharge_ga_data_for_summary";
    public static final String TICKETS_VIRTICAL_ID = "26";
    public static final String FLIGHTS_VIRTICAL_ID = "64";
    public static final String HOTEL_VIRTICAL_ID = "60";
    public static final String TRAINS_VIRTICAL_ID = "72";
    public static final String BUS_CATEGORY_ID = "25173";

    public static final String UBER_TEXT_WHEN_INSTALLED_01 = "You will now be redirected to Uber app \n where you can book your ride.\n \n";
    public static final String UBER_TEXT_WHEN_INSTALLED_02 = "Do you wish to proceed?";

    public static final String UBER_TEXT_WHEN_NOT_INSTALLED_01 = "Oops! Looks like you don't have the Uber app installed.\n \n";
    public static final String UBER_TEXT_WHEN_NOT_INSTALLED_02 = "Would you like to download the Uber app \n now to book your ride? \n \n";
    public static final String UBER_TEXT_WHEN_NOT_INSTALLED_03 = "Use the code \"BookUber\" and avail ₹50 off on your first 3 rides";

    public static final String INSURANCE_ALAWAYS_TRUE = "minsuranceAlwaysTrue";
    public static final String INSURANCE_OPTIONS = "minsuranceOption";
    public static final String EXTRA_INSURANCE_CHECKED = "insuranceChecked";

    public static final String PREF_KEY_NEARBY_LATITUDE = "nearby_lat";
    public static final String PREF_KEY_NEARBY_LONGITUDE = "nearby_long";

    public static final String SEARCH_MAP = "search_map";

    public static final String IS_SMS_PERMISSION_MESSAGE_VISIBLE = "isSMSpermissionMsgvisibl";
    public static final String IS_LOCATION_PERMISSION_MESSAGE_VISIBLE = "isLocationpermissionMsgvisibl";
    public static final String IS_CALL_PERMISSION_MESSAGE_VISIBLE = "isCallpermissionMsgvisibl";
    public static final String IS_CONTACT_PERMISSION_MESSAGE_VISIBLE = "isContactpermissionMsgvisibl";
    public static final String INTENT_EXTRA_FILTER_URL_PARAMS = "intent_extra_filter_url_params";
    public static final String INTENT_EXTRA_GRID_FULL_URL = "intent_extra_grid_full_url";
    public static final String INTENT_EXTRA_FRONT_END_FILTERS = "intent_extra_front_end_filters";
    public static final String INTENT_EXTRA_CATEGORY_STRING_IN_URL = "intent_extra_category_string_in_url";

    public static final String MP_REPORT_ERROR_MAIL_ID = "error.marketplace@paytm.com";

    public static final String INTENT_EXTRA_ISSUE_TICKET_NUMBER = "issueTicketNumber";
    public static final String URL_TYPE_CST = "cst";

    public static final String INTENT_EXTRA_CST_ISSUE_DETAIL_FILE_URI = "file_uri";
    public static final String CST_ISSUE_DETAIL_DATE_FORMAT = "yyyyMMdd_HHmmss";
    public static final String CST_ISSUE_DEATIL_IMAGE_DIRECTORY_NAME = "Paytm";
    public static final String INTENT_EXTRA_ISSUE_TICKET_TITLE = "issueTicketTitle";
    public static final String INTENT_EXTRA_ISSUE_TICKET_SUB_TITLE = "issueTicketSubTitle";
    public static final String INTENT_EXTRA_ISSUE_TICKET_ITEM_NAME = "issueTicketSubTitle";
    public static final String INTENT_EXTRA_ISSUE_TICKET_ITEM_ID = "issueTicketItemId";

    public static final String ITEM_STATUS_15 = "15";
    public static final String ITEM_STATUS_22 = "22";
    public static final String ITEM_STATUS_2 = "2";

    public static final int KEY_FORGOT_PASSWORD = 619;
    public static final String PASSCODE_RESET_SCOPE = "reset_secret";
    public static final String UTILITIES_REPORT_ERROR_MAIL_ID = "error.utilities@paytm.com";
    public static final String MOVIES_REPORT_ERROR_MAIL_ID = "error.movies@paytm.com";
    public static final String FLIGHT_ERROR_MAIL = "error.flights@paytm.com";
    public static final String TRAIN_ERROR_MAIL = "error.trains@paytm.com";
    public static final String CST_ERROR_MAIL = "error.cst@paytm.com";
    public static final String BUS_ERROR_MAIL = "error.bus@paytm.com";
    public static final String HOTEL_ERROR_MAIL = "error.hotels@paytm.com";
    public static final String GOLD_ERROR_MAIL = "error.gold@paytm.com";
    public static final String RECHARGE_ERROR_MAIL = "error.recharges@paytm.com";
    public static final String WALLET_ERROR_MAIL = "error.wallet@paytm.com";
    public static final String AUTH_ERROR_MAIL = "error.auth@paytm.com";
    public static final String KYC_ERROR_MAIL = "error.kyc@paytm.com";
    public static final String FLIGHT_PRICE_CHANGE_CODE = "FL2_2222";


    public static final String SEARCH_GRID_CD_VALUE = "CD_VALUE";

    public static final int RECHARGE_APPLY_OFFER = 979;
    public static final String INTENT_EXTRA_OFFERS_LIST_RECHARGE = "offer_list_recharge";
    public static final String INTENT_EXTRA_IS_ADD_PAYTM_CASH = "isAddPaytmcash";
    public static final String INTENT_EXTRA_IS_FROM_PREPAID_MOBILE = "isFromPrepaidMobile";
    public static final String INTENT_EXTRA_RECHARGE_CART = "extraRechargeCart";
    public static final String INTENT_EXTRA_SELECTED_RECHRAGE_LIST = "selectedRechargeList";
    public static final String INTENT_EXTRA_SELECTED_COUPON_LIST = "selectedcouponList";
    public static final String INTENT_EXTRA_MAIN_RECHARGE_PRODUCT = "mainRechargeproduct";
    public static final String INTENT_OFFER_LIST_RECHARGE = "offers_recharge";

    public static final int MOVIE_BOTTOM_BAR = 0;
    public static final int EVENT_BOTTOM_BAR = 1;
    public static final int PARK_BOTTOM_BAR = 2;
    public static final String INTENT_EXTRA_ORDER_ID = "orderId";
    public static final String INTENT_EXTRA_WALLET_TXN_ID = "walletTxnId";
    public static final String INTENT_EXTRA_BANK_TXN_ID = "bankTxnId";
    public static final String PAYTYPE_LOAN = "Loan";
    public static final String PAYTYPE_INSURANCE = "Insurance";
    public static final String PAYTYPE_NEW_REGISTRATION = "New Registration";
    public static final String PAYTYPE_FEE_PAYMENT = "Fee payment";
    public static final String PAYTYPE_POSTPAID = "postpaid";

    public static final String KEY_FULL_NAME = "fullname";
    public static final String KEY_MEAL_TYPE = "meal_type";
    public static final String KEY_AGE = "age";
    public static final String KEY_PASSPORT_NO = "passport_no";
    public static final String KEY_NATIONALITY = "nationality";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PASSPORT_COUNTRY = "passport_country";
    public static final String KEY_PASSPORT_EXPIRY = "passport_expiry";
    public static final String KEY_PASSPORT_ISSUE_DATE = "passport_issue_date";
    public static final String KEY_IRCTC_ID = "irctc_id";
    public static final String KEY_VISA_TYPE = "visa_type";
    public static final String KEY_MIDDLE_NAME = "middlename";
    public static final String KEY_FLIGHT_FIRST_NAME = "firstname";
    public static final String KEY_FLIGHT_LAST_NAME = "lastname";
    public static final String KEY_FLIGHT_DOB = "dob";
    public static final String KEY_FREQUENT_FLYER_NO = "frequent_flyer_number";
    public static final String KEY_FREQUENT_FLYER = "frequent_flyer";
    public static final String KEY_DATE = "date";
    public static final String KEY_TEXT_INPUT = "textInput";
    public static final String KEY_DROPDOWN = "dropdown";
    public static final String KEY_MULTI_DROPDOWN = "multi_dropdown";
    public static final String KEY_AUTOSUGGEST = "autosuggest";
    public static final String KEY_PASSPORT_ISSUE_COUNTRY = "passport_issue_country";
    public static final String KEY_BIRTH_COUNTRY= "birth_country";
    public static final String KEY_GST_NUMBER = "gst_number";

    /*
     Intro screen for travel profile
     */
    public static final String KEY_BUS_INTRO = "bus_intro_flag";
    public static final String KEY_FLIGHT_INTRO = "flight_intro_flag";
    public static final String KEY_TRAIN_INTRO = "train_intro_flag";

    public static final String URL_TYPE_BAZAAR = "bazaar";
    public static final int POSITION_HOME = 0;
    public static final int POSITION_MALL = 1;
    public static final int POSITION_PROFILE = 2;
    public static final int POSITION_NOTIFICATION = 3;

    public static final String TRIP_ID = "trip_id";
    public static final String SELECTED_SEAT = "selected_seats";
    public static final String GCLID = "gclid";
    public static final String INTENT_EXTRA_BBPS_LOGO_URL = "bbps_logo_url";


    public static final String FLIGHT_HOME_RECENT_SEARCH_DETAILs = "flight_recent_search_details";

    public static final String PUSH_FLIGHT_REFERRAL = "referral";
    public static final String FLIGHT_OPENED_FROM = "opened_from";
    public static final String FLIGHT_NON_STOP_SELECTED = "nonstop";

    public static final String URL_TYPE_VIDEO = "video";

    public static final String URL_TYPE_KYC_PAN_DEEPLINK = "add_pan";
    public static final String URL_TYPE_KYC_ADHAR_DEEPLINK = "add_aadhaar";
    public static final String KYC_DEEPLINK_PAN = "kyc_pan_deeplink";
    public static final String KYC_DOC_DEEPLINK = "is_kyc_doc_deeplink";
    public static final String URL_TYPE_DEEPLINK_PASSCODE = "passcode";

    public static final String PASSBOOK_ENTRY_DETAIL = "passbook_detail_data";
    public static final String DEALS_MESSAGE = "deals_message";
    public static final String META_DATA_DEALS_MESSAGE = "DealsMessage";

    public static final String YOUTUBE_VIDEO_DETAIL_API_KEY = "AIzaSyBin1_WqVQOPHYvUPYpCgtpxNgNbwF4k20";

    public static final String EXTRA_KEY_IS_FROM_PASSBOOK = "is_from_passbook";

    public static final String LONG_PRESS_PAYTM_MENU_BUTTON_CLICKED = "long_press_paytm_menu_button_clicked";
    public static final String MENU_BUTTON_NAME = "menu_button_name";
    public static final String UTILITY_HIDE_TITLE = "utility_hide_title";

    public static final String EXTRA_SOCIAL_USERNAME = "username";
    // Banner GA constant
    public static final String SOURCE_POSITION_KEY = "source_position";
    public static final String SOURCE_ID_KEY = "source_id";
    public static final String SOURCE_CONTAINER_INSTANCE_ID_KEY = "source_container_instance_id";
    public static final String SOURCE_CONTAINER_ID_KEY = "source_container_id";


    // New Order Summary Type
    public static final String ORDER_SUMMARY_TYPE = "order-summary-type";
    public static final String ORDER_SUMMARY_TYPE_FLIGHT = "flight";
    public static final String URL_TYPE_CHALLAN = "challan";
    public static final String META_DATA_FEE_TYPE_KEY = "fee_type_key";
    public static final String BANK_PASSBOOK_OFFLINE = "bank_passbook_offline";

    public static final String ORDER_FF_RESPONSE = "fulfillment_response";
    public static final String ORDER_QUANTITY = "quantity";
    public static final String ORDER_PINCODE = "recharge_number_5";
    public static final String ORIGIN_MY_ORDERS = "MyOrders";

    public static final String URL_TYPE_BANK_KYC = "payments_bank";

    public static final String PAYMENT_BANK = "Payments Bank";

    public static final String ORDER_SUMMARY_APPLICATION_XML = "application/xml";

    public static final String SOURCE_LAYOUT_TYPE = "widget_layout_type";
    public static final String APP_VERSION_NUMBER = "app_version_number_";
    public static final String DEEPLINK_SCHEMA_PAYTM = "paytmmp://";
    public static final String DEEPLINK_SCHEMA_PAYTM_MALL = "paytmmall://";

    public static final String CART_META_DATA = "meta_data";
    public static final String CART_DEEPLINK = "deeplink";
    public static final String CART_QR_CODE_TIMESTAMP = "qrcode_scan_timestamp";
    public static final String CART_QRCODE_ID = "qrcode_id";
    public static final String CART_AFFILIATE_ID = "affiliate_id";
    public static final String CART_CONFIGURATION = "configuration";
    public static final String O2O_BOTTOM_TAB = "O2OBottomTab";

    public static final String GTM_KEY_VALUE_FOR_COUPON_SCREEN = "/coupon/";
    public static final int PAYMENT_MODE_REQ_CODE = 604;
    public static final String INTENT_EXTRA_PAYMENT_MODE_LIST = "payment_mode_list";
    public static final String INTENT_EXTRA_CONVENIENCE_FEE_MAP = "convenience_fee_map";
    public static final String INTENT_EXTRA_PAYMENT_MODE_SELECTED = "payment_mode_selected";
    public static final String INTENT_EXTRA_CONVENIENCE_FEE = "convenience_fee_selected";
    public static final String KEY_VALIDATION_PAYMENT_METHOD = "payment_method";
    public static final String KEY_USER_ACCOUNT_USER_ID = "user_account_user_id";
    public static final String KEY_USER_ACCOUNT_CURRENT_APP = "user_account_current_app_version";
    public static final String KEY_USER_ACCOUNT_UPDATE_APP = "user_account_update_app_version";
    public static final String KEY_USER_ACCOUNT_UPGRADE_CLICKED = "user_account_upgrade_now_clicked";

    public static final String UBER_ON_HOLD_BLOG_URL = "https://paytm.com/offer/uber-introduces-a-new-feature-on-paytm/";

    public static final String SAVING_ACCOUNT_BALANCE_MODEL = "EFFAVL";
    public static final String SUSPENSE_ACCOUNT_BALANCE_MODEL = "SUSPENSE_BAL";
    public static final String KEY_USER_ACCEPT_PYMT_KYC = "user_is_kyc_status";
    public static final String KEY_USER_ACCEPT_PYMT_MERCHANT = "user_is_merchant";
    public static final String KEY_USER_ACCEPT_PYMT_NEW_WALLET = "new_wallet_screen_type";
    public static final String KEY_USER_ACCEPT_PYMT_SCREEN_TYPE = "accept_payment";
    public static final String KEY_USER_ACCEPT_PYMT_PROCEED = "new_wallet_accept_payment_proceed_clicked";
    public static final String KEY_USER_ACCEPT_PYMT_REQUEST = "new_wallet_accept_payment_request_kyc_clicked";

    public static final String KEY_USER_EVENT_SUBS_SELECTED = "user_account_subscriptions_tab_name_selected";
    public static final String KEY_USER_SUBS_TAB_NAME_VAR = "user_account_subscriptions_tab_name";
    public static final String KEY_USER_EVENT_DELINK_CLICK = "user_account_subscriptions_delink_wallet_clicked";
    public static final String KEY_USER_SUBS_MERCH_NAME_VAR = "user_account_subscriptions_merchant_name";

    public static final String ACTIVE = "ACTIVE";

    public static final String BLACKLIST = "BLACKLIST";

    public static final String LOW_BALANCE = "LOW BALANCE";
    public static final String HOT_LIST = "HOTLIST";
    public static final String SUSPENSION = "SUSPENSION";


    public static final String KEY_ORDER_ID_TO_TRACK = "KEY_ORDER_ID_TO_TRACK";
    public static final String KEY_ITEM_ID_TO_TRACK = "KEY_ITEM_ID_TO_TRACK";

    public static final String CALL_URI_CONSTANT_PREFIX = "tel:";
    public static final String EMAIL_TYPE_FOR_INTENT = "message/rfc822";
    public static final String EMAIL_URI_PREFIX_FOR_INTENT = "mailto:";
    public static final String RETURN_REPLACE_V2 = "returnreplacev2";

    public static final String KEY_ORDERED_CART = "KEY_ORDERED_CART";


    //p2m deeplink constants
    public static final String DEEPLINK_P2M_AMOUNT = "amount";
    public static final String DEEPLINK_MID = "recipient";
    public static final String DEEPLINK_P2M_COMMENT = "comment";
    public static final String ORDER_ITEM_STATUS_SUCCESSFUL = "20";
    public static final int VERTICAL_ID_ADD_MONEY_ORDER = 16;

    public static final String PAYTM_COM = "paytm.com";
    public static final String BANK_REPORT_CODE = "reportCode";
    public static final String BANK_TRANSACTION_DATE = "transaction_date";
    public static final String CST_BANK_VERTICAL_LABEL = "bank";
    public static final String BANK_TXN_DESCRIPTION = "bank_description";

    public static final String EXTRA_MONEY_TRANSFER_TOBANK_FRM_PASSBOOK = "send money to bank from passbbok";

    public static final String EXTRA_REDEEM_FD = "redeem_fd";
    public static final String EXTRA_FD_ACCOUNT_ID = "fd_account_id";
    public static final String EXTRA_REDEEM_FD_LIST = "redeem_fd_list";
    public static final String EXTRA_FD_TOTAL = "fd_total";
    public static final String EXTRA_SAVINGS_ACCOUNT_BALANCE = "savings_account_balance";

    public static final String POWER_PLAY = "powerplay";
    public static final String PASS_ACCESS_TOKEN = "pass_access_token";

    public static final String INTENT_EXTRA_DIGITAL_CREDIT_TITLE = "digital_credit_title";
    public static final String INTENT_EXTRA_DIGITAL_CREDIT_APPLICATION_STATUS = "application_status";
    public static final String INTENT_EXTRA_DIGITAL_CREDIT_LENDER = "lender";
    public static final String INTENT_EXTRA_DIGITAL_CREDIT_ITEM = "digital_credit_item";
    public static final String INTENT_EXTRA_FROM_DIGITAL_CREDIT = "from_digital_credit";
    public static final int REQUEST_CODE_APPLY_DIGITAL_CREDIT = 0x1000;
    public static final int REQUEST_CODE_ENTER_DETAILS_DIGITAL_CREDIT = 0x2000;
    public static final int REQUEST_CODE_DIGITAL_CREDIT_VERIFY_PASSCODE = 0x4000;
    public static final int REQUEST_CODE_DIGITAL_CREDIT_SET_PASSCODE = 0x8000;
    public static final String INTENT_EXTRA_FROM_DIGITAL_CREDIT_LAST_ID = "last_id";
    public static final String INTENT_EXTRA_FROM_DIGITAL_CREDIT_COUNT = "count";

    //order details revamp
    public static final String NUMBER_OF_DAYS = "numberOfDays";


    public static final String CONTACT_US_NUMBER = "0120 3888 388";

    public static final String TYPE_INPUT_PASSWORD = "password";
    public static final String INPUT_TYPE_NUMERIC = "numeric";
    public static final String CREDIT_CARD_CONFIG = "recharge_number";
    public static final String CREDIT_CARD_CONFIG_2 = "recharge_number_2";
    public static final String CART_VERIFY_TYPE = "CC_BILL_PAYMENT";
    public static final String CREDIT_CARD_TITLE = "Credit Card Number";
    public static final String INTENT_EXTRA_ORDERED_PRODUCT = "ordered_product";
    public static final String DEEPLINK_CONTACTUS_ISSUE_PARENT_ID = "parent_id";
    public static final String DEEPLINK_CONTACTUS_ISSUE_VERTICAL_LABEL = "vertical_label";

    public static final String SEARCH_URL_SCHEME = "search.paytm.com";

    public static final String EXTRA_INTENT_IS_SOLD_OUT_HOTEL = "isSoldOutHotel";
    public static final String EXTRA_INTENT_SOLD_OUT_HOTEL_ID = "soldOutHotelId";
    public static final String CST_ISSUE_TYPE = "issue";
    public static final String CST_CTA_TYPE = "cta";
    public static final String CST_MESSAGE_TYPE = "message";
    public static final String CST_CTA_RAISE_ACTION = "raise ticket";
    public static final String CST_CTA_CALL_ACTION = "call";
    public static final String CST_CTA_CONTINUE_SHOPPING_ACTION = "continue shopping";
    public static final String CST_CTA_GO_TO_YOUR_PRODUCT_ACTION = "go to your product";
    public static final String CST_CTA_GO_TO_ORDER_HISTORY_ACTION = "go to order history";
    public static final String CST_CTA_RETRY_RECHARGE_ACTION = "retry recharge";
    public static final String CST_CTA_RETURN_REPLACEMENT_ACTION = "return/replace";
    public static final String CST_CTA_CANCEL_ITEM_ACTION = "cancel item";
    public static final String CST_CTA_CANCEL_RECHARGE_ACTION = "cancel recharge";
    public static final String CST_CTA_DOWNLOAD_INVOICE_ACTION = "download invoice";
    public static final String ACTION_ITEM_CANCEL = "Cancel_Item";
    public static final String CST_RAISE_ACTION_TEXT = "Message Us";
    public static final String CST_CONTINUE_SHOPPING_ACTION_TEXT = "Continue Shopping";

    public static final String INTENT_EXTRA_PAYMENT_OPTIONS = "intent_extra_payment_options";
    public static final int UTILITY_AMOUNT_SELECTION_REQ_CODE = 605;
    public static final String INTENT_EXTRA_UTILITY_SELECTED_AMOUNT = "intent_extra_selected_amount";
    public static final String INTENT_EXTRA_CST_ISSUES_LEVEL_NUMBER = "intent_extra_level_number";
    public static final String INTENT_EXTRA_CST_ISSUES_NODE_NUMBER = "intent_extra_node_number";
    public static final String CST_CTA_DOWNLOAD_IMAGE_ACTION = "download image";
    public static final String INTENT_EXTRA_IS_FROM_TICKETS = "isFromTicekts";

    //Barcode
    public static final int BARCODE_TYPE_MALL = 1007;
    public static final int BARCODE_TYPE_PAYTM = 1008;


}
