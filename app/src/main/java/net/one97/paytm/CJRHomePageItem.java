
package net.one97.paytm;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CJRHomePageItem extends CJRItem {

    private static final long serialVersionUID = 1L;

    @SerializedName("name")
    public String mName;

    @SerializedName("price")
    public String mPrice;

    @SerializedName("image_url")
    public String mImageUrl;

    @SerializedName("alt_image_url")
    public String mAltImageUrl;


    @SerializedName("seourl")
    public String mseourl;
    @SerializedName("pushCityValue")
    private String pushCityValue;
    private String mPushFilterJson;


    public ArrayList<CJRHomePageItem> getMproducts() {
        return mproducts;
    }

    public void setMproducts(ArrayList<CJRHomePageItem> mproducts) {
        this.mproducts = mproducts;
    }

    @SerializedName("products")
    private ArrayList<CJRHomePageItem> mproducts = new ArrayList<CJRHomePageItem>();


    private Map<String, Object> sourceInfo = null;

    public String mReferralNo;

    public String mRefferralAmount;

    public String mReferralRechargeType;

    public String mReferralSource;

    public String mSearchType;

    public String mSearchCategory;

    public String mSearchTerm;

    public String mSearchResultType;

    public String mGTMListName;

    public String mContainerInstanceID;

    public String mSearchABValue;

    private String qRCodeId;
    private String affilaiteID;
    private String timestamp;
    private String deeplink;

    public String getqRCodeId() {
        return qRCodeId;
    }

    public void setqRCodeId(String qRCodeId) {
        this.qRCodeId = qRCodeId;
    }

    public String getAffilaiteID() {
        return affilaiteID;
    }

    public void setAffilaiteID(String affilaiteID) {
        this.affilaiteID = affilaiteID;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDeeplink() {
        return deeplink;
    }

    public void setDeeplink(String deeplink) {
        this.deeplink = deeplink;
    }

    @Override
    public String getmContainerInstanceID() {
        return mContainerInstanceID;
    }

    @Override
    public String getParentID() {
        return mParentId;
    }

    public void setmContainerInstanceID(String mContainerInstanceID) {
        this.mContainerInstanceID = mContainerInstanceID;
    }

    public String qrid;
    public int mGTMPosition;
    @SerializedName("url_type")
    public String mUrlType;
    @SerializedName("url")
    public String mUrl;
    @SerializedName("status")
    public String mStatus;
    @SerializedName("has_reward")
    public boolean mHasRewards;
    @SerializedName("reward_text")
    public String mRewardText;
    @SerializedName("description")
    public String mDescription;
    @SerializedName("login_required")
    public boolean mLoginRequired;

    @SerializedName("rating")
    public float mRating;
    @SerializedName("parent_id")
    public String mParentId;
    @SerializedName("stock")
    public boolean mIsInStock;
    @SerializedName("img_width")
    public String mImgWidth;
    @SerializedName("img_height")
    public String mImgHeight;
    @SerializedName("discount")
    public String mDiscount;
    private String mSearchUrl;
    private String mSearcKey;
    private String mCategoryId;
    private boolean mIsFromSearch;
    private String mRechargeNumber;
    private String mRechargeAmount;
    private String mRechargePromoCode;
    private String mRoamingEnabled;
    private boolean imageFromResource;
    private String mTitle = "";
    @SerializedName("brand")
    private String mBrand;
    @SerializedName("offer_price")
    private float mOfferPrice;
    @SerializedName("actual_price")
    private float mActualPrice;
    @SerializedName("label")
    private String mLabel;
    @SerializedName("tag")
    private String mOfferTag;
    private String mPushProductId;
    private boolean mPushShowPopup;
    private String mPushAlertMessageBody = "";
    private String mPushTitle;
    private String mPushQuantity;
    private String mPushPromoCode;
    private String mPushCashAdd;
    private String mPushUtmSource;
    @SerializedName("featuretype")
    private String mPushFeatureType = "";
    private String mPushWalletCode;
    private String mPushRecipient;
    private String mPushComment;
    private String mUtmMedium;
    private String mUtmContent;
    private String mUtmTerm;
    private String mUtmCampaign;
    private boolean mDeepLinking;
    private String mPushCheckInDate;
    private String mPushCheckOutDate;
    private String mPushCity;
    private String mPushRoomDetailsJson;
    private String mQueryString;
    private String mPushHotelName;
    private String mPushHotelId;
    private String mPushHotelExtras;
    private String mPushHotelFinalPriceWithTax;
    private String mOrigin;
    private String mPushSourceCityName;
    private String mPushSourceCityShortName;
    private String mPushDestinationCityName;
    private String mPushDestinationCityShortName;
    private String mPushDate;
    private String mPushPassengerCount;
    private String mPushType;
    private String mPushCode;

    private String ifsc;
    private String userName;
    private String bankAccountNumber;
    private String bankcomment;
    private String bankAmount;
    private String p2pmobilenumber;
    private String p2pamount;
    private String p2pcomment;


    private String mTrainSourceCity;
    private String mTrainDestinationCity;
    private String mTrainDepartureDate;
    private boolean isTrainRoundTrip;

    private String landing_page;
    private String mode;
    private String kyc_name ;
    private String aadhar_number ;

    private String mTrainSourceCityName;
    private String mTrainDestinationCityName;


    private String p2mamount;
    private String merchantMID;
    private String p2mComment;

    private String overlay;

    private boolean isFromReqDelivery=false;
    @SerializedName("related_category")
    private ArrayList<CJRRelatedCategory> mRelatedCategories = new ArrayList<CJRRelatedCategory>();
    @SerializedName("url_info")
    public String mUrlInfo;
    @SerializedName("id")
    private String mItemID;
    @SerializedName("priority")
    private String mPripority;
    private String mParentItem;
    @SerializedName("category")
    private String mCategory;
    private boolean itemViewed;
    @SerializedName("source")
    private String mSource;
    private boolean isSellerStoreImpressionSent;
    @SerializedName("layout")
    private CJRLayoutParams layoutParam;

    private String layoutType;
    private String contactUsIssueParentId;
    private String contactUsIssueVerticalLabel;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getOverlay() {
        return overlay;
    }

    public void setOverlay(String overlay) {
        this.overlay = overlay;
    }

    public String getReferralRechargeType() {
        return mReferralRechargeType;
    }

    public void setReferralRechargeType(String referralRechargeType) {
        this.mReferralRechargeType = referralRechargeType;
    }

    public String getReferralSource() {
        return mReferralSource;
    }

    public void setmReferralSource(String referralSource) {
        this.mReferralSource = referralSource;
    }

    public String getReferralNo() {
        return mReferralNo;
    }

    public void setReferralNo(String refNo) {
        this.mReferralNo = refNo;
    }

    public String getReferralAmount() {
        return mRefferralAmount;
    }

    public void setReferralAmount(String refAmount) {
        this.mRefferralAmount = refAmount;
    }

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String label) {
        mLabel = label;
    }

    @Override
    public String getListName() {
        return mGTMListName;
    }

    public void setListName(String listname) {
        this.mGTMListName = listname;
    }

    public String getQrid() {
        return qrid;
    }

    public void setQrid(String qrid) {
        this.qrid = qrid;
    }


    @Override
    public int getListPosition() {
        return mGTMPosition;
    }

    public void setListPosition(int position) {
        this.mGTMPosition = position;
    }

    @Override
    public String getSearchType() {
        return mSearchType;
    }

    public void setSearchType(String searchType) {
        this.mSearchType = searchType;
    }

    @Override
    public String getSearchCategory() {
        return mSearchCategory;
    }

    public void setSearchCategory(String searchCategory) {
        this.mSearchCategory = searchCategory;
    }

    @Override
    public String getSearchTerm() {
        return mSearchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.mSearchTerm = searchTerm;
    }

    @Override
    public String getSearchResultType() {
        return mSearchResultType;
    }

    public void setSearchResultType(String searchResultType) {
        this.mSearchResultType = searchResultType;
    }

    public String getSearchUrl() {
        return mSearchUrl;
    }

    public void setSearchUrl(String mSearchUrl) {
        this.mSearchUrl = mSearchUrl;
    }

    public String getSearchKey() {
        return mSearcKey;
    }

    public void setSearcKey(String mSearcKey) {
        this.mSearcKey = mSearcKey;
    }

    public String getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(String categoryId) {
        this.mCategoryId = categoryId;
    }

    public void setIsFromSearch(boolean isFromSearch) {
        this.mIsFromSearch = isFromSearch;
    }

    public boolean isFromFromSearch() {
        return mIsFromSearch;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getStatus() {
        return mStatus;
    }

    public boolean isHasRewards() {
        return mHasRewards;
    }

    public String getRewardText() {
        return mRewardText;
    }

    public String getDescription() {
        return mDescription;
    }

    public boolean isLoginRequired() {
        return mLoginRequired;
    }


    public float getRating() {
        return mRating;
    }

    public String getParentId() {
        return mParentId;
    }

    public String getPushMessageBody() {
        return mPushAlertMessageBody;
    }

    public void setPushMessageBody(String pushMessageBody) {
        mPushAlertMessageBody = pushMessageBody;
    }

    public String getPushTitle() {
        return mPushTitle;
    }

    public void setPushTitle(String pushTitle) {
        mPushTitle = pushTitle;
    }

    public boolean isPushShowPopup() {
        return mPushShowPopup;
    }

    public void setPushShowPopup(boolean pushShowPopup) {
        mPushShowPopup = pushShowPopup;
    }

    public String getPushProductId() {
        return mPushProductId;
    }

    public void setPushProductId(String mPushProductId) {
        this.mPushProductId = mPushProductId;
    }

    public String getPushQuantity() {
        return mPushQuantity;
    }

    public void setPushQuantity(String mPushQuantity) {
        this.mPushQuantity = mPushQuantity;
    }

    public String getPushPromoCode() {
        return mPushPromoCode;
    }

    public void setPushPromoCode(String mPushPromoCode) {
        this.mPushPromoCode = mPushPromoCode;
    }

    public String getPushCashAdd() {
        return mPushCashAdd;
    }

    public void setPushCashAdd(String mPushCashAdd) {
        this.mPushCashAdd = mPushCashAdd;
    }

    public String getUtmSource() {
        return mPushUtmSource;
    }

    public void setPushUtmSource(String source) {
        mPushUtmSource = source;
    }

    public String getOfferTag() {
        return mOfferTag;
    }

    public String getPushFeatureType() {
        return mPushFeatureType;
    }

    public void setPushFeatureType(String pushFeatureType) {
        this.mPushFeatureType = pushFeatureType;
    }

    public String getPushWalletCode() {
        return mPushWalletCode;
    }

    public void setPushWalletCode(String pushWalletCode) {
        this.mPushWalletCode = pushWalletCode;
    }

    public String getPushRecipient() {
        return mPushRecipient;
    }

    public void setPushRecipient(String pushRecipient) {
        this.mPushRecipient = pushRecipient;
    }

    public String getPushComment() {
        return mPushComment;
    }

    public void setPushComment(String pushComment) {
        this.mPushComment = pushComment;
    }

    public String getUrlInfo() {
        return mUrlInfo;
    }

    public String getmPripority() {
        return mPripority;
    }

    public ArrayList<CJRRelatedCategory> getRelatedCategories() {
        return mRelatedCategories;
    }

    public void setRelatedCategories(ArrayList<CJRRelatedCategory> category) {
        mRelatedCategories = category;
    }

    public float getOfferPrice() {
        return mOfferPrice;
    }

    public float getActualPrice() {
        return mActualPrice;
    }

    public String getFormatedOfferPrice() {
        if (mOfferPrice > 0) {
            return CJRAppUtils.formatNumber(String.valueOf(mOfferPrice));
        } else return "Free";
    }

    public String getFormatedActualPrice() {
        if (mActualPrice > 0) {
            return CJRAppUtils.formatNumber(String.valueOf(mActualPrice));
        } else return "Free";
    }

    @Override
    public String getURLType() {
        return mUrlType;
    }

    @Override
    public String getURL() {

        if(mUrl==null) return mseourl;
        return mUrl;
    }

    @Override
    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPrice() {
        if (mPrice != null) {
            if (!mPrice.contains("Rs")) {
                mPrice = "Rs " + mPrice;
            }
        }
        return mPrice;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getAltImageUrl() {
        return mAltImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
    }

    @Override
    public String getBrand() {
        // TODO Auto_generated method stub
        return mBrand;
    }

    @Override
    public boolean equals(Object obj) {
        try {
            return mItemID.equals(((CJRHomePageItem) obj).mItemID);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public String getItemID() {
        return mItemID;
    }

    public void setItemID(String itemId) {
        this.mItemID = itemId;
    }

    public void setUrlType(String urlType) {
        mUrlType = urlType;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getParentItem() {
        return mParentItem;
    }

    public void setParentItem(String parentItem) {
        this.mParentItem = parentItem;
    }

    public String getProductCategory() {
        return mCategory;
    }

    public void setItemViewed() {
        itemViewed = true;
    }

    public boolean isItemViewed() {
        return itemViewed;
    }

    public String getUtmMedium() {
        return mUtmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.mUtmMedium = utmMedium;
    }

    public String getUtmContent() {
        return mUtmContent;
    }

    public void setUtmContent(String utmContent) {
        this.mUtmContent = utmContent;
    }

    public String getUtmTerm() {
        return mUtmTerm;
    }

    public void setUtmTerm(String utmTerm) {
        this.mUtmTerm = utmTerm;
    }

    public String getUtmCampaign() {
        return mUtmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.mUtmCampaign = utmCampaign;
    }

    public boolean isDeepLinking() {
        return mDeepLinking;
    }

    public void setDeepLinking(boolean deepLinking) {
        this.mDeepLinking = deepLinking;
    }

    public String getPushCheckInDate() {
        return mPushCheckInDate;
    }

    public void setPushCheckInDate(String checkInDate) {
        this.mPushCheckInDate = checkInDate;
    }

    public String getPushCheckOutDate() {
        return mPushCheckOutDate;
    }

    public void setPushCheckOutDate(String checkOutDate) {
        this.mPushCheckOutDate = checkOutDate;
    }

    public String getPushCity() {
        return mPushCity;
    }

    public void setPushCity(String city) {
        this.mPushCity = city;
    }

    public String getPushRoomDetailsJson() {
        return mPushRoomDetailsJson;
    }

    public void setPushRoomDetailsJson(String roomDetailsJson) {
        this.mPushRoomDetailsJson = roomDetailsJson;
    }

    public String getQueryString() {
        return mQueryString;
    }

    public void setQueryString(String queryString) {
        this.mQueryString = queryString;
    }

    public boolean getIsInStock() {
        return mIsInStock;
    }

    public void setIsInStock(boolean mIsInStock) {
        this.mIsInStock = mIsInStock;
    }

    public String getProductImgWidth() {
        return mImgWidth;
    }

    public void setProductImgWidth(String mImgWidth) {
        this.mImgWidth = mImgWidth;
    }

    public String getProductImgHeight() {
        return mImgHeight;
    }

    public void setProductImgHeight(String mImgHeight) {
        this.mImgHeight = mImgHeight;
    }

    public String getDiscountPercent() {
        return mDiscount;
    }

    public void setDiscountPercent(String mDiscount) {
        this.mDiscount = mDiscount;
    }

    public String getPushHotelName() {
        return mPushHotelName;
    }

    public void setPushHotelName(String mHotelName) {
        this.mPushHotelName = mHotelName;
    }

    public String getPushHotelId() {
        return mPushHotelId;
    }

    public void setPushHotelId(String mHotelId) {
        this.mPushHotelId = mHotelId;
    }

    public String getPushHotelExtras() {
        return mPushHotelExtras;
    }

    public void setPushHotelExtras(String mHotelExtras) {
        this.mPushHotelExtras = mHotelExtras;
    }

    public String getPushHotelFinalPriceWithTax() {
        return mPushHotelFinalPriceWithTax;
    }

    public void setPushHotelFinalPriceWithTax(String mHotelFinalPriceWithTax) {
        this.mPushHotelFinalPriceWithTax = mHotelFinalPriceWithTax;
    }

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        this.mOrigin = origin;
    }

    public String getPushSourceCityName() {
        return mPushSourceCityName;
    }

    public void setPushSourceCityName(String pushSourceCityName) {
        this.mPushSourceCityName = pushSourceCityName;
    }

    public String getPushSourceCityShortName() {
        return mPushSourceCityShortName;
    }

    public void setPushSourceCityShortName(String pushSourceCityShortName) {
        this.mPushSourceCityShortName = pushSourceCityShortName;
    }

    public String getPushDestinationCityName() {
        return mPushDestinationCityName;
    }

    public void setPushDestinationCityName(String pushDestinationCityName) {
        this.mPushDestinationCityName = pushDestinationCityName;
    }

    public String getPushDestinationCityShortName() {
        return mPushDestinationCityShortName;
    }

    public void setPushDestinationCityShortName(String pushDestinationCityShortName) {
        this.mPushDestinationCityShortName = pushDestinationCityShortName;
    }

    public String getPushDate() {
        return mPushDate;
    }

    public void setPushDate(String pushDate) {
        this.mPushDate = pushDate;
    }

    public String getPushPassengerCount() {
        return mPushPassengerCount;
    }

    public void setPushPassengerCount(String pushPassengerCount) {
        this.mPushPassengerCount = pushPassengerCount;
    }

    public String getPushRechargeNumber() {
        return mRechargeNumber;
    }

    public void setPushRechargeNumber(String rechargeNumber) {
        mRechargeNumber = rechargeNumber;
    }

    public String getPushRechargeAmount() {
        return mRechargeAmount;
    }

    public void setPushRechargeAmount(String price) {
        mRechargeAmount = price;
    }

    public String getPushRechargePromo() {
        return mRechargePromoCode;
    }

    public void setPushRechargePromo(String rechargePromo) {
        mRechargePromoCode = rechargePromo;
    }

    public String getSource() {
        return mSource;
    }

    public void setSource(String mSource) {
        this.mSource = mSource;
    }

    public String getPushType() {
        return mPushType;
    }

    public void setPushType(String pushType) {
        this.mPushType = pushType;
    }

    public String getPushCode() {
        return mPushCode;
    }

    public void setPushCode(String pushCode) {
        this.mPushCode = pushCode;
    }

    public boolean isSellerStoreImpressionSent() {
        return isSellerStoreImpressionSent;
    }

    public void setSellerStoreImpressionSent(boolean mISSellerStoreImpressionSent) {
        this.isSellerStoreImpressionSent = mISSellerStoreImpressionSent;
    }

    public String getListId() {
        return mListId;
    }

    public void setListId(String mListId) {
        this.mListId = mListId;
    }

    private String mListId = "";

    public boolean getImageFromResource() {
        return imageFromResource;
    }

    public void setImageFromResource(boolean isImageFromResource) {
        imageFromResource = isImageFromResource;
    }

    public CJRLayoutParams getLayoutParam() {
        return layoutParam;
    }

    private String mPushFlightTripType;
    private String mPushFlightClass;
    private String mPushFlightDepartDate;
    private String mPushFlightReturnDate;

    public String getPushFlightClass() {
        return mPushFlightClass;
    }

    public void setPushFlightClass(String mFlightClass) {
        this.mPushFlightClass = mFlightClass;
    }

    public String getPushFlightTripType() {
        return mPushFlightTripType;
    }

    public void setPushFlightTripType(String mFlightTripType) {
        this.mPushFlightTripType = mFlightTripType;
    }

    public String getPushFlightReturnDate() {
        return mPushFlightReturnDate;
    }

    public void setPushFlightReturnDate(String mPushFlightReturnDate) {
        this.mPushFlightReturnDate = mPushFlightReturnDate;
    }

    public String getPushFlightDepartDate() {
        return mPushFlightDepartDate;
    }

    public void setPushFlightDepartDate(String mPushFlightDepartDate) {
        this.mPushFlightDepartDate = mPushFlightDepartDate;
    }


    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getIfsc() {
        return ifsc;
    }


    public void setBankUserName(String userName) {
        this.userName = userName;
    }

    public String getBankUserName() {
        return userName;
    }


    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }


    public void setBankComment(String bankcomment) {
        this.bankcomment = bankcomment;
    }

    public String getBankComment() {
        return bankcomment;
    }


    public void setBankAmount(String bankAmount) {
        this.bankAmount = bankAmount;
    }

    public String getBankAmount() {
        return bankAmount;
    }

    public String getTrainSourceCityCode() {
        return mTrainSourceCity;
    }

    public void setTrainSourceCityCode(String mTrainSourceCity) {
        this.mTrainSourceCity = mTrainSourceCity;
    }

    public String getTrainDestinationCityCode() {
        return mTrainDestinationCity;
    }

    public void setTrainDestinationCityCode(String mTrainDestinationCity) {
        this.mTrainDestinationCity = mTrainDestinationCity;
    }

    public String getTrainDepartureDate() {
        return mTrainDepartureDate;
    }

    public void setTrainDepartureDate(String mTrainDepartureDate) {
        this.mTrainDepartureDate = mTrainDepartureDate;
    }

    public String getTrainSourceCityName() {
        return mTrainSourceCityName;
    }

    public void setTrainSourceCityName(String mTrainSourceCityName) {
        this.mTrainSourceCityName = mTrainSourceCityName;
    }

    public String getTrainDestinationCityName() {
        return mTrainDestinationCityName;
    }

    public void setTrainDestinationCityName(String mTrainDestinationCityName) {
        this.mTrainDestinationCityName = mTrainDestinationCityName;
    }

    public boolean isTrainRoundTrip() {
        return isTrainRoundTrip;
    }

    public void setTrainRoundTrip(boolean trainRoundTrip) {
        isTrainRoundTrip = trainRoundTrip;
    }

    // p2p deep link params

    public String getP2pamount() {
        return p2pamount;
    }

    public void setP2pamount(String p2pamount) {
        this.p2pamount = p2pamount;
    }

    public String getP2pcomment() {
        return p2pcomment;
    }

    public void setP2pcomment(String p2pcomment) {
        this.p2pcomment = p2pcomment;
    }

    public String getP2pmobilenumber() {
        return p2pmobilenumber;
    }

    public void setP2pmobilenumber(String p2pmobilenumber) {
        this.p2pmobilenumber = p2pmobilenumber;
    }

    //Events
    private String mEventName = null;
    private String mEventCityName = null;
    private String mEventCategory = null;
    private String mEventId = null;
    private String mEventProviderId = null;
    private String mEventSubCategory = null;


    public String getEventName() {
        return mEventName;
    }

    public void setEventName(String mEventName) {
        this.mEventName = mEventName;
    }

    public String getEventCityName() {
        return mEventCityName;
    }

    public void setEventCityName(String mEventCityName) {
        this.mEventCityName = mEventCityName;
    }

    public String getmEventId() {
        return mEventId;
    }

    public void setmEventId(String mEventId) {
        this.mEventId = mEventId;
    }

    public String getEventCategory() {
        return mEventCategory;
    }

    public void setEventCategory(String mEventCategory) {
        this.mEventCategory = mEventCategory;
    }

    public String getEventSubCategory() {
        return mEventSubCategory;
    }

    public void setEventSubCategory(String mEventSubCategory) {
        this.mEventSubCategory = mEventSubCategory;
    }
    public String getEventProviderId() {
        return mEventProviderId;
    }

    public void setEventProviderId(String mEventProviderId) {
        this.mEventProviderId = mEventProviderId;
    }

    public void setSearchABValue(String mSearchABValue) {
        this.mSearchABValue = mSearchABValue;
    }

    @Override
    public String getSearchABValue() {
        return mSearchABValue;
    }

    //Amusement Park
    private String mParkName = null;
    private String mParkId = null;
    private String mParkCityName = null;
    private String mParkCategory = null;
    private String mParkProviderId = null;
    private String mParkcityLabel = null;
    private String mParkcityValue = null;

    public String getParkcityLabel() {
        return mParkcityLabel;
    }

    public void setmParkcityLabel(String parkcityLabel) {
        this.mParkcityLabel = parkcityLabel;
    }

    public String getParkcityValue() {
        return mParkcityValue;
    }

    public void setParkcityValue(String parkcityValue) {
        this.mParkcityValue = parkcityValue;
    }

    public String getmParkCategory() {
        return mParkCategory;
    }

    public void setmParkCategory(String mParkCategory) {
        this.mParkCategory = mParkCategory;
    }

    public String getmParkProviderId() {
        return mParkProviderId;
    }

    public void setmParkProviderId(String mParkProviderId) {
        this.mParkProviderId = mParkProviderId;
    }

    public String getmParkCityName() {
        return mParkCityName;
    }

    public void setmParkCityName(String mParkCityName) {
        this.mParkCityName = mParkCityName;
    }

    public String getmParkName() {
        return mParkName;
    }

    public void setmParkName(String mParkName) {
        this.mParkName = mParkName;
    }

    public String getmParkId() {
        return mParkId;
    }

    public void setmParkId(String mParkId) {
        this.mParkId = mParkId;
    }

    // deeplink for gift card

    public String mGiftCardUrl = null;

    public String getGiftCardUrl() {
        return mGiftCardUrl;
    }

    public void setGiftCardUrl(String giftCardUrl) {
        this.mGiftCardUrl = giftCardUrl;
    }

    //deeplink for upgade wallet
    public String getLanding_page() {
        return landing_page;
    }

    public void setLanding_page(String landing_page) {
        this.landing_page = landing_page;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getAadhar_number() {
        return aadhar_number;
    }

    public void setAadhar_number(String aadhar_number) {
        this.aadhar_number = aadhar_number;
    }


    public String getKyc_name() {
        return kyc_name;
    }

    public void setKyc_name(String kyc_name) {
        this.kyc_name = kyc_name;
    }

    public boolean isFromReqDelivery() {
        return isFromReqDelivery;
    }

    public void setPushRechargeRoaming(String mRoamingEnabled) {
        this.mRoamingEnabled = mRoamingEnabled;
    }

    public void setFromReqDelivery(boolean fromReqDelivery) {
        isFromReqDelivery = fromReqDelivery;
    }

    public String getPushRechargeRoaming() {
        return mRoamingEnabled;
    }

    public void setPushCityValue(String pushCityValue) {
        this.pushCityValue = pushCityValue;
    }

    public String getPushCityValue() {
        return pushCityValue;
    }


    //CST
    private String mTicketId = null;

    public String getmTicketId() {
        return mTicketId;
    }

    public void setmTicketId(String mTicketId) {
        this.mTicketId = mTicketId;
    }

    private String mCinemaMovieCode = null;

    public String getCinemaMovieCode() {
        return mCinemaMovieCode;
    }

    public void setCinemaMovieCode(String mCinemaMovieCode) {
        this.mCinemaMovieCode = mCinemaMovieCode;
    }


    private String mFlightReferral;

    public String getmFlightReferral() {
        return mFlightReferral;
    }

    public void setmFlightReferral(String mFlightReferral) {
        this.mFlightReferral = mFlightReferral;
    }

    public void setSourceInfo(Map<String, Object> info) {
        sourceInfo = info;
    }

    public Map<String, Object> getSourceInfo() {
        return sourceInfo;
    }

    public String getDisplayFieldValue(String displayField) {
        HashMap<String, String> displayFieldsMap = new HashMap<String, String>() {{
            put("brand", getBrand());
            put("image_url", getImageUrl());
            put("name", getName());
            put("offer_price", getFormatedOfferPrice());
            put("actual_price", getFormatedActualPrice());
            put("discount", getDiscountPercent());
        }};
        return displayFieldsMap.get(displayField);
    }

    public String getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }

    public String getP2mamount() {
        return p2mamount;
    }

    public void setP2mamount(String p2mamount) {
        this.p2mamount = p2mamount;
    }

    public String getMerchantMID() {
        return merchantMID;
    }

    public void setMerchantMID(String merchantMID) {
        this.merchantMID = merchantMID;
    }

    public String getP2mComment() {
        return p2mComment;
    }

    public void setP2mComment(String p2mComment) {
        this.p2mComment = p2mComment;
    }

    /*etc params Hotels2.0*/
    @SerializedName("mAltImageUrl")
    private String mAltImageUrl2;
    @SerializedName("mDeleteUrl")
    private String mDeleteUrl;
    @SerializedName("mUrlKey")
    private String mUrlKey;


    public String getAltImageUrl2() {
        return mAltImageUrl2;
    }

    public String getDeleteUrl() {
        return mDeleteUrl;
    }
    public String getmUrlKey() {
        return mUrlKey;
    }

    public String getContactUsIssueParentId() {
        return contactUsIssueParentId;
    }

    public void setContactUsIssueParentId(String contactUsIssueParentId) {
        this.contactUsIssueParentId = contactUsIssueParentId;
    }

    public String getContactUsIssueVerticalLabel() {
        return contactUsIssueVerticalLabel;
    }

    public void setContactUsIssueVerticalLabel(String contactUsIssueVerticalLabel) {
        this.contactUsIssueVerticalLabel = contactUsIssueVerticalLabel;
    }
    public String getPushFilterJson() {
        return mPushFilterJson;
    }

    public void setPushFilterJson(String roomDetailsJson) {
        this.mPushFilterJson = roomDetailsJson;
    }
}
