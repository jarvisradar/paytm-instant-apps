package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.webkit.URLUtil;


import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rama on 6/12/2016.
 */
public class HomePresenter {

    CJRHomePageV2 mHomePage;
    private FJRHomeFragment mHomeFragment;
    private Activity mActivity;
    private Context mContext;
    private String homeUrl = "";
    private String TAG = HomePresenter.class.getSimpleName();

    public HomePresenter(FJRHomeFragment homeFragment) {
        mHomeFragment = homeFragment;
        mActivity = mHomeFragment.getActivity();
        mContext = mActivity.getApplicationContext();
    }

    public void loadData() {

        mHomeFragment.isDataLoading();
        homeUrl = prepareHomeUrl();
        homeUrl = homeUrl + "?deviceIdentifier=LENOVO-LenovoP1ma40-869441021144411&deviceManufacturer=LENOVO&deviceName=Lenovo_P1ma40&client=androidapp&version=6.0.1&playStore=false&lat=12.9348907&long=77.6140976&language=en&networkType=WIFI&imei=869441021144411&osVersion=5.1&lang_id=1&device=android&resolution=3&child_site_id=1&site_id=1";
        updateVolleyRequest();
    }


    public CJRHomePageDetailV2 getWalletHomeLayout() {
        CJRHomePageDetailV2 cjrHomePageDetail = new CJRHomePageDetailV2();

        CJRHomePageLayoutV2 cjrHomePageLayoutV2 = new CJRHomePageLayoutV2();
        ArrayList<CJRHomePageItem> mHomePageItemList = new ArrayList<>();
//        String imageUrl = "https://assetscdn.paytm.com/images/catalog/custom_list/1460367065743" +
//                ".png";

        CJRHomePageItem payItem = new CJRHomePageItem();
        payItem.setName(mActivity.getResources().getString(R.string.top_menu_pay));
        payItem.setUrlType(mActivity.getResources().getString(R.string.top_menu_pay));
//        payItem.setImageUrl(imageUrl);
        payItem.setImageUrl(mActivity.getResources().getString(R.string.top_menu_pay));
        payItem.setImageFromResource(true);

        CJRHomePageItem addItem = new CJRHomePageItem();
        addItem.setName(mActivity.getResources().getString(R.string.top_menu_add_money));
        addItem.setUrlType(mActivity.getResources().getString(R.string.top_menu_add_money));
//        addItem.setImageUrl(imageUrl);
        addItem.setImageUrl(mActivity.getResources().getString(R.string.top_menu_add_money));
        addItem.setImageFromResource(true);

        CJRHomePageItem passBookItem = new CJRHomePageItem();
        passBookItem.setName(mActivity.getResources().getString(R.string.top_menu_passbook));
        passBookItem.setUrlType(mActivity.getResources().getString(R.string.top_menu_passbook));
//        passBookItem.setImageUrl(imageUrl);
        passBookItem.setImageUrl(mActivity.getResources().getString(R.string
                .top_menu_passbook));
        passBookItem.setImageFromResource(true);

        CJRHomePageItem requestItem = new CJRHomePageItem();
        requestItem.setName(mActivity.getResources().getString(R.string
                .top_menu_request_money));
        requestItem.setUrlType(mActivity.getResources().getString(R.string
                .top_menu_request_money));
//        requestItem.setImageUrl(imageUrl);
        requestItem.setImageUrl(mActivity.getResources().getString(R.string
                .top_menu_request_money));
        requestItem.setImageFromResource(true);


        mHomePageItemList.add(payItem);
        mHomePageItemList.add(addItem);
        mHomePageItemList.add(passBookItem);
        mHomePageItemList.add(requestItem);

        cjrHomePageLayoutV2.setLayout(LayoutType.LAYOUT_WALLET_OPTIONS.getName());
        cjrHomePageLayoutV2.setmHomePageItemList(mHomePageItemList);
        cjrHomePageDetail.setmHomePageLayoutList(cjrHomePageLayoutV2);
        return cjrHomePageDetail;
    }

    public String postRequestBodyForV2(Context context, String prevPage, String currPage) {
        return createRequestBodyForV2(context, prevPage, currPage, null);
    }

    public String createRequestBodyForV2(Context context, String prevPage, String currPage, CJRSelectCityModel selectedLocation) {

        JSONObject jsonUserId = new JSONObject();
        JSONObject jsonContext = new JSONObject();
        JSONObject jsonGeo = new JSONObject();
        JSONObject jsonDevice = new JSONObject();
        JSONObject mjsonData = new JSONObject();
        JSONObject jsonTracking = new JSONObject();

        String mlat, mlong;

        if(selectedLocation != null && !TextUtils.isEmpty(selectedLocation.getLatitude()) && !TextUtils.isEmpty(selectedLocation.getLongitude())) {
            mlat = selectedLocation.getLatitude();
            mlong = selectedLocation.getLongitude();
        } else {

        }

        String manufacturer = android.os.Build.MANUFACTURER;
        String model = android.os.Build.MODEL;
        String osVersion = Build.VERSION.RELEASE;
        String hardwareVersion = android.os.Build.DEVICE;
        String userAgent = System.getProperty("http.agent");
        String device_uuid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String ipV6 = "";

        String appVersion = null;
        try {
            appVersion = context.getPackageManager().getPackageInfo(context
                    .getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }

        long userID = 0L;


        try {
            jsonUserId.put("user_id", userID);
            jsonUserId.put("experiment_id", CJRConstants.EXPERIMENT_ID);


            if (userAgent != null) jsonDevice.put("ua", userAgent);
            if (ipV6 != null) jsonDevice.put("ip_v6", ipV6);
            if (manufacturer != null) jsonDevice.put("make", manufacturer);
            if (model != null) jsonDevice.put("model", model);
            if (osVersion != null) jsonDevice.put("osv", osVersion);
            if (hardwareVersion != null) jsonDevice.put("hwv", hardwareVersion);
            if (device_uuid != null) jsonDevice.put("browser_uuid", device_uuid);
            jsonDevice.put("device_type", "PHONE");
            jsonDevice.put("os", "Android");

            jsonContext.put("user", jsonUserId);
            if (appVersion != null) jsonContext.put("version", appVersion);
            jsonContext.put("geo", jsonGeo);
            jsonContext.put("device", jsonDevice);
            jsonContext.put("channel", "APP");

            if (prevPage != null) jsonTracking.put("prev_page", prevPage);
            if (currPage != null) jsonTracking.put("current_page", currPage);
            jsonTracking.put("referer_ui_element", "");

            mjsonData.put("context", jsonContext);
            mjsonData.put("tracking", jsonTracking);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return mjsonData.toString();
    }

    public Map<String, String> postRequestHeaderForV2() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        return headers;
    }

    private void updateVolleyRequest() {

        String jsonContext = postRequestBodyForV2(mContext, "", "paytm-home");

        Map<String, String> headers = postRequestHeaderForV2();

        NetworkRequest request = new NetworkRequest(homeUrl, new
                Response.Listener<IJRDataModel>() {

                    @Override
                    public void onResponse(IJRDataModel dataModel) {

                        mHomeFragment.updateHomePageData(dataModel);

                        //call recommendation
                        CJRHomePageV2 mHomePageItemList = (CJRHomePageV2) dataModel;
                        //loadRecommendedProduct(mHomePageItemList);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mActivity == null) {
                            return;
                        }
                        mHomeFragment.hideProgress();
                        mHomeFragment.showNetworkErrorMessage(mActivity.getResources
                                        ().getString(R.string.unexpected_error_title),
                                mActivity.getResources().getString(R.string
                                        .unexpected_error_message));
                    }
                }, new CJRHomePageV2(), 1, headers, jsonContext.toString());

        CJRVolley.addToRequestQueue(request);
    }

    private String prepareHomeUrl() {

        String homePageUrl = "https://catalog.paytm.com/v2/h/events/recharge";

        if (!URLUtil.isValidUrl(homePageUrl)) {
            /*CJRAppUtility.showAlert(mContext, mContext.getResources()
                            .getString(R.string.error),
                    mContext.getResources().getString(R.string.msg_invalid_url));*/
            return null;
        } else {

            return homePageUrl;
        }

    }



}
