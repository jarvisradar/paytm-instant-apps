package net.one97.paytm;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class EditView extends RelativeLayout {

	private RelativeLayout mMainLayout;
//	private ImageView mImage;
	private TextView mTextView;
	private OnEditViewClickListener mOnEditViewClickListener;

	public interface OnEditViewClickListener {

		void onEditViewClick(View view);
	}

	public EditView(Context context) {
		super(context);
		init(context);
	}

	public EditView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public EditView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}



	private void init(Context context) {
		mMainLayout = new RelativeLayout(context);
		LayoutParams mainParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		mMainLayout.setBackgroundColor(Color.DKGRAY);
		
//		mImage = new ImageView(context);
		mTextView = new TextView(context);
		mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
		setEditText("Edit");
		mTextView.setTextColor(getResources().getColor(R.color.paytm_blue));
		//CJRAppUtility.setRobotoMediumFont(context, mTextView, Typeface.NORMAL);
		mTextView.setGravity(Gravity.CENTER);
		
//		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//		mMainLayout.addView(mImage, params);
		
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mMainLayout.addView(mTextView, params);
		
		addView(mMainLayout, mainParams);
		
		mMainLayout.setClickable(true);
		mMainLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				mOnEditViewClickListener.onEditViewClick(view);
			}
		});
//		mMainLayout.setVisibility(View.GONE);
	}
	
	public void setMainLayoutParams(int gridUnit, int height) {
		LayoutParams listViewParams = (LayoutParams) mMainLayout.getLayoutParams();
		listViewParams.width = gridUnit;
		listViewParams.height = height;
		
		LayoutParams textParams = (LayoutParams) mTextView.getLayoutParams();
		textParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//textParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		
		/*LayoutParams imageParams = (LayoutParams) mImage.getLayoutParams();
		imageParams.addRule(RelativeLayout.CENTER_VERTICAL);*/
	}
	
	public void setMainLayoutPadding(int left, int top, int right, int bottom) {
		mMainLayout.setPadding(left, top, right, bottom);
	}
	
	public void setEditTextMargin(int left, int top, int right, int bottom) {
		LayoutParams textParams = (LayoutParams) mTextView.getLayoutParams();
		textParams.leftMargin = left;
		textParams.topMargin = top;
		textParams.rightMargin = right;
		textParams.bottomMargin = bottom;
	}
	
	public void setMainLayoutBackground(int resid) {
		mMainLayout.setBackgroundResource(resid);
	}

	/*public void setImageBackground(int resid) {
		mImage.setBackgroundResource(resid);
	}*/

	public void setEditTextBackground(int resid) {
		mTextView.setBackgroundResource(resid);
	}
	
	public void setEditTextColor(int color) {
		mTextView.setTextColor(color);
	}

	public void setEditText(String text) {
		if (text != null) {
			mTextView.setVisibility(View.VISIBLE);
			mTextView.setText(text);
		} else {
			mTextView.setVisibility(View.GONE);
		}
	}
	
	public void setClickable(boolean isClickable) {
		mMainLayout.setClickable(isClickable);
	}
	
	public String getEditText() {
		return mTextView.getText().toString();
	}
	
	public void setTextSize(float size) {
		mTextView.setTextSize(size);
	}

	public void setEditTextVisibility(int visibility) {
		mTextView.setVisibility(visibility);
	}
	
	public void setOnEditViewClickListener(OnEditViewClickListener listener) {
		mOnEditViewClickListener = listener;
		
	}
}