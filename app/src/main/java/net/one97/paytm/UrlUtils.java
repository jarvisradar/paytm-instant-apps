package net.one97.paytm;

import java.net.URLEncoder;

/**
 * Created by vipulgupta on 6/2/17.
 */

public class UrlUtils {

    private static String[] siteIdClientsList = new String[]{"catalog.paytm.com", "cart.paytm.com", "/promosearch/product", "search.paytm.com"};

    public static String appendSiteIdInUrl(String url){
        return appendSiteId(url);
    }

    private static String appendSiteId(String url) {

        if(url == null) {
            return null;
        }

        for (String siteIdClient :
                siteIdClientsList) {
            if (url.contains(siteIdClient)) {
                url = appendSiteIdToUrl(url);
                break;
            }
        }
        return url;
    }

    private static String appendSiteIdToUrl(String url) {
        if ((url != null) && (!url.contains(CJRConstants.SEARCH_URL_KEYWORD_SITE_ID))) {

            try {
                String childSiteId = CJRAppUtils.getChildSiteId();
                String siteId = CJRAppUtils.getSiteId();
                if (childSiteId != null) {
                    if (url.contains("?")) {
                        url += CJRConstants.SEARCH_URL_KEYWORD_CHILD_SITE_ID +
                                URLEncoder.encode(childSiteId, CJRConstants.ENCODE_CHARSET_NAME);
                    } else {
                        url +=  CJRConstants.SEARCH_URL_KEYWORD_CHILD_SITE_ID.replace("&", "?") +
                                URLEncoder.encode(childSiteId, CJRConstants.ENCODE_CHARSET_NAME);
                    }
                }
                if (siteId != null) {
                    url += CJRConstants.SEARCH_URL_KEYWORD_SITE_ID +
                            URLEncoder.encode(siteId, CJRConstants.ENCODE_CHARSET_NAME);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return url;
    }
}